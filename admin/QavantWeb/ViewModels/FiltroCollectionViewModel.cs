﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class FiltroCollectionViewModel
    {
        public FiltroCollectionViewModel()
        {
            FiltroCollection = new List<FiltroViewModel>();
        }

        public List<FiltroViewModel> FiltroCollection { get; set; }
    }
}