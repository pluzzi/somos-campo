﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class SegmentoViewModel
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public byte TipoFiltroId { get; set; }
        public string TipoFiltroDescripcion { get; set; }
        public int ValorFiltroId { get; set; }
        public string ValorFiltroDescripcion { get; set; }
    }
}