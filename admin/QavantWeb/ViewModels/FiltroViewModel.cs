﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class FiltroViewModel
    {
        public TipoFiltroViewModel Tipo { get; set; }
        public Dictionary<string,string> Opciones { get; set; }
    }
}