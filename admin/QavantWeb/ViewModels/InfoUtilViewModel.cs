﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class InfoUtilViewModel
    {
        public InfoUtilViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public InfoUtil InfoUtil { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}