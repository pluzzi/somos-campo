﻿using QavantWeb.Models;

namespace QavantWeb.ViewModels
{
    public class FormViewModel
    {
        public FormViewModel()
        {
            FiltroConfig = new FiltroConfigViewModel();
        }

        public Form Formulario { get; set; }

        public FiltroConfigViewModel FiltroConfig { get; set; }
    }
}