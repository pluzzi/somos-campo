﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.ViewModels
{
    public class FiltroConfigViewModel
    {
        public FiltroConfigViewModel()
        {
            Filtros = new FiltroCollectionViewModel();
            Segmentacion = new SegmentacionViewModel();
        }
        public FiltroCollectionViewModel Filtros { get; set; }

        public SegmentacionViewModel Segmentacion { get; set; }
    }
}