﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    public class GrafTendenciasController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Dashboard
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            var audPorArea = db.est_DistribuciónPorArea(empresaId);
            ViewBag.Est_AudienciaPorArea = audPorArea;
            
            var audPorEdad = db.est_AudienciaPorEdades(empresaId, DateTime.Now).ToList();
            string porEdadDecenaUnida = "";
            var cantActPorEdad = "";
            var cantInactPorEdad = "";
            foreach (var item in audPorEdad)
            {
                if (item.DecenaUnida != null)
                {
                    porEdadDecenaUnida += "\'" + item.DecenaUnida + "\'" + ",";
                }
                if (item.cantidad != null && item.Activo > 0)
                {
                    cantActPorEdad += item.cantidad + ",";
                    cantInactPorEdad += "0,";
                }
                else
                {
                    cantInactPorEdad += item.cantidad + ",";
                    cantActPorEdad += "0,";
                }

            }
            if (porEdadDecenaUnida != "")
                porEdadDecenaUnida = porEdadDecenaUnida.Substring(0, porEdadDecenaUnida.Length - 1);
            ViewBag.porEdadDecenaUnida = porEdadDecenaUnida;
            if (cantActPorEdad != "")
                cantActPorEdad = cantActPorEdad.Substring(0, cantActPorEdad.Length - 1);
            ViewBag.cantActivosPorEdad = cantActPorEdad;
            if (cantInactPorEdad != "")
                cantInactPorEdad = cantInactPorEdad.Substring(0, cantInactPorEdad.Length - 1);
            ViewBag.cantInactivosPorEdad = cantInactPorEdad;

            //var audPorPuesto = db.est_GetAudiencia_DistribuciónPorPuesto(empresaId);            
            //string AudienciaPorPuesto = "";
            //var cantActPorPuesto = "";
            //var cantInactPorPuesto = "";
            //foreach (var item in audPorPuesto)
            //{
            //    if (item.descripcion != null)
            //        AudienciaPorPuesto += "\'" + item.descripcion + "\'" + ",";
            //    if (item.cantidad != null && item.active)
            //    {
            //        cantActPorPuesto += item.cantidad + ",";
            //        cantInactPorPuesto += "0,";
            //    }
            //    else
            //    {
            //        cantInactPorPuesto += item.cantidad + ",";
            //        cantActPorPuesto += "0,";
            //    }
            //}
            //if (AudienciaPorPuesto != "")
            //    AudienciaPorPuesto = AudienciaPorPuesto.Substring(0, AudienciaPorPuesto.Length - 1);
            //ViewBag.Est_AudienciaPorPuesto = AudienciaPorPuesto;
            //if (cantActPorPuesto != "")
            //    cantActPorPuesto = cantActPorPuesto.Substring(0, cantActPorPuesto.Length - 1);
            //ViewBag.Est_ActPorPuesto = cantActPorPuesto;
            //if (cantInactPorPuesto != "")
            //    cantInactPorPuesto = cantInactPorPuesto.Substring(0, cantInactPorPuesto.Length - 1);
            //ViewBag.Est_InactPorPuesto = cantInactPorPuesto;


            //var audPorGenero = db.est_GetAudiencia_DistribuciónPorGenero(empresaId);
            //ViewBag.Est_AudienciaPorGenero = audPorGenero;

            return View();
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
