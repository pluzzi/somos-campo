﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.Services.Settings;
using System.Text.RegularExpressions;

namespace QavantWeb.Controllers
{
    public class DashboardController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        private string fechaDesde = DateTime.Now.Date.AddDays(-30).ToShortDateString();
        private string fechaHasta = DateTime.Now.Date.ToShortDateString();

        // GET: Dashboard
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            DateTime desde = DateTime.Parse(fechaDesde);
            DateTime hasta = DateTime.Parse(fechaHasta);
            string userId = User.Identity.GetUserId();


            #region VISITAS POR DÍA TODOS LOS CANALES 
            var visitasPorDiaCanales = db.est_Dashboard_VisitasPorDia(empresaId, desde, hasta);
            var cantidadVisitasPorDiasCanales = "";

            foreach (var item in visitasPorDiaCanales)
            {
                cantidadVisitasPorDiasCanales += item.Cantidad + ",";
            }

            if (cantidadVisitasPorDiasCanales != "")
                cantidadVisitasPorDiasCanales = cantidadVisitasPorDiasCanales.Substring(0, cantidadVisitasPorDiasCanales.Length - 1);
            ViewBag.CantidadVisitasPorDiasCanales = cantidadVisitasPorDiasCanales;
            ViewBag.fDia = desde.Day;
            ViewBag.fMes = desde.Month - 1;
            ViewBag.fAnio = desde.Year;
            #endregion

            #region VISITAS POR HORA TODOS LOS CANALES
            var visitasPorHoraCanales = db.est_Dashboard_VisitasPorHora(empresaId, desde, hasta);
            var cantVisitPorHoraCanales = "";
            ViewBag.fAyer = DateTime.Now.AddDays(-1).ToShortDateString();

            foreach (var item in visitasPorHoraCanales)
            {
                cantVisitPorHoraCanales += item.Cantidad + ",";
            }
            if (cantVisitPorHoraCanales != "")
                cantVisitPorHoraCanales = cantVisitPorHoraCanales.Substring(0, cantVisitPorHoraCanales.Length - 1);
            ViewBag.CantVisitPorHoraCanales = cantVisitPorHoraCanales;

            #endregion

            #region MÓDULOS MAYOR REPERCUCIÓN 
            var modMayorRepercucion = db.est_Dashboard_ModulosMasRepercucion(empresaId, desde, hasta);
            var mayorReperTitulo = "";
            var mReperCantComentarios = "";
            var mReperCantSentimentos = "";
            var mReperCantVisitas = "";
            var mRepecCantTotal = "";

            foreach (var item in modMayorRepercucion)
            {
                mayorReperTitulo += "\'" + item.Nombre.SoloLetrasYNumeros().ReemplazaCaracteresEspeciales() + "\'" + ",";
                mReperCantComentarios += item.CantidadComentarios +  ",";
                mReperCantSentimentos += item.CantidadSentimientos + ",";
                mReperCantVisitas += item.CantidadVisitas + ",";
                mRepecCantTotal += item.CantidadTotal + ",";
            }
            if( mayorReperTitulo != "")
                mayorReperTitulo = mayorReperTitulo.Substring(0, mayorReperTitulo.Length - 1);
            if(mReperCantComentarios != "")
                mReperCantComentarios = mReperCantComentarios.Substring(0, mReperCantComentarios.Length - 1);
            if(mReperCantSentimentos != "")
                mReperCantSentimentos = mReperCantSentimentos.Substring(0, mReperCantSentimentos.Length - 1);
           if( mReperCantVisitas != "")
                mReperCantVisitas = mReperCantVisitas.Substring(0, mReperCantVisitas.Length - 1);
            if(mRepecCantTotal != "")
                mRepecCantTotal = mRepecCantTotal.Substring(0, mRepecCantTotal.Length - 1);

            ViewBag.MayorRepercTitulo = mayorReperTitulo;
            ViewBag.MayorRepercComentarios = mReperCantComentarios;
            ViewBag.MayorRepercSentimientos = mReperCantSentimentos;
            ViewBag.MayorRepecVisitas = mReperCantVisitas;
            #endregion

            #region TABLA USUARIOS MÁS PARTICIPATIVOS
            var usuariosMasParticipativos = db.est_Dashboard_UsuariosMasParticipativos(empresaId, desde, hasta);
            List<est_Dashboard_UsuariosMasParticipativos_Result> lstUsuMasParticip = new List<est_Dashboard_UsuariosMasParticipativos_Result>();
            foreach (var item in usuariosMasParticipativos)
            {
                lstUsuMasParticip.Add(item);
            }
            SettingsService _settingsService = new SettingsService(Enums.Modulos.Params);
            var parametros =  _settingsService.GetParametrosGlobales();
            var colorPrincipal = "#808080";
            var colorSecundario = "#fff";

            foreach (var item in parametros)
            {
                if (item.Key == "ColorPrincipal")               
                    colorPrincipal = item.Value; 
            } 
            foreach (var item in lstUsuMasParticip)
            {
                var primerLetraNombre = " ";
                var primerLetraApellido = " ";
                string NombreTemp = item.UsuarioNombre.RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                string[] inicialesNombre = NombreTemp.Split(' ');
                primerLetraNombre = inicialesNombre[0].Substring(0, 1).ToUpper();
                if(inicialesNombre.Length > 1)
                    primerLetraApellido = inicialesNombre[1].Substring(0, 1).ToUpper();
                if (item.imagen == "")
                {
                    AvatarGenerator avatar = new AvatarGenerator();
                    item.imagen = avatar.Generate(primerLetraNombre, primerLetraApellido, colorPrincipal, colorSecundario);
                }
            }

            ViewBag.listaUsuariosTabla = lstUsuMasParticip;
            #endregion

            #region  /* --beneficios más valorados */
            var benefMasValorados = db.vw_gra_BeneficiosValorados
                                        .Where(w => w.EmpresaId == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                        .ToList();
            string benefCategorias = "";
            string benefValores = "";

            foreach (var item in benefMasValorados)
            {
                benefCategorias += "'" + item.Titulo + "',";
                benefValores += item.CantLikes + ",";
            }

            if (benefCategorias != "")
                benefCategorias = benefCategorias.Substring(0, benefCategorias.Length - 1);

            if (benefValores != "")
                benefValores = benefValores.Substring(0, benefValores.Length - 1);

            ViewBag.benefCatMasValorados = benefCategorias;
            ViewBag.benefValMasValorados = benefValores;
            #endregion

            #region  /*--beneficios más visitados    */
            var benefMasVisitados = db.vw_gra_BeneficiosVisitados
                                        .Where(w => w.Empresaid == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                        .Take(10).ToList();

            benefCategorias = "";
            benefValores = "";

            foreach (var item in benefMasVisitados)
            {
                benefCategorias += "'" + item.Titulo + "',";
                benefValores += item.CantVisitas + ",";
            }

            if (benefCategorias != "")
                benefCategorias = benefCategorias.Substring(0, benefCategorias.Length - 1);

            if (benefValores != "")
                benefValores = benefValores.Substring(0, benefValores.Length - 1);

            ViewBag.benefCatMasVisitados = benefCategorias;
            ViewBag.benefValMasVisitados = benefValores;
            #endregion

            #region /*--noticias más visitadas  Log de Actividad desc*/

            var noticiasMasVisitadas = db.vw_gra_NoticiasVisitadas
                                        .Where(w => w.Empresaid == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                        .Take(10).ToList();


            string noticiaCategorias = "";
            string noticiaValores = "";

            foreach (var item in noticiasMasVisitadas)
            {
                noticiaCategorias += "'" + item.Titulo + "',";
                noticiaValores += item.CantVisitas + ",";
            }

            if (noticiaCategorias != "")
                noticiaCategorias = noticiaCategorias.Substring(0, noticiaCategorias.Length - 1);

            if (noticiaValores != "")
                noticiaValores = noticiaValores.Substring(0, noticiaValores.Length - 1);

            ViewBag.notiCatMasVisitados = noticiaCategorias;
            ViewBag.notiValMasVisitados = noticiaValores;
            #endregion

            #region /*--secciones más visitadas */
            var seccionMasVisitada = db.vw_gra_ModulosVisitados
                                        .Where(w => w.EmpresaId == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                        .OrderByDescending(o => o.cantidad)
                                        .Take(10)
                                        .ToList();

            string seccionCategorias = ""; //3,2
            string seccionValores = "";//beneficio-detalle/noticia-detalle


            foreach (var item in seccionMasVisitada)
            {
                seccionCategorias += "'" + item.Nombre + "',";
                seccionValores += item.cantidad + ",";
            }

            if (seccionCategorias != "")
                seccionCategorias = seccionCategorias.Substring(0, seccionCategorias.Length - 1);

            if (seccionValores != "")
                seccionValores = seccionValores.Substring(0, seccionValores.Length - 1);

            ViewBag.seccionCatMasVisitadas = seccionCategorias;
            ViewBag.seccionValMasVisitadas = seccionValores;
            #endregion

            #region  /*-- USUARIOS ACTIVOS  */
            /*-- USUARIOS ACTIVOS
            select count(1) from Users
            where Active = 1*/
            var usersActivos = db.Users
                                    .Where(w => w.EmpresaId == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                    .Where(w => w.Active == true).Count();

            ViewBag.usuariosActivos = usersActivos;
            #endregion

            #region /*-- PAG VISITADAS */
            var pagVisitadasReg = db.vw_gra_PaginasVisitadas
                                    .Where(w => w.EmpresaId == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                    .FirstOrDefault();

            int? pagVisitadas = default(int);
            if (pagVisitadasReg != null) {
                pagVisitadas = pagVisitadasReg.cantidad;
            }
            ViewBag.paginasVisitadas = pagVisitadas;


            /* --Visitas por hora */
            //var visitPorHora = db.est_GetCanales_VisitasPorHora(empresaId, desde, hasta);            
            var visitPorHora = db.est_Canales_VisitasPorHora(empresaId, desde, hasta);
            string visitaWebCantidad = "";
            string visitaAppCantidad = "";

            foreach (var item in visitPorHora)
            {
                for (int i = 0; i < 23; i++)
                {
                    if(item.Hora == i)
                    {
                        if(item.Nombre == "Web")
                        {
                            visitaWebCantidad += item.Cantidad + ",";
                        }
                        else
                        {
                            visitaAppCantidad += item.Cantidad + ",";
                        }
                    }
                }
            }
          
            if (visitaWebCantidad != "")
                visitaWebCantidad = visitaWebCantidad.Substring(0, visitaWebCantidad.Length - 1);
            ViewBag.cantVisitasPorHoraWeb = visitaWebCantidad;

            if (visitaAppCantidad != "")
                visitaAppCantidad = visitaAppCantidad.Substring(0, visitaAppCantidad.Length - 1);
            ViewBag.cantVisitasPorHoraApp = visitaAppCantidad;
            #endregion

            #region /*--Uso de la app en los últimos 30 dias*/

            var usoPorDias = db.vw_gra_UsoUltimos30Dias
                                    .Where(w => w.EmpresaId == db.AspNetUsers.Where(wh => wh.Id == userId).FirstOrDefault().EmpresaId)
                                    .ToList();

            string valorVisitValDia = "";
            string dias = "";

            for (int i = 0; i <= 30; i++)
            {
                DateTime tday = DateTime.Now.AddDays(-30 + i);
                string resul;
                resul = string.Format("{0:dd/MM/yyyy}", tday);

                //dias += "'" + tday.Day.ToString( )+ "/" + tday.Month.ToString() + "',";
                dias += "'" + resul.Substring(0, resul.Length - 5) + "',";

                var d = usoPorDias.Where(w => w.fecha.ToString() == tday.Day.ToString() + "/" + tday.Month.ToString() + "/" + tday.Year.ToString()).FirstOrDefault();
                if (d != null)
                {
                    valorVisitValDia += d.cantidad + ",";

                }
                else
                {
                    valorVisitValDia += "0,";
                }
            }

            if (valorVisitValDia != "")
                valorVisitValDia = valorVisitValDia.Substring(0, valorVisitValDia.Length - 1);
            if (dias != "")
                dias = dias.Substring(0, dias.Length - 1);

            ViewBag.UltimasVisitas = valorVisitValDia;
            ViewBag.UltimosDias = dias;
            #endregion

            return View();
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
