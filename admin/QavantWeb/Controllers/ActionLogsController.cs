﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class ActionLogsController : Controller
    {
        private QavantEntities db = new QavantEntities();

        // GET: ActionLogs
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            SelectListItem actionLogTypesDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsActionLogTypes = new SelectList(db.ActionLogTypes, "Id", "Name").ToList();
            lsActionLogTypes.Insert(0, actionLogTypesDefault);

            SelectListItem SectionDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsSections = new SelectList(db.Sections.OrderBy(x => x.Name), "Id", "Name").ToList();
            lsSections.Insert(0, SectionDefault);

            SelectListItem UserDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsUsers = new SelectList(db.Users, "Id", "Name").ToList();
            lsUsers.Insert(0, UserDefault);

            SelectListItem BenefitDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsBenefits = new SelectList(db.Benefits.OrderBy(x => x.Title), "Id", "Title").ToList();
            lsBenefits.Insert(0, BenefitDefault);

            SelectListItem NewsDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsNews = new SelectList(db.Noticias.OrderBy(x => x.Titulo), "Id", "Titulo").ToList();
            lsNews.Insert(0, NewsDefault);

            ViewBag.ActionLogTypeId = lsActionLogTypes;
            ViewBag.SectionId = lsSections;
            ViewBag.User_Id = lsUsers;
            ViewBag.BenefitId = lsBenefits;
            ViewBag.NewsId = lsNews;

            var actionLogs = db.ActionLogs.Include(a => a.ActionLogType).Include(a => a.Section).Include(a => a.User); //tabla actionLogs

            foreach (var act in actionLogs)
            {
                if (act.SectionId == (byte)Enums.SectionsIds.Beneficios)
                {
                    act.ItemTitle = db.Benefits.Find(act.ItemId).Title;
                }
                else if (act.SectionId == (byte)Enums.SectionsIds.Noticias)
                {
                    act.ItemTitle = db.Noticias.Find(act.ItemId).Titulo;
                }

            }

            return View(actionLogs);
        }

        // POST: ActionLogs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(ActionLog actionLog)
        {
            var sectionId = string.IsNullOrEmpty(actionLog.SectionId.ToString()) ? 0 : actionLog.SectionId;
            var actionLogTypeId = string.IsNullOrEmpty(actionLog.ActionLogTypeId.ToString()) ? 0 : actionLog.ActionLogTypeId;
            int itemId = 0;

            List<ActionLog> actions = db.ActionLogs.Include(a => a.User).ToList();

            if (sectionId != 0)
            {
                actions = actions.Where(w => w.SectionId == sectionId).ToList();
            }

            if (actionLogTypeId != 0)
            {
                actions = actions.Where(w => w.ActionLogTypeId == actionLogTypeId).ToList();
            }

            if (sectionId == (byte)Enums.SectionsIds.Beneficios)
            {
                itemId = actionLog.BenefitId;
            }
            else if (sectionId == (byte)Enums.SectionsIds.Noticias)
            {
                itemId = actionLog.NewsId;
            }

            if (itemId != 0)
            {
                actions = actions.Where(x => x.ItemId == itemId).ToList();
            }

            foreach (var act in actions)
            {
                if (act.SectionId == (byte)Enums.SectionsIds.Beneficios)
                {
                    act.ItemTitle = db.Benefits.Find(act.ItemId).Title;
                }
                else if (act.SectionId == (byte)Enums.SectionsIds.Noticias)
                {
                    act.ItemTitle = db.Noticias.Find(act.ItemId).Titulo;
                }
                 
            }

            SelectListItem SeccionDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsSections = new SelectList(db.Sections, "Id", "Name", sectionId).ToList();
            lsSections.Insert(0, SeccionDefault);
            ViewBag.SectionId = lsSections;

            SelectListItem ActionLogDefault = new SelectListItem { Text = " Seleccionar", Value = "0" };
            List<SelectListItem> lsActionLog = new SelectList(db.ActionLogTypes, "Id", "Name", actionLogTypeId).ToList();
            lsActionLog.Insert(0, ActionLogDefault);
            ViewBag.ActionLogTypeId = lsActionLog;

            SelectListItem BenefitDefault = new SelectListItem { Text = "  Seleccionar  ", Value = "0" };
            SelectListItem NewsDefault = new SelectListItem { Text = "  Seleccionar ", Value = "0" };

            List<SelectListItem> lsBenefit = new SelectList(db.Benefits, "Id", "Title", 0).ToList();
            List<SelectListItem> lsNews = new SelectList(db.Noticias, "Id", "Titulo", 0).ToList();

            lsBenefit.Insert(0, BenefitDefault);
            lsNews.Insert(0, ActionLogDefault);

            if (sectionId == (byte)Enums.SectionsIds.Beneficios)
            {
                //lsBenefit = new SelectList(db.Benefits, "Id", "Title", itemId).ToList();
                var selected = lsBenefit.Where(x => x.Value == itemId.ToString()).First();
                selected.Selected = true;
            }
            else if (sectionId == (byte)Enums.SectionsIds.Noticias)
            {
                //lsNews = new SelectList(db.Noticias, "Id", "Title", itemId).ToList();
                var selected = lsNews.Where(x => x.Value == itemId.ToString()).First();
                selected.Selected = true;
            }

            ViewBag.BenefitId = lsBenefit;
            ViewBag.NewsId = lsNews;

            return View(actions);
        }

        // GET: ActionLogs/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            return View(actionLog);
        }

        // GET: ActionLogs/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            ViewBag.ActionLogTypeId = new SelectList(db.ActionLogTypes, "Id", "Name");
            ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name");
            ViewBag.User_Id = new SelectList(db.Users, "Id", "Name");
            return View();
        }

        // POST: ActionLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create([Bind(Include = "Id,SectionId,Date,ActionLogTypeId,User_Id,ItemId")] ActionLog actionLog)
        {
            if (ModelState.IsValid)
            {
                db.ActionLogs.Add(actionLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ActionLogTypeId = new SelectList(db.ActionLogTypes, "Id", "Name", actionLog.ActionLogTypeId);
            ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name", actionLog.SectionId);
            ViewBag.User_Id = new SelectList(db.Users, "Id", "Name", actionLog.User_Id);
            return View(actionLog);
        }

        // GET: ActionLogs/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActionLogTypeId = new SelectList(db.ActionLogTypes, "Id", "Name", actionLog.ActionLogTypeId);
            ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name", actionLog.SectionId);
            ViewBag.User_Id = new SelectList(db.Users, "Id", "Name", actionLog.User_Id);
            return View(actionLog);
        }

        // POST: ActionLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit([Bind(Include = "Id,SectionId,Date,ActionLogTypeId,User_Id,ItemId")] ActionLog actionLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actionLog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ActionLogTypeId = new SelectList(db.ActionLogTypes, "Id", "Name", actionLog.ActionLogTypeId);
            ViewBag.SectionId = new SelectList(db.Sections, "Id", "Name", actionLog.SectionId);
            ViewBag.User_Id = new SelectList(db.Users, "Id", "Name", actionLog.User_Id);
            return View(actionLog);
        }

        // GET: ActionLogs/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = db.ActionLogs.Find(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            return View(actionLog);
        }

        // POST: ActionLogs/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActionLog actionLog = db.ActionLogs.Find(id);
            db.ActionLogs.Remove(actionLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
