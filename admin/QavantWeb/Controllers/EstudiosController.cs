﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class EstudiosController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Estudios
        public ActionResult Index(int ? id)
        {
            var estudios = db.Estudios.Where(w => w.UserId == id && w.User.EmpresaId == empresaId).Include(e => e.Catalogo).Include(e => e.Establecimiento).Include(e => e.Titulo).Include(e => e.User);
            ViewData["userId"] = id;
            return View(estudios.ToList());
        }

        // GET: Estudios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estudio estudio = db.Estudios.Find(id);
            if (estudio == null)
            {
                return HttpNotFound();
            }
            return View(estudio);
        }

        // GET: Estudios/Create
        public ActionResult Create(int? id)
        {
            ViewData["myUserId"] = id;

            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();            
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catId = 2;

            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "ESTU")
                {
                    lstCatCustom.Add(item);
                    catId = item.id;
                    break;
                }
            }

            List<Catalogo> lstCatalogo = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId).ToList();
            }
            else
            {                
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId).ToList();
            }

            ViewBag.TipoEstudioCatalogoId = new SelectList(lstCatalogo, "Id", "DescripcionLarga");
            ViewBag.EstablecimientoId = new SelectList(db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado), "Id", "Nombre");
            ViewBag.TituloId = new SelectList(db.Titulos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion");
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name");
            //ViewBag.UserId = id;
            return View();
        }

        // POST: Estudios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Estudio estudio, int id)
        {
            ViewData["myUserId"] = id;
            if (id != null)
                estudio.UserId = id;
            if (ModelState.IsValid)
            {
                if(estudio.TipoEstudioCatalogoId > 0)
                {
                    db.Estudios.Add(estudio);
                    db.SaveChanges();
                }
                return RedirectToAction("Index", new { id = estudio.UserId });
            }
      
            ViewBag.TipoEstudioCatalogoId = new SelectList(db.Catalogos.Where(w => w.CatalogosTipo.EmpresaId == empresaId), "Id", "DescripcionLarga", estudio.TipoEstudioCatalogoId);
            ViewBag.EstablecimientoId = new SelectList(db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado), "Id", "Nombre", estudio.EstablecimientoId);
            ViewBag.TituloId = new SelectList(db.Titulos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", estudio.TituloId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", estudio.UserId);
                       
            return View(estudio);
        }

        // GET: Estudios/Edit/5
        public ActionResult Edit(int? id, int ? uId)
        {
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estudio estudio = db.Estudios.Where(w => w.User.EmpresaId == empresaId).Single(s => s.Id == id);
            if (estudio == null)
            {
                return HttpNotFound();
            }

            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catId = 2;

            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "ESTU")
                {
                    lstCatCustom.Add(item);
                    catId = item.id;
                    break;
                }
            }

            List<Catalogo> lstCatalogo = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
            {
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId).ToList();
            }
            else
            {
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId).ToList();
            }

            ViewBag.TipoEstudioCatalogoId = new SelectList(lstCatalogo, "Id", "DescripcionLarga");
            //ViewBag.TipoEstudioCatalogoId = new SelectList(db.Catalogos.Where(w => w.CatalogosTipo.EmpresaId == empresaId).Where(w=>w.CatalogoTipoId == 2), "Id", "DescripcionLarga", estudio.TipoEstudioCatalogoId);
            ViewBag.EstablecimientoId = new SelectList(db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado), "Id", "Nombre", estudio.EstablecimientoId);
            ViewBag.TituloId = new SelectList(db.Titulos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", estudio.TituloId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", estudio.UserId);
            return View(estudio);
        }

        // POST: Estudios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Estudio estudio,int ? id, int uId)
        {
            estudio.UserId = uId;
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;
            GetErrorListFromModelState(ModelState);
            if (ModelState.IsValid)
            {
                db.Entry(estudio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Estudios", new { id = uId });
            }
            ViewBag.TipoEstudioCatalogoId = new SelectList(db.Catalogos.Where(w => w.CatalogosTipo.EmpresaId == empresaId).Where(w => w.CatalogoTipoId == 2), "Id", "DescripcionLarga", estudio.TipoEstudioCatalogoId);
            ViewBag.EstablecimientoId = new SelectList(db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado), "Id", "Nombre", estudio.EstablecimientoId);
            ViewBag.TituloId = new SelectList(db.Titulos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", estudio.TituloId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", estudio.UserId);
            return View(estudio);
        }
        public static List<string> GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;

            var errorList = query.ToList();
            return errorList;
        }

        // GET: Estudios/Delete/5
        public ActionResult Delete(int? id , int? uId)
        {
            if (id != 0)
            {
                if (db.Estudios.FirstOrDefault(f => f.Id == id).Establecimiento.EmpresaId != empresaId)
                    //Intenton hackear por empresaId
                    return RedirectToAction("Index");
            }
            ViewData["uId"] = uId;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estudio estudio = db.Estudios.Where(w => w.Establecimiento.EmpresaId == empresaId).Single(s => s.Id == id);
            if (estudio == null)
            {
                return HttpNotFound();
            }
            db.Estudios.Remove(estudio);
            db.SaveChanges();
            return RedirectToAction("Index", "Estudios", new { id = uId });
        }

        // POST: Estudios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estudio estudio = db.Estudios.Find(id);
            db.Estudios.Remove(estudio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
