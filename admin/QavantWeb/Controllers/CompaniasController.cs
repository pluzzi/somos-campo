﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class CompaniasController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Companias
        public ActionResult Index()
        {
            var companias = db.Companias.Include(c => c.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(companias.ToList());
        }

        // GET: Companias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compania compania = db.Companias.Find(id);
            if (compania == null)
            {
                return HttpNotFound();
            }
            return View(compania);
        }

        // GET: Companias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Compania compania)
        {
            if (ModelState.IsValid)
            {
                compania.EmpresaId = empresaId;
                db.Companias.Add(compania);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var companias = db.Companias.Include(r => r.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(companias);
        }

        // GET: Companias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compania compania = db.Companias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (compania == null)
            {
                return HttpNotFound();
            }           
            return View(compania);
        }

        // POST: Companias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Compania compania)
        {
            if (compania.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            if (ModelState.IsValid)
            {
                db.Entry(compania).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "Nombre", compania.EmpresaId);
            return View(compania);
        }

        // GET: Companias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compania compania = db.Companias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (compania == null)
            {
                return HttpNotFound();
            }
            return View(compania);
        }

        // POST: Companias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Compania compania = db.Companias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if(compania.Users.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar la compañía. la compañía posee usuarios asociados. ");
            }
            else
            {
                db.Companias.Remove(compania);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(compania);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
