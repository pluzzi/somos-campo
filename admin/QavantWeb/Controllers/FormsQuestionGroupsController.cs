﻿using QavantWeb.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class FormsQuestionGroupsController : BaseController
    {
        private Repository<FormsQuestionGroup> formsQuestionGroupRepository;
        private Repository<Form> formRepository;
        public FormsQuestionGroupsController()
        {
            formsQuestionGroupRepository = unitOfWork.Repository<FormsQuestionGroup>();
            formRepository = unitOfWork.Repository<Form>();
        }
        public ActionResult Index(int? id)
        {
            ViewBag.Form_Id = id;
            ViewBag.FormTitle = formRepository.Table.Where(x => x.Id == id).FirstOrDefault().Title;
            ViewBag.FormTypeId = formRepository.Table.Where(x => x.Id == id).FirstOrDefault().FormTypeId;
            //return View(formsQuestionGroupRepository.Table.Where(x=> x.Form_Id==id && x.Enabled).ToList());
            return View();
        }

        // GET: FormsQuestionGroups/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        
        public ActionResult Create(int? formId)
        {
            Form form = formRepository.GetById(formId);
            FormsQuestionGroup formsQuestionGroup = new FormsQuestionGroup();
            //formsQuestionGroup.Form_Id = (int)formId;
            ViewBag.Form_Id = formId; 
            ViewBag.FormTitle = form.Title;
            ViewBag.FormTypeId = form.FormTypeId;
            return View(formsQuestionGroup);
        }

        // POST: FormsQuestionGroups/Create
        [HttpPost]
        public ActionResult Create(FormsQuestionGroup formsQuestionGroup)
        {
            if (ModelState.IsValid)
            {
                //formsQuestionGroup.Enabled = true;
                formsQuestionGroupRepository.Insert(formsQuestionGroup);
                // return RedirectToAction("Index", new { id = formsQuestionGroup.Form_Id });
                return RedirectToAction("Index", new { id = formsQuestionGroup });
            }
            return View(formsQuestionGroup);
        }

        // GET: FormsQuestionGroups/Edit/5
        public ActionResult Edit(int id)
        {
            FormsQuestionGroup formsQuestionGroup = formsQuestionGroupRepository.GetById(id);
            if (formsQuestionGroup == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Form_Id = new SelectList(formRepository.Table, "Id", "Title", formsQuestionGroup.Form_Id);
            //ViewBag.FormTitle = formsQuestionGroup.Form.Title;
            //ViewBag.FormTypeId = formsQuestionGroup.Form.FormTypeId;
            return View(formsQuestionGroup);
        }

        // POST: FormsQuestionGroups/Edit/5
        [HttpPost]
        public ActionResult Edit(FormsQuestionGroup formsQuestionGroup)
        {
            if (ModelState.IsValid)
            {
                formsQuestionGroupRepository.Update(formsQuestionGroup);
                //return RedirectToAction("Index", new { id = formsQuestionGroup.Form_Id });
                return RedirectToAction("Index", new { id = formsQuestionGroup});
            }
            //ViewBag.Form_Id = new SelectList(formRepository.Table, "Id", "Title", formsQuestionGroup.Form_Id);
            //ViewBag.FormTitle = formsQuestionGroup.Form.Title;
            //ViewBag.FormTypeId = formsQuestionGroup.Form.FormTypeId;
            return View(formsQuestionGroup);
        }

        // GET: FormsQuestionGroups/Delete/5
        public ActionResult Delete(int? id)
        {
            FormsQuestionGroup formsQuestionGroup = formsQuestionGroupRepository.GetById(id);
            if (formsQuestionGroup == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Form_Id = new SelectList(formRepository.Table, "Id", "Title", formsQuestionGroup.Form_Id);
            //ViewBag.FormTitle = formsQuestionGroup.Form.Title;
            //ViewBag.FormTypeId = formsQuestionGroup.Form.FormTypeId;
            return View(formsQuestionGroup);
        }

        // POST: FormsQuestionGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormsQuestionGroup formsQuestionGroup = formsQuestionGroupRepository.GetById(id);
            //formsQuestionGroup.Enabled = false;
            formsQuestionGroupRepository.Update(formsQuestionGroup);
            //return RedirectToAction("Index", new { id = formsQuestionGroup.Form_Id });
            return RedirectToAction("Index", new { id = formsQuestionGroup});
        }
    }
}
