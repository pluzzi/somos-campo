﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;

namespace QavantWeb.Controllers
{
    public class PuestosController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Puestos
        public ActionResult Index(int id)
        {
            ViewData["userId"] = id;
            var puestosUsers = db.PuestosUsers
                                .Include(p => p.Puesto)
                                .Include(p => p.User)
                                .Where(o => o.Puesto.EmpresaId == empresaId)
                                .Where(p => p.UserId == id)
                                .OrderByDescending(x => x.FechaDesde);
            return View(puestosUsers.ToList());
        }
        // GET: IndexPuestos
        public ActionResult IndexPuestos()
        {
            var puestos = db.Puestos.Include(c => c.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(puestos);
        }
        // GET: Puestos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PuestosUser puestosUser = db.PuestosUsers.Find(id);
            if (puestosUser == null)
            {
                return HttpNotFound();
            }
            return View(puestosUser);
        }

        // GET: Puestos/Create
        public ActionResult Create(int? id)
        {
            ViewData["uId"] = id;
            ViewBag.PuestoId = new SelectList(db.Puestos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado), "Id", "Descripcion");
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", id);
            return View();
        }

        // GET: Puestos/CreatePuestos
        public ActionResult CreatePuestos()
        {
            return View();
        }

        // POST: Puestos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PuestosUser puestosUser)
        {
            if (ModelState.IsValid)
            {
                db.PuestosUsers.Add(puestosUser);
                db.SaveChanges();
                return RedirectToAction("Index", "Puestos", new { id = puestosUser.UserId });
            }

            ViewBag.PuestoId = new SelectList(db.Puestos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", puestosUser.PuestoId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", puestosUser.UserId);
            return View(puestosUser);
        }

        // POST: Puestos/CreatePuestos
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePuestos(Puesto puesto)
        {
            if (ModelState.IsValid)
            {
                puesto.EmpresaId = empresaId;
                db.Puestos.Add(puesto);
                db.SaveChanges();
                return RedirectToAction("IndexPuestos");
            }
            var puestos = db.Puestos.Include(r => r.Empresa).Where(w => w.EmpresaId == empresaId);
            return View(puestos);
        }

        // GET: Puestos/Edit/5
        public ActionResult Edit(int? id, int? uId)
        {
            if (id == null || uId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;

            PuestosUser puestosUser = db.PuestosUsers.Where(w=>w.Puesto.EmpresaId == empresaId).Single(p => p.Id == id);
            if (puestosUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.PuestoId = new SelectList(db.Puestos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", puestosUser.PuestoId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", puestosUser.UserId);
            return View(puestosUser);
        }

        // GET: Puestos/EditPuestos/1
        public ActionResult EditPuestos(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puesto puesto = db.Puestos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (puesto == null)
            {
                return HttpNotFound();
            }
            return View(puesto);
        }

        // POST: Puestos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PuestosUser puestosUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(puestosUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Puestos", new { id = puestosUser.UserId });
            }
            ViewBag.PuestoId = new SelectList(db.Puestos.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", puestosUser.PuestoId);
            ViewBag.UserId = new SelectList(db.Users.Where(w => w.EmpresaId == empresaId), "Id", "Name", puestosUser.UserId);
            return View(puestosUser);
        }
        // POST: Puestos/EditPuestos/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPuestos(Puesto puesto)
        {
            if (puesto.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            if (ModelState.IsValid)
            {
                db.Entry(puesto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexPuestos");
            }
            ViewBag.EmpresaId = new SelectList(db.Empresas, "Id", "Nombre", puesto.EmpresaId);
            return View(puesto);
        }


        // GET: Puestos/Delete/5
        public ActionResult Delete(int? id, int? uId)
        {
            if (id != 0)
            {
                if (db.PuestosUsers.FirstOrDefault(f => f.Id == id).Puesto.EmpresaId != empresaId)
                    //Intenton hackear por empresaId
                    return RedirectToAction("Index");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewData["myUserId"] = id;
            ViewData["uId"] = uId;

            PuestosUser puestosUser = db.PuestosUsers.Where(w=>w.Puesto.EmpresaId == empresaId).FirstOrDefault(f=>f.Id == id);
            if (puestosUser == null)
            {
                return HttpNotFound();
            }
            db.PuestosUsers.Remove(puestosUser);
            db.SaveChanges();
            return RedirectToAction("Index", "Puestos", new { id = puestosUser.UserId });
            
        }

        // GET: Puestos/DeletePuestos/5
        public ActionResult DeletePuestos(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puesto puesto = db.Puestos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (puesto == null)
            {
                return HttpNotFound();
            }
            return View(puesto);
        }

        // POST: Puestos/DeletePuestosConfirmed/5
        [HttpPost, ActionName("DeletePuestos")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePuestosConfirmed(int id)
        {
            Puesto puestos = db.Puestos.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if(puestos.PuestosUsers.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar el puesto. El puesto posee usuarios asociados. ");
            }
            else
            {
                db.Puestos.Remove(puestos);
                db.SaveChanges();
                return RedirectToAction("IndexPuestos", "Puestos");
            }
            return View(puestos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
