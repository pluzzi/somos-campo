﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;

namespace QavantWeb.Controllers
{
    public class FormsPerformanceEvaluationController : Controller
    {
        private QavantEntities db = new QavantEntities();

        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Evaluacion(int FormId)
        {
            //FormsPerformanceEvaluation forms = db.FormsPerformanceEvaluations
            //                                        .Include(ins => ins.Form)
            //                                        .Include(i => i.Area)
            //                                        .Include(i => i.Area1)
            //                                        .Include(i => i.User)
            //                                        .Include(i => i.User1)
            //                                        .Where(w => w.Form.Enabled)
            //                                        .Where(w=>w.Form.Id = )
            //                                        .FirstOrDefault();
            List<User> lsUsers = db.Users.Include(i => i.Area).Where(w => w.Enable).OrderBy(o => o.Surname).ThenBy(t => t.Name).ToList();
            ViewBag.Users = lsUsers.Select(s => new
                                                {
                                                    s.Id,
                                                    Name = s.Surname + ", " + s.Name
                                                });
            ViewBag.Evaluators = lsUsers.Where(w => w.IsEvaluator).Select(s => new
                                                                        {
                                                                            s.Id,
                                                                            Name = s.Surname + ", " + s.Name
                                                                        }); ;


            //FormsPerformanceEvaluation forms = new FormsPerformanceEvaluation();
            //forms.Evaluated_Id = 1; //TODO: A reemplazar por el id del usuario logueado, cuando el login se unifique con el de la APP
            //forms.User = lsUsers.Where(w => w.Id == forms.Evaluated_Id).FirstOrDefault();
            //forms.EvaluatedArea_Id = (int)forms.User.Area_Id;
            //forms.Form = 
            var forms = db.Forms.Where(w => w.Enabled)
                                .Where(w => w.Id == FormId)
                                .FirstOrDefault();
            //forms.StartedDate = DateTime.Now;
            return View(forms);
        }
    }
}