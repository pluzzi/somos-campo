﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Excel = Microsoft.Office.Interop.Excel;
using QavantWeb.Models;
using System.Diagnostics;

namespace QavantWeb.Controllers
{
    public class PuntosController : Controller
    {
        private QavantEntities db = new QavantEntities();

        // GET: Puntos
        public ActionResult Index()
        {
            ViewBag.Lista = new List<PuntoExcelRow>();
            return View();
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelFile)
        {
            ViewBag.Lista = new List<PuntoExcelRow>();

            if (excelFile == null || excelFile.ContentLength == 0)
            {
                ViewBag.Error = "Por favor seleccione un archivo Excel.";
                return View("Index");
            }
            if(!(excelFile.FileName.EndsWith("xls") || excelFile.FileName.EndsWith("xlsx")))
            {
                ViewBag.Error = "Por favor seleccione un archivo con formato Excel.";
                return View("Index");
            }

            string path = Server.MapPath("~/ExcelFiles/" + excelFile.FileName);

            if (System.IO.File.Exists(path))
            {
                try
                {
                    System.IO.File.Delete(path);
                }catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                
            }

            excelFile.SaveAs(path);

            Excel.Application application = new Excel.Application();
            Excel.Workbook workbook = application.Workbooks.Open(path);
            Excel.Worksheet worksheet = workbook.ActiveSheet;
            Excel.Range range = worksheet.UsedRange;
            List<PuntoExcelRow> list = new List<PuntoExcelRow>();

            for(int r = 2; r <= range.Rows.Count; r++)
            {
                PuntoExcelRow item = new PuntoExcelRow();
                item.User = ((Excel.Range)range.Cells[r, 1]).Text;
                item.Catalogo = ((Excel.Range)range.Cells[r, 2]).Text;
                item.Puntos = decimal.Parse(((Excel.Range)range.Cells[r, 3]).Text);

                User user = db.Users.Where(w => w.UserName == item.User).FirstOrDefault();
                if (user == null)
                {
                    item.Mensaje = "No se encontró el usuario.";
                    list.Add(item);
                    continue;
                }

                TipoCatalogoProducto catalogo = db.TipoCatalogoProductos.Where(w => w.Descripcion== item.Catalogo).FirstOrDefault();
                if (catalogo == null)
                {
                    item.Mensaje = "No se encontró el catálogo.";
                    list.Add(item);
                    continue;
                }

                TipoCatalogoProductosxUsuario catalogoUsuario = db.TipoCatalogoProductosxUsuarios
                    .Where(w => w.UserId == user.Id && w.TipoCatalogoProductoId == catalogo.Id)
                    .FirstOrDefault();

                if (catalogoUsuario == null)
                {
                    TipoCatalogoProductosxUsuario nuevo = new TipoCatalogoProductosxUsuario();
                    nuevo.UserId = user.Id;
                    nuevo.TipoCatalogoProductoId = catalogo.Id;
                    nuevo.PuntosAcumulados = item.Puntos;
                    nuevo.FechaActualizacionPuntos = DateTime.Now;

                    db.TipoCatalogoProductosxUsuarios.Add(nuevo);
                    db.SaveChanges();

                }
                else
                {
                    catalogoUsuario.PuntosAcumulados = item.Puntos;
                    catalogoUsuario.FechaActualizacionPuntos = DateTime.Now;
                    db.SaveChanges();
                }
                
                list.Add(item);
            }

            workbook.Close();
            application.Quit();


            ViewBag.Lista = list;
            
            return View("Index");
        }
    }
}