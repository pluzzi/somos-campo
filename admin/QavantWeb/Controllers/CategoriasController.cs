﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using QavantWeb.Models;
using QavantWeb.Common;

namespace QavantWeb.Controllers
{
    public class CategoriasController : Controller
    {

        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        // GET: Categorias
        public ActionResult Index()
        {
            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList();
            foreach (var item in list)
            {
                switch (item.Modulo.Nemonico)
                {
                    case "INFUT":
                        item.Modulo.Nombre = "Info Útil";
                        break;
                    case "GALER":
                        item.Modulo.Nombre = "Galerías";
                        break;
                }
            }
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

            return View(lsCat);
        }

        // GET: Categorias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        public JsonResult GetModulos()
        {
            //var modulos = db.Modulos.Where(w => w.UsoEnCategorias).Where(w => w.Empresas.Any(an => an.Id == empresaId)).ToList();
            var modulos = db.Modulos.Where(w => w.UsoEnCategorias).ToList();
            dynamic data = new List<dynamic>();
            foreach (var item in modulos)
            {
                switch (item.Nemonico)
                {
                    case "INFUT":
                        item.Nombre = "Info Útil";
                        break;
                    case "GALER":
                        item.Nombre = "Galerías";
                        break;
                }
                data.Add(new
                {
                    Id = item.Id,
                    Nombre = item.Nombre
                });
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCategoriasByModulo(int moduloId)
        {
            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias.Where(w => w.EmpresaId == empresaId).Where(c => c.Habilitada && c.ModuloId == moduloId).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

            dynamic data = new List<dynamic>();
            foreach (var item in lsCat)
            {
                data.Add(new
                {
                    Id = item.Id,
                    Nombre = item.Nombre
                });
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoriaById(int id)
        {
            if (id == 0)
                throw new Exception("Id de categoría no válido.");

            Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            if (categoria == null)
                throw new Exception("Categoría no existente.");

            //recuperamos categorias
            List<Categoria> categoriasAux = new List<Categoria>();
            List<Categoria> list = db.Categorias.Where(w => w.EmpresaId == empresaId).Where(c => c.Habilitada && c.ModuloId == categoria.ModuloId).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref categoriasAux, null, "", true);
            dynamic categoriasList = new List<dynamic>();
            foreach (var item in categoriasAux)
            {
                categoriasList.Add(new
                {
                    Id = item.Id,
                    Nombre = item.Nombre
                });
            }

            //recuperamos modulos
            dynamic modulosList = new List<dynamic>();
            var modulos = db.Modulos.Where(w => w.UsoEnCategorias).ToList();
            foreach (var item in modulos)
            {
                switch (item.Nemonico)
                {
                    case "INFUT":
                        item.Nombre = "Info Útil";
                        break;
                    case "GALER":
                        item.Nombre = "Galerías";
                        break;
                }
                modulosList.Add(new
                {
                    Id = item.Id,
                    Nombre = item.Nombre
                });

            }

            return Json(
                new
                {
                    ModuloId = categoria.ModuloId,
                    CategoriaPadreId = (categoria.PadreCategoriaId != null) ? categoria.PadreCategoriaId : 0,
                    CategoriaNombre = categoria.Nombre,
                    Modulos = modulosList,
                    Categorias = categoriasList
                },
                JsonRequestBehavior.AllowGet);
        }

        // GET: Categorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categoria categoria)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (categoria.PadreCategoriaId == 0)
                        categoria.PadreCategoriaId = null;
                    categoria.Habilitada = true;
                    categoria.EmpresaId = empresaId;
                    db.Categorias.Add(categoria);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
            }
            return View(categoria);
        }

        // GET: Categorias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            if (categoria == null)
            {
                return HttpNotFound();
            }

            switch (categoria.Modulo.Nemonico)
            {
                case "INFUT":
                    categoria.Modulo.Nombre = "Info Útil";
                    break;
                case "GALER":
                    categoria.Modulo.Nombre = "Galerías";
                    break;
            }

            return View(categoria);
        }

        // POST: Categorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Categoria categoria)
        {
            //que no sea el padre
            try
            {

                if (categoria.EmpresaId != empresaId)
                {
                    ModelState.AddModelError("", "Error al actualizar el registro.");
                }
                if (categoria.Id == categoria.PadreCategoriaId)
                {
                    ModelState.AddModelError("", "La categoría no puede ser padre de sí misma.");
                }

                if (ModelState.IsValid)
                {
                    if (categoria.PadreCategoriaId == 0)
                        categoria.PadreCategoriaId = null;
                    categoria.Habilitada = true;
                    db.Entry(categoria).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
            }
            return View(categoria);
        }

        // GET: Categorias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).Single(s => s.Id == id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            switch (categoria.Modulo.Nemonico)
            {
                case "INFUT":
                    categoria.Modulo.Nombre = "Info Útil";
                    break;
                case "GALER":
                    categoria.Modulo.Nombre = "Galerías";
                    break;
            }
            return View(categoria);
        }

        // POST: Categorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Categoria categoria = db.Categorias.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            try
            {
                var tieneSubCat = db.Categorias.Count(cat => cat.Habilitada && cat.PadreCategoriaId == id) > 0;
                var tieneDocs = db.Documents.Count(doc => doc.Enabled && doc.CategoriaId == id) > 0;
                var tieneInfoUtil = db.InfoUtils.Count(info => info.Habilitado.Value && info.CategoriaId.Value == id) > 0;
                var tieneCursos = db.Courses.Count(course => course.Enabled && course.CategoriaId == id) > 0;
                var tieneBeneficios = db.BenefitCategories.Count(bc => bc.Benefit.Enabled && bc.CategoriaId == id) > 0;
                var tieneEvaluaciones = db.Forms.Count(f => f.FormTypeId == (int)Enums.FormsTypes.Evaluacion && f.Enabled && f.CategoriaId == id) > 0;
                var tieneEncuestas = db.Forms.Count(f => f.FormTypeId == (int)Enums.FormsTypes.Encuesta && f.Enabled && f.CategoriaId == id) > 0;
                var tieneFormsGenerales = db.Forms.Count(f => f.FormTypeId == (int)Enums.FormsTypes.Formulario && f.Enabled && f.CategoriaId == id) > 0;

                if (tieneSubCat)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene subcategorías asociadas.");
                }
                if (tieneDocs)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene documentos asociados.");
                }
                if (tieneInfoUtil)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene informaciones útiles asociadas.");
                }
                if (tieneCursos)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene capacitaciones asociadas.");
                }
                if (tieneBeneficios)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene beneficios asociados.");
                }
                if (tieneEvaluaciones)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene formularios de evaluación asociados.");
                }
                if (tieneEncuestas)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene formularios de encuesta asociados.");
                }
                if (tieneFormsGenerales)
                {
                    ModelState.AddModelError("", "No se puede eliminar la categoría. La categoría tiene formularios generales asociados.");
                }

                if (ModelState.IsValid)
                {
                    categoria.Habilitada = false;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
            }
            return View(categoria);
        }


        // GET: cat        
        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted()
        {
 
            List<Categoria> list = db.Categorias.Where(w => w.EmpresaId == empresaId).Where(x => x.Habilitada == false).ToList();
            foreach (var item in list)
            {
                switch (item.Modulo.Nemonico)
                {
                    case "INFUT":
                        item.Modulo.Nombre = "Info Útil";
                        break;
                    case "GALER":
                        item.Modulo.Nombre = "Galerías";
                        break;
                }
            }


            return View(list);
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            try
            {
                Categoria categ = db.Categorias.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
                categ.Habilitada = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
            }
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
