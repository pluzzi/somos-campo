﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using QavantWeb.Functions;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using QavantWeb.Common;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.Services.Settings;

namespace QavantWeb.Controllers
{
    public class UsersController : Controller
    {

        private string FotoPorDefectoMasculino = WebConfigurationManager.AppSettings["FotoPorDefectoMasculino"].ToString();
        private string FotoPorDefectoFemenino = WebConfigurationManager.AppSettings["FotoPorDefectoFemenino"].ToString();

        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public UsersController() : this(new SettingsService(Enums.Modulos.MiPerfil), new FileStorageFactory()) { }

        public UsersController(ISettingsService settingsService, IFileStorageFactory fileStorageFactory)
        {
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
        }

        // GET: Users
        [Authorize]
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            return View();
        }

        private string FormatDate(DateTime? dt)
        {
            string formatted = string.Empty;
            if (dt != null)
            {
                formatted = ((DateTime)dt).ToString("dd/MM/yyyy");
            }
            return formatted;
        }

        //return Json data
        [HttpPost]
        public ActionResult LoadFullData()
        {
            var a = 1;
            dynamic data = new List<dynamic>();
            try
            {


                using (QavantEntities context = new QavantEntities())
                {
                    List<Catalogo> catalogo = context.Catalogos.ToList();
                    List<User> users = context.Users.Where(w => w.EmpresaId == empresaId).Where(u => u.Enable == true)
                        //.Take(2)
                        .Include(i => i.Regione)
                        .Include(i => i.Compania)
                        .Include(i => i.Sucursale)
                        .Include(i => i.Area)  //FK
                        .Include(i => i.Familiares)
                        .Include(i => i.Estudios)

                        .ToList();

                    foreach (var user in users)
                    {
                        dynamic familiaresAux = new List<dynamic>();
                        foreach (var familiar in user.Familiares)
                        {
                            familiaresAux.Add(new
                            {
                                Nombre = familiar.Nombre,
                                Apellido = familiar.Apellido,
                                Vinculo = catalogo.Single(c => c.Id == familiar.VinculoCatalogoId).DescripcionLarga,
                                Genero = catalogo.Single(c => c.Id == familiar.GeneroCatalogoId).DescripcionLarga,
                                FechaNacimiento = FormatDate(familiar.FechaNacimiento)
                            });
                        }

                        dynamic estudiosAux = new List<dynamic>();
                        foreach (var estudio in user.Estudios)
                        {
                            estudiosAux.Add(new
                            {
                                Tipo = catalogo.Single(c => c.Id == estudio.TipoEstudioCatalogoId).DescripcionLarga,
                                Establecimiento = estudio.Establecimiento.Nombre,
                                Titulo = estudio.Titulo.Descripcion,
                                Desde = FormatDate(estudio.FechaDesde),
                                Hasta = FormatDate(estudio.FechaHasta),
                                Completo = estudio.EstudioCompleto
                            });
                        }

                        string puestoActual = string.Empty;
                        var existePuesto = user.PuestosUsers.OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
                        if (existePuesto != null)
                        {
                            puestoActual = existePuesto.Puesto.Descripcion;
                        }

                        //.OrderByDescending(x => x.Delivery.SubmissionDate);

                        data.Add(new
                        {
                            Nombre = user.Name,
                            Apellido = user.Surname,
                            FechaNacimiento = FormatDate(user.BirthDate),
                            Documento = user.Dni,
                            Celular = user.Mobile,
                            TelefonoParticular = user.TelefonoPersonal,
                            TelefonoUrgencia = user.TelefonoUrgencias,
                            Genero = user.Genero,
                            Direccion = user.Address,
                            Localidad = user.City,

                            LaboralRegion = user.Regione.Descripcion,
                            LaboralCompania = user.Compania.Descripcion,
                            LaboralDireccion = user.Compania?.Descripcion,
                            LaboralSucursal = user.Sucursale?.Descripcion,
                            LaboralArea = user.Area?.Nombre,
                            LaboralPuesto = puestoActual,
                            LaboralEmail = user.Email,
                            LaboralFechaIngreso = FormatDate(user.FechaIngreso),
                            LaboralFechaEgreso = FormatDate(user.FechaEgreso),
                            LaboralLegajo = user.FileNumber,
                            LaboralCUIL = user.Cuil,
                            LaboralTelefono = user.Phone,
                            LaboralInterno = user.Intern,

                            AppUsuario = user.UserName,
                            AppActivo = user.Active,

                            Familiares = familiaresAux,
                            Estudios = estudiosAux
                        });
                    }
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                a = 2;
            }
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        //return Json data
        [HttpPost]
        public ActionResult LoadData(string activeFilter)
        {
            if (activeFilter == null) { activeFilter = "0"; }

            var draw = Request.Form.GetValues("draw").FirstOrDefault();//Datatable parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();//paging parameter
            var length = Request.Form.GetValues("length").FirstOrDefault();//paging parameter           
            var sortColumn = Request.Form.GetValues("order[0][column]")[0];
            var sortColumnDir = Request.Form.GetValues("order[0][dir]")[0];
            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();//filter parameter

            List<User> allUsers = new List<User>();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;

            using (QavantEntities dc = new QavantEntities())
            {
                dc.Configuration.LazyLoadingEnabled = false;
                var v = dc.Users.Where(w => w.EmpresaId == empresaId).Include(x => x.Area).Where(x => x.Enable).ToList();

                if (!string.IsNullOrEmpty(searchValue))
                {
                    v = v.Where(p => p.Name.ToString().ToLower().Contains(searchValue.ToLower()) ||
                                     p.Surname.ToString().ToLower().Contains(searchValue.ToLower())
                               ).ToList();
                }

                switch (activeFilter)
                {
                    case "1":
                        v = v.Where(x => x.Active == true).ToList();
                        break;
                    case "2":
                        v = v.Where(x => x.Active == false).ToList();
                        break;
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))//Sort
                {
                    switch (sortColumn)
                    {
                        case "0":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Surname).ToList();
                            else
                                v = v.OrderByDescending(o => o.Surname).ToList();
                            break;
                        case "1":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Image).ToList();
                            else
                                v = v.OrderByDescending(o => o.Image).ToList();
                            break;
                        case "2":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Email).ToList();
                            else
                                v = v.OrderByDescending(o => o.Email).ToList();
                            break;
                        case "3":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Area_Id).ToList();
                            else
                                v = v.OrderByDescending(o => o.Area_Id).ToList();
                            break;
                        case "4":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Phone).ToList();
                            else
                                v = v.OrderByDescending(o => o.Phone).ToList();
                            break;
                        case "5":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Intern).ToList();
                            else
                                v = v.OrderByDescending(o => o.Intern).ToList();
                            break;
                        case "6":
                            if (sortColumnDir == "asc")
                                v = v.OrderBy(o => o.Mobile).ToList();
                            else
                                v = v.OrderByDescending(o => o.Mobile).ToList();
                            break;
                    }
                }

                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pageSize).ToList();

                var parametros = _settingsService.GetParametrosGlobales();
                var colorPrincipal = "#808080";

                foreach (var item in parametros)
                {
                    if (item.Key == "ColorPrincipal")
                    {
                        colorPrincipal = item.Value;
                    }

                }
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].Image == "")
                    {
                        var primerLetraNombre = data[i].Name.Trim().RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                        primerLetraNombre = primerLetraNombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = data[i].Surname.TrimEnd().TrimStart();
                        primerLetraApellido = primerLetraApellido.RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                        primerLetraApellido = primerLetraApellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraNombre + primerLetraApellido;
                        data[i].Image = "<div class='contenedor-texto'><div class='circular-shadow' style='background-color:" + colorPrincipal + "'></div><div class='texto-centrado'>" + texto + "</div></div>";
                    }
                    else
                    {
                        data[i].Image = "<img src=" + data[i].Image + " style='max-width:100px;' class='thumbnail' />";
                    }

                    if (data[i].Area == null)
                        data[i].Area = new Area();
                    else
                    {
                        data[i].Area.Users = null;
                        data[i].Area.AreaPadreId = null;
                        data[i].Area.Areas1 = null;
                        data[i].Area.Area1 = null;
                    }
                    data[i].Name = data[i].Surname + " " + data[i].Name;
                }
                var dataToReturn = data.Select(p => new { p.Id, p.Email, p.Image, p.Name, p.Area, p.Phone, p.Intern, p.Mobile, p.Active });

                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = dataToReturn }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Users/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            User user = new User();
            ViewBag.lstEstudios = db.Estudios.ToList();
            ViewBag.listaEstablecimientos = db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado).ToList();

            ViewBag.RegionId = new SelectList(db.Regiones.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.RegionId);
            ViewBag.CompaniaId = new SelectList(db.Companias.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.CompaniaId);
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.SucursalId);
            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);

            ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id).ToList();

            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catId = 1;
            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catId = item.id;
                    break;
                }
            }
            List<Catalogo> lstCatalogo = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            else
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogo, "Id", "DescripcionLarga");

            return View();
        }

        public static List<string> GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;

            var errorList = query.ToList();
            return errorList;
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(User user, int? listaPuestosId)//, int listaDirecId, int Area_Id)
        {
            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);
            try
            {
                user.IsEvaluator = false;
                user.Enable = true;
                ViewBag.lstEstudios = db.Estudios.Where(w => w.UserId == user.Id).ToList();
                ViewBag.listaEstablecimientos = db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado).ToList();
                ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id).ToList();

                string archivo;

                if (user.Area_Id == 0 || user.Area_Id == null)
                {
                    ModelState.AddModelError("", "Ingrese Área");
                }

                if (ModelState.IsValid)
                {
                    var Existe = db.Users.Where(w => w.EmpresaId == empresaId).Any(x => x.UserName == user.UserName);
                    //verificar que el usuario no exista
                    if (!Existe) //si no existe un usuario con ese nombre
                    {
                        HttpPostedFileBase file = Request.Files["ImageFile"];
                        if (file != null && file.ContentLength != 0)
                        {
                            user.Image = SaveFile(file);
                        }
                        else//si no cargó una imagen
                        {
                            user.Image = "";
                        }
                        user.RegistrationDate = DateTime.Now;
                        user.EmpresaId = empresaId;
                        var gen = db.Catalogos.Where(w => w.Id == user.GeneroCatalogoId && w.Habilitado == true).FirstOrDefault();
                        user.Genero = gen.DescripcionCorta;
                        db.Users.Add(user);
                        db.SaveChanges();
                        int scopeid = user.Id; // recupero el id asignado por la bd                    
                        db.SaveChanges();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        var checkWichList = db.Users.Where(w => w.EmpresaId == empresaId).Any(x => x.Enable && x.UserName == user.UserName);
                        if (checkWichList)
                        {
                            ModelState.AddModelError("", "El usuario " + user.UserName + " ya existe, ingrese un nombre de usuario diferente.");
                        }
                        else
                        {
                            ModelState.AddModelError("", "El usuario " + user.UserName + " ya existe, verifique en la lista de usuarios eliminados.");
                        }
                        ViewBag.RegionId = new SelectList(db.Regiones.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.RegionId);
                        ViewBag.CompaniaId = new SelectList(db.Companias.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.CompaniaId);
                        ViewBag.SucursalId = new SelectList(db.Sucursales.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.SucursalId);
                        ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id);
                        List<CatalogosTipo> listCatDefault = db.CatalogosTipoes.ToList();
                        List<CatalogosTipo> listCatCustom = new List<CatalogosTipo>();
                        int categId = 1;
                        foreach (CatalogosTipo item in listCatDefault)
                        {
                            if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                            {
                                listCatCustom.Add(item);
                                categId = item.id;
                                break;
                            }
                        }
                        List<Catalogo> listCatalogo = new List<Catalogo>();
                        if (listCatCustom.Count() > 0)
                            listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
                        else
                            listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
                        ViewBag.GeneroCatalogoId = new SelectList(listCatalogo, "Id", "DescripcionLarga");
                        return View(user);
                    }
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
            }

            ViewBag.RegionId = new SelectList(db.Regiones.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.RegionId);
            ViewBag.CompaniaId = new SelectList(db.Companias.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.CompaniaId);
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(w => w.EmpresaId == empresaId), "Id", "Descripcion", user.SucursalId);
            ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id);
            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catId = 1;
            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catId = item.id;
                    break;
                }
            }
            List<Catalogo> lstCatalogo = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            else
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogo, "Id", "DescripcionLarga");
            return View(user);
        }

        // GET: Users/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            List<CatalogosTipo> lstCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> lstCatCustom = new List<CatalogosTipo>();
            int catId = 1;
            foreach (CatalogosTipo item in lstCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    lstCatCustom.Add(item);
                    catId = item.id;
                    break;
                }
            }
            List<Catalogo> lstCatalogo = new List<Catalogo>();
            if (lstCatCustom.Count() > 0)
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            else
                lstCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == catId && w.Habilitado == true).ToList();
            ViewBag.GeneroCatalogoId = new SelectList(lstCatalogo, "Id", "DescripcionLarga");
            ViewData["uId"] = id;
            ViewBag.lstEstudios = db.Estudios.Where(w => w.UserId == id).ToList();
            ViewBag.listaEstablecimientos = db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado).ToList();

            List<Puesto> listaPuestos = db.Puestos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado).ToList();
            ViewBag.listaPuestos = listaPuestos;

            List<Area> lsArea = new List<Area>();
            Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada).ToList(), ref lsArea, null, "", true);

            ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre").ToList();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (user == null)
            {
                return HttpNotFound();
            }

            user.PuestosUsers = user.PuestosUsers.OrderByDescending(x => x.FechaDesde).ToList();

            ViewBag.RegionId = new SelectList(db.Regiones.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.RegionId);
            ViewBag.SucursalId = new SelectList(db.Sucursales.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.SucursalId);
            ViewBag.CompaniaId = new SelectList(db.Companias.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.CompaniaId);

            List<SelectListItem> Area_Id = new List<SelectListItem>();

            ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id).ToList();
            ViewBag.EvaluatorUser_Id = db.Users
                                        .Where(w => w.EmpresaId == empresaId)
                                         .Where(p => p.Enable && p.IsEvaluator && p.Id != id)
                                         .Select(x => new SelectListItem
                                         {
                                             Value = x.Id.ToString(),
                                             Text = x.Name + " " + x.Surname,
                                             Selected = (x.Id == user.EvaluatorUser_Id)
                                         });
            List<CatalogosTipo> listCatDefault = db.CatalogosTipoes.ToList();
            List<CatalogosTipo> listCatCustom = new List<CatalogosTipo>();
            int categId = 1;
            foreach (CatalogosTipo item in listCatDefault)
            {
                if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                {
                    listCatCustom.Add(item);
                    categId = item.id;
                    break;
                }
            }
            List<Catalogo> listCatalogo = new List<Catalogo>();
            if (listCatCustom.Count() > 0)
                listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
            else
                listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
            ViewBag.GeneroCatalogoId = new SelectList(listCatalogo, "Id", "DescripcionLarga", user.GeneroCatalogoId);
            if (user.Image == "")
            {
                var primerLetraNombre = user.Name.RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                primerLetraNombre = primerLetraNombre.TrimEnd().TrimStart().Substring(0, 1).ToUpper();

                var primerLetraApellido = user.Surname.RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                primerLetraApellido = primerLetraApellido.TrimEnd().TrimStart().Substring(0, 1).ToUpper();
                var texto = primerLetraNombre + primerLetraApellido;
                ViewBag.Iniciales = texto;
                var parametros = _settingsService.GetParametrosGlobales();
                var colorPrincipal = "#808080";

                foreach (var item in parametros)
                {
                    if (item.Key == "ColorPrincipal")
                    {
                        colorPrincipal = item.Value;
                    }

                }
                ViewBag.colorPrincipal = colorPrincipal;
            }
            return View(user);
        }

        // POST: Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(User user, HttpPostedFileBase imageFile, string foto, int id, int? listaPuestosId)
        {
            try
            {
                HttpPostedFileBase file = Request.Files["ImageFile"];
                if (user.EmpresaId != empresaId)
                    ModelState.AddModelError("", "Error al actualizar el registro.");

                var userNameDistinto = db.Users.Where(w => w.EmpresaId == empresaId).Any(x => x.UserName != user.UserName && x.Id == id);
                if (userNameDistinto)
                {
                    var checkWichList = db.Users.Where(w => w.EmpresaId == empresaId).Any(x => x.Enable && x.UserName == user.UserName);
                    if (checkWichList)
                    {
                        ModelState.AddModelError("", "El usuario " + user.UserName + " ya existe, ingrese un nombre de usuario diferente.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "El usuario " + user.UserName + " ya existe, verifique en la lista de usuarios eliminados o ingrese un nombre de usuario diferente.");
                    }
                }

                string archivo;
                if (!user.Enable) //Si el usuario no está habilitado,elimino los datos del device
                {
                    var userDevice = db.Devices.Where(w => w.User.EmpresaId == empresaId).Where(w => w.UserId == user.Id).FirstOrDefault();
                    if (userDevice != null)
                    {
                        db.Devices.Remove(userDevice);
                        db.SaveChanges();
                    }
                }

                if (ModelState.IsValid)
                {

                    var gen = db.Catalogos.Where(w => w.Id == user.GeneroCatalogoId && w.Habilitado == true).FirstOrDefault();
                    user.Genero = gen.DescripcionCorta;

                    //HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {   //El usuario cambia la imagen -  sube y guarda uno nuevo.
                        user.Image = SaveFile(file);
                    }
                    else
                    {
                        if (foto != null && foto != "")
                            user.Image = foto;
                        else
                            user.Image = "";
                    }

                    user.RegistrationDate = DateTime.Now;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                    User userEdit = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
                    if (userEdit == null)
                    {
                        return HttpNotFound();
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    //HttpPostedFileBase file = Request.Files["ImageFile"];

                    if (file != null && file.ContentLength != 0)
                    {
                        user.Image = SaveFile(file);
                    }
                    else
                    {
                        if (foto != null && foto != "")
                            user.Image = foto;
                        else
                            user.Image = "";
                    }
                    ViewBag.listaEstablecimientos = db.Establecimientos.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitado).ToList();
                    ViewBag.RegionId = new SelectList(db.Regiones.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.RegionId);
                    ViewBag.SucursalId = new SelectList(db.Sucursales.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.SucursalId);
                    ViewBag.CompaniaId = new SelectList(db.Companias.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada), "Id", "Descripcion", user.CompaniaId);
                    List<Area> lsArea = new List<Area>();
                    Common.FormatterHelpers.AreasToTree(db.Areas.Where(w => w.EmpresaId == empresaId).Where(w => w.Habilitada || user.Area_Id == w.Id).ToList(), ref lsArea, null, "", true);
                    ViewBag.Area_Id = new SelectList(lsArea, "Id", "Nombre", user.Area_Id);
                    ViewBag.EvaluatorUser_Id = db.Users.Where(w => w.EmpresaId == empresaId).Where(p => p.Enable && p.IsEvaluator && p.Id != id).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name + " " + x.Surname, Selected = (x.Id == user.EvaluatorUser_Id) });
                    List<CatalogosTipo> listCatDefault = db.CatalogosTipoes.ToList();
                    List<CatalogosTipo> listCatCustom = new List<CatalogosTipo>();
                    int categId = 1;
                    foreach (CatalogosTipo item in listCatDefault)
                    {
                        if (item.EmpresaId == empresaId && item.Nemonico == "GENE")
                        {
                            listCatCustom.Add(item);
                            categId = item.id;
                            break;
                        }
                    }
                    List<Catalogo> listCatalogo = new List<Catalogo>();
                    if (listCatCustom.Count() > 0)
                        listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
                    else
                        listCatalogo = db.Catalogos.Where(w => w.CatalogoTipoId == categId && w.Habilitado == true).ToList();
                    ViewBag.GeneroCatalogoId = new SelectList(listCatalogo, "Id", "DescripcionLarga", user.GeneroCatalogoId);
                    var primerLetraNombre = user.Name.Substring(0, 1).ToUpper();
                    var primerLetraApellido = user.Surname.Substring(0, 1).ToUpper();
                    var texto = primerLetraNombre + primerLetraApellido;
                    ViewBag.Iniciales = texto;
                    var parametros = _settingsService.GetParametrosGlobales();
                    var colorPrincipal = "";

                    foreach (var item in parametros)
                    {
                        if (item.Key == "ColorPrincipal")
                        {
                            colorPrincipal = item.Value;
                        }

                    }
                    ViewBag.colorPrincipal = colorPrincipal;
                    return View(user);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message + " - " + ex.StackTrace + " - " + ex.Source);
                return View(user);
            }
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult ResetPassword(int id)
        {
            var user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(x => x.Id == id);

            user.Password = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8);
            user.Active = false;

            List<Device> dev = db.Devices.Where(w => w.User.EmpresaId == empresaId).Where(w => w.UserId == user.Id).ToList();

            foreach (Device d in dev)
            {
                db.Devices.Remove(d);
            }

            db.SaveChanges();

            string msj = "<h2>" + System.Configuration.ConfigurationManager.AppSettings["TitleForgotPasswordEmail"] + "</h2>";
            msj += "<p>Para poder activar tu usuario nuevamente, deberás ingresar a la aplicación con tu usuario y el siguiente password temporal.</p>";
            msj += "<p><strong>Password: </strong>" + user.Password + "</p>";
            msj += "<p><strong>IMPORTANTE: </strong> Para poder configurar tu password definitivo, deberás ingresarlo y confirmarlo una vez que la aplicación te lo solicite.</p>";

            MailSender.SendMails(new List<QueueItem>
            {
                new QueueItem(user.Email, msj, System.Configuration.ConfigurationManager.AppSettings["TitleForgotPasswordEmail"])
            });

            return RedirectToAction("Index");
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);

            if (user == null)
            {
                return HttpNotFound();
            }
            if (user.Image == "")
            {
                var parametros = _settingsService.GetParametrosGlobales();
                var colorPrincipal = "#808080";

                foreach (var item in parametros)
                {
                    if (item.Key == "ColorPrincipal")
                    {
                        colorPrincipal = item.Value;
                    }

                }
                ViewBag.colorPrincipal = colorPrincipal;
                var primerLetraNombre = user.Name.Trim().RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                primerLetraNombre = user.Name.Substring(0, 1).ToUpper();
                var primerLetraApellido = user.Surname.TrimEnd().TrimStart();
                primerLetraApellido = primerLetraApellido.RemueveAcentos().SoloLetrasYNumeros().ReemplazaCaracteresEspeciales();
                primerLetraApellido = primerLetraApellido.Substring(0, 1).ToUpper();
                var texto = primerLetraNombre + primerLetraApellido;
                user.Image = "<div class='contenedor-texto'><div class='circular-shadow' style='background-color:" + colorPrincipal + "'></div><div class='texto-centrado'>" + texto + "</div></div>";
            }
            else
            {
                user.Image = "<img src=" + user.Image + " style='max-width:100px;' class='thumbnail' />";
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            user.Enable = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Users
        public ActionResult IndexDeleted()
        {
            return View(db.Users.Where(w => w.EmpresaId == empresaId).Where(x => x.Enable == false).ToList());
        }

        // GET: Users/Enable/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            User user = db.Users.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            user.Enable = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Internal members
        private string SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerUsuarios"];
            return fileStorage.Save(file, filename, container);
        }
        #endregion
    }
}

