﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Common;
using QavantWeb.Models;
using QavantWeb.Services;

namespace QavantWeb.Controllers
{
    public class SucursalesController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private readonly IFiltroService _filtroService;

        public SucursalesController() : this(new FiltroService()) { }

        public SucursalesController(IFiltroService filtroService)
        {
            _filtroService = filtroService;
        }




        // GET: Sucursales
        public ActionResult Index()
        {
            return View(db.Sucursales.Where(w => w.EmpresaId == empresaId).ToList());
        }

        // GET: Sucursales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursale sucursale = db.Sucursales.Find(id);
            if (sucursale == null)
            {
                return HttpNotFound();
            }
            return View(sucursale);
        }

        // GET: Sucursales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sucursales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Sucursale sucursale)
        {
            if (ModelState.IsValid)
            {
                sucursale.EmpresaId = empresaId;
                db.Sucursales.Add(sucursale);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sucursale);
        }

        // GET: Sucursales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursale sucursale = db.Sucursales.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (sucursale == null)
            {
                return HttpNotFound();
            }
            return View(sucursale);
        }

        // POST: Sucursales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Sucursale sucursale)
        {
            if (sucursale.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            if (ModelState.IsValid)
            {
                db.Entry(sucursale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sucursale);
        }

        // GET: Sucursales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sucursale sucursale = db.Sucursales.Where(w => w.EmpresaId == empresaId).FirstOrDefault(w => w.Id == id);
            if (sucursale == null)
            {
                return HttpNotFound();
            }
            return View(sucursale);
        }

        // POST: Sucursales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sucursale sucursal = db.Sucursales.FirstOrDefault(suc => suc.Id == id && suc.EmpresaId == empresaId);

            if (sucursal == null)
            {
                return HttpNotFound();
            }

            // valida usuarios asociados.
            if (sucursal.Users.Count > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar la sucursal ya que posee usuarios asociados.");
            }

            // valida filtros asociados.
            var cantFiltros = _filtroService.GetSegmentationCountByFilterValue(empresaId, Enums.Filtros.Sucursal, id);
            if (cantFiltros > 0)
            {
                ModelState.AddModelError("", "No es posible eliminar la sucursal ya que posee filtros asociados.");
            }

            if (ModelState.IsValid)
            {
                db.Sucursales.Remove(sucursal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sucursal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
