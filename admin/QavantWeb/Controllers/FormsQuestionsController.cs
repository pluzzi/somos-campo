﻿using QavantWeb.Common;
using QavantWeb.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class FormsQuestionsController : BaseController
    {
        private const string QUESTION_WITHOUT_OPTIONS_ERROR_MSG = "Ingrese al menos dos opciones para este tipo de pregunta.";
        private const string ENABLED_MARK = "1";
        private const string DISABLED_MARK = "0";
        readonly QavantEntities db = new QavantEntities();
        readonly Repository<FormsQuestionsOption> formQuestionsOptionRepository;
        readonly Repository<FormsQuestion> formsQuestionRepository;
        readonly Repository<FormsQuestionGroup> formQuestionGroupRepository;
        readonly Repository<Form> formRepository;
        private int empresaId = SecurityHelpers.getEmpresaId();

        public FormsQuestionsController()
        {
            formQuestionsOptionRepository = unitOfWork.Repository<FormsQuestionsOption>();
            formsQuestionRepository = unitOfWork.Repository<FormsQuestion>();
            formQuestionGroupRepository = unitOfWork.Repository<FormsQuestionGroup>();
            formRepository = unitOfWork.Repository<Form>();
        }

        // GET: FormsQuestions
        public ActionResult Index(int? id)
        {
            ViewBag.Form_Id = id;
            var formsQuestions = db.FormsQuestions.Include(f => f.Form).Include(f => f.FormsQuestionsType).Where(w => w.Form.EmpresaId == empresaId).Where(w => w.Form_Id == id && w.Enabled == true);
            ViewBag.FormTitle = db.Forms.Where(w => w.EmpresaId == empresaId).FirstOrDefault(x => x.Id == id).Title;
            ViewBag.FormTypeId = db.Forms.Where(w => w.EmpresaId == empresaId).FirstOrDefault(x => x.Id == id).FormTypeId;
            return View(formsQuestions.ToList());
        }

        public ActionResult Clone(int id)
        {
            var formsQuestion = db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (formsQuestion == null)
            {
                return HttpNotFound();
            }
            formsQuestion.Order = (short)(formsQuestion.Form.FormsQuestions.Count(fq => fq.Enabled) + 1);

            ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId), "Id", "Title", formsQuestion.Form_Id);
            ViewBag.FormTitle = formsQuestion.Form.Title;
            ViewBag.FormTypeId = formsQuestion.Form.FormTypeId;
            //ViewBag.FormsQuestionGroup_Id = new SelectList(db.FormsQuestionGroups.Where(w => w.Form.EmpresaId == empresaId).Where(x => x.Form_Id == formsQuestion.Form_Id && x.Enabled), "Id", "Description", formsQuestion.FormsQuestionGroup_Id);

            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, formsQuestion.Form.FormTypeId), "Id", "Name", formsQuestion.FormsQuestionsType_Id);
            ViewBag.FormsQuestionsTypes = db.FormsQuestionsTypes.ToList();
            ViewBag.FormsQuestionsOptions = formsQuestion.FormsQuestionsOptions.Where(w => w.Enabled).ToList();
            return View(formsQuestion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Clone(FormsQuestion formsQuestion)
        {
            if (ModelState.IsValid)
            {
                var form = formRepository.GetById(formsQuestion.Form_Id);

                formsQuestion.Enabled = true;
                foreach (FormsQuestionsOption fqo in formsQuestion.FormsQuestionsOptions)
                {
                    if (fqo.Habilitado == ENABLED_MARK)
                    {
                        fqo.Enabled = true;
                    }
                    else
                    {
                        fqo.Enabled = false;
                    }
                }

                db.FormsQuestions.Add(formsQuestion);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = formsQuestion.Form_Id });

            }

            ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId), "Id", "Title", formsQuestion.Form_Id);
            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, 0), "Id", "Name", formsQuestion.FormsQuestionsType_Id);

            return View(formsQuestion);
        }

        // GET: FormsQuestions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var formsQuestion = db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            if (formsQuestion == null)
            {
                return HttpNotFound();
            }
            return View(formsQuestion);
        }

        // GET: FormsQuestions/Create
        public ActionResult Create(int? formId)
        {
            Form form = db.Forms.Where(w => w.EmpresaId == empresaId).Where(x => x.Id == formId).FirstOrDefault();
            var formsQuestion = new FormsQuestion
            {
                Form_Id = (int)formId,
                Order = (short)(form.FormsQuestions.Count() + 1)
            };
            ViewBag.Form_Id = formId; //new SelectList(db.Forms, "Id", "Title");
            ViewBag.FormTitle = form.Title;
            ViewBag.FormTypeId = form.FormTypeId;
            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, form.FormTypeId), "Id", "Name");

            return View(formsQuestion);
        }


        private static bool ValidateOptionsCount(FormsQuestion formsQuestion)
        {
            var result = true;
            if (formsQuestion.FormsQuestionsOptions.Count(op => op.Habilitado == ENABLED_MARK) < 2 &&
                (formsQuestion.FormsQuestionsType_Id == Enums.FormsQuestionsTypes.DROPDOWN.ToString() ||
                formsQuestion.FormsQuestionsType_Id == Enums.FormsQuestionsTypes.MULTIPLE.ToString() ||
                formsQuestion.FormsQuestionsType_Id == Enums.FormsQuestionsTypes.OPTION.ToString()) )
                result = false;
            return result;
        }


        // POST: FormsQuestions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormsQuestion formsQuestion, byte FormTypeId, string FormTitle)
        {
            if ( !ValidateOptionsCount(formsQuestion))
            {
                ModelState.AddModelError("", QUESTION_WITHOUT_OPTIONS_ERROR_MSG);
            }

            if (ModelState.IsValid)
            {
                var form = formRepository.GetById(formsQuestion.Form_Id);

                formsQuestion.Enabled = true;
                foreach (FormsQuestionsOption fqo in formsQuestion.FormsQuestionsOptions)
                {
                    if (fqo.Habilitado == ENABLED_MARK)
                    {
                        fqo.Enabled = true;
                    }
                    else
                    {
                        fqo.Enabled = false;
                    }
                }

                db.FormsQuestions.Add(formsQuestion);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = formsQuestion.Form_Id });
            }

            ViewBag.Form_Id = formsQuestion.Form_Id;
            ViewBag.FormTitle = FormTitle;
            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, FormTypeId), "Id", "Name", formsQuestion.FormsQuestionsType_Id);
            ViewBag.FormTypeId = FormTypeId;

            return View(formsQuestion);
        }

        // GET: FormsQuestions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormsQuestion formsQuestion = db.FormsQuestions.Find(id);

            if (formsQuestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.Form_Id = new SelectList(db.Forms, "Id", "Title", formsQuestion.Form_Id);
            ViewBag.FormTitle = formsQuestion.Form.Title;
            ViewBag.FormTypeId = formsQuestion.Form.FormTypeId;
            //ViewBag.FormsQuestionGroup_Id = new SelectList(db.FormsQuestionGroups.Where(w => w.Form.EmpresaId == empresaId).Where(x => x.Form_Id == formsQuestion.Form_Id && x.Enabled), "Id", "Description", formsQuestion.FormsQuestionGroup_Id);

            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, formsQuestion.Form.FormTypeId), "Id", "Name", formsQuestion.FormsQuestionsType_Id);
            ViewBag.FormsQuestionsTypes = db.FormsQuestionsTypes.ToList();
            ViewBag.FormsQuestionsOptions = formsQuestion.FormsQuestionsOptions.Where(w => w.Enabled).ToList();
            return View(formsQuestion);
        }

        // POST: FormsQuestions/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormsQuestion formsQuestion, int FormTypeId)
        {
            if (!ValidateOptionsCount(formsQuestion))
            {
                ModelState.AddModelError("", QUESTION_WITHOUT_OPTIONS_ERROR_MSG);
            }
            if (ModelState.IsValid)
            {
                var lsFqo = formsQuestion.FormsQuestionsOptions.ToList();
                foreach (FormsQuestionsOption fqo in lsFqo)
                {
                    switch (formsQuestion.FormsQuestionsType_Id)
                    {
                        case "TEXT":
                        case "TEXTAREA":
                        case "DATE":
                            fqo.Action = "M";
                            fqo.Habilitado = DISABLED_MARK;
                            break;
                    }

                    if (fqo.Habilitado == ENABLED_MARK)
                    {
                        fqo.Enabled = true;
                    }
                    else
                    {
                        fqo.Enabled = false;
                    }

                    switch (fqo.Action)
                    {
                        case "A":
                            db.Entry(fqo).State = EntityState.Added;
                            break;

                        case "M":
                        case "0":
                            db.Entry(fqo).State = EntityState.Modified;
                            break;
                    }
                }

                db.Entry(formsQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = formsQuestion.Form_Id });
            }
            ViewBag.Form_Id = new SelectList(db.Forms.Where(w => w.EmpresaId == empresaId), "Id", "Title", formsQuestion.Form_Id);
            ViewBag.FormsQuestionsType_Id = new SelectList(FilterFormsQuestionsTypes(db.FormsQuestionsTypes, 0), "Id", "Name", formsQuestion.FormsQuestionsType_Id);
            ViewBag.FormsQuestionsOptions = formsQuestion.FormsQuestionsOptions.Where(w => w.Enabled).ToList();
            ViewBag.FormTypeId = FormTypeId;
            return View(formsQuestion);
        }

        // GET: FormsQuestions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormsQuestion formsQuestion = db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            ViewBag.Form_Id = formsQuestion.Form.Id;
            ViewBag.FormTypeLabel = GetFormTypeLabel((byte)formsQuestion.Form.FormTypeId);
            if (formsQuestion == null)
            {
                return HttpNotFound();
            }
            return View(formsQuestion);
        }



        private string GetFormTypeLabel(int formTypeId)
        {
            string ret = "";
            switch (formTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = "Encuesta";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = "Evaluación";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = "Formulario General";
                    break;
            }
            return ret;
        }


        // POST: FormsQuestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormsQuestion formsQuestion = db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            formsQuestion.Enabled = false;
            db.SaveChanges();
            return RedirectToAction("Index", new { id = formsQuestion.Form_Id });
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted(int formId)
        {
            ViewBag.Form_Id = formId;
            return View(db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).Where(x => x.Form_Id == formId && x.Enabled == false).ToList());
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            FormsQuestion formQuestion = db.FormsQuestions.Where(w => w.Form.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            formQuestion.Enabled = true;
            db.SaveChanges();
            return RedirectToAction("Index", new { id = formQuestion.Form_Id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<FormsQuestionsType> FilterFormsQuestionsTypes(IEnumerable<FormsQuestionsType> elems, byte formTypeId)
        {
            if ((byte)QavantWeb.Models.Enums.FormsTypes.Evaluacion == formTypeId)
            {
                var foo = elems.ToList();
                foo.RemoveAll(x => x.Id == "DATE" || x.Id == "TEXT" || x.Id == "TEXTAREA");
                return foo;
            }
            return elems;
        }
    }
}