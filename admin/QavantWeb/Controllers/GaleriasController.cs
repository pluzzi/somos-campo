﻿using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.extensions;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.Infrastructure.ImageResizer;
using QavantWeb.Models;
using QavantWeb.Services;
using QavantWeb.Services.Settings;
using QavantWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace QavantWeb.Controllers
{
    public class FileMetadata
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Peso { get; set; }
        public string RutaArchivo { get; set; }
    }

    [Authorize(Roles = "Administrador")]
    public class GaleriasController : Controller
    {
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();
        private int moduloId = (int)Enums.Modulos.Galerias;
        private const string DELETE_MARK = "B";

        private readonly IFiltroService _filterService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;
        private readonly IImageResizer _imageResizer;

        public GaleriasController() : this(
            new SettingsService(Enums.Modulos.Galerias), 
            new FileStorageFactory(), 
            new FiltroService(),
            new Fotosizer()) { }

        public GaleriasController(
            ISettingsService settingsService, 
            IFileStorageFactory fileStorageFactory, 
            IFiltroService filterService,
            IImageResizer imageResizer)
        {
            _filterService = filterService;
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
            _imageResizer = imageResizer;
        }

        public ActionResult Index()
        {
            var galerias = db.Galerias.Where(w => w.EmpresaId == empresaId).Include(g => g.Categoria).Where(x => x.Habilitada);
            return View(galerias.ToList());
        }

        public ActionResult IndexDeleted()
        {
            var galerias = db.Galerias.Include(g => g.Categoria).Include(g => g.Empresa).Where(x => x.Habilitada == false);
            return View(galerias.ToList());
        }

        public ActionResult Enable(int id)
        {
            Galeria galeria = db.Galerias.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            galeria.Habilitada = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Galeria galeria = db.Galerias.Find(id);
            if (galeria == null)
            {
                return HttpNotFound();
            }
            return View(galeria);
        }

        public ActionResult Create()
        {
            ViewBag.MultimediaTipoId = new SelectList(db.MultimediaTipoes, "Id", "Descripcion");

            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
            ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");

            var viewModel = new GaleriaViewModel();
            var galeria = new Galeria();
            viewModel.Galeria = galeria;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
            var currentUser = User.Identity.GetUserId();
            HttpContext.Session.Abandon();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            //Recupero archivo que viene del cliente
            var file = Request.Files["file"];

            //Guardo en repositorio (cloud o local) 
            if (file != null && file.ContentLength > 0)
            {
                var fileMetadata = (file.IsImage()) ? SaveImage(file, 1280, 720) : SaveFile(file);
                if (!string.IsNullOrEmpty(fileMetadata.RutaArchivo))
                {
                    GuardarEnSesionGaleriaActual(fileMetadata);
                }
            }
            return Json(new
            {
                Message = string.Empty
            });
        }


        [HttpPost]
        public ActionResult CancelUploadFile(FileMetadata fileToDelete)
        {
            var temp = db.Multimedias.Where(w => w.EmpresaId == empresaId && w.RutaArchivo == fileToDelete.Nombre).FirstOrDefault();
            var tabla = HttpContext.Session[User.Identity.GetUserId()] as Dictionary<string, FileMetadata>;

            if (temp != null)
            {
                DeleteFileTemp(fileToDelete.Nombre);
                // recupero el id y lo elimino de la bd
                int idToDeleted = temp.Id;
                db.Multimedias.Remove(temp);
                db.SaveChanges();

            }
            else
            {
                string filePath = string.Empty;

                foreach (var item in tabla)
                {
                    if (fileToDelete.Nombre == item.Value.Nombre)
                    {
                        filePath = item.Key;
                    }
                    if (filePath.Length > 0)
                    {
                        string[] soloElNombre;
                        soloElNombre = filePath.Split('/');
                        // primero eliminar del Azure/repo 
                        var resul = DeleteFileTemp(soloElNombre[soloElNombre.Length - 1]);
                        if (resul)
                        {
                            //elimino y actualizo la tabla
                            tabla.Remove(item.Key);
                            HttpContext.Session[User.Identity.GetUserId()] = tabla;
                            break;
                        }
                    }
                }
            }
            return Json(new
            {
                Message = string.Empty
            });
        }
        [HttpPost]
        public ActionResult DeleteCoverPage(FileMetadata fileToDelete)
        {
            var temp = db.Multimedias.Where(w => w.EmpresaId == empresaId && w.RutaArchivo == fileToDelete.Nombre).FirstOrDefault();
            var tabla = HttpContext.Session[User.Identity.GetUserId()] as Dictionary<string, FileMetadata>;

            if (temp != null)
            {
                DeleteFileTemp(fileToDelete.Nombre);
                // recupero el id y lo elimino de la bd
                int idToDeleted = temp.Id;
                db.Multimedias.Remove(temp);
                db.SaveChanges();
            }
            else
            {
                string filePath = string.Empty;

                foreach (var item in tabla)
                {
                    if (fileToDelete.Nombre == item.Value.Nombre)
                    {
                        filePath = item.Key;
                    }
                    if (filePath.Length > 0)
                    {
                        string[] soloElNombre;
                        soloElNombre = filePath.Split('/');
                        // primero eliminar del Azure/repo 
                        var resul = DeleteFileTemp(soloElNombre[soloElNombre.Length - 1]);
                        if (resul)
                        {
                            //elimino y actualizo la tabla
                            tabla.Remove(item.Key);
                            HttpContext.Session[User.Identity.GetUserId()] = tabla;
                            break;
                        }
                    }
                }
            }
            return Json(new
            {
                Message = string.Empty
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GaleriaViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                    ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");
                    model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                    return View(model);
                }
                SaveModel(model);

            }
            catch (Exception ex)
            {
                List<Categoria> lsCat = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);
                ViewBag.CategoriaId = new SelectList(lsCat, "Id", "Nombre");
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult GetAttachments()
        {
            List<Multimedia> attachmentsList = new List<Multimedia>();
            var multId = Convert.ToInt64(TempData["galeriaId"].ToString());            
            var listaMultimedia = db.Multimedias.Where(w => w.EmpresaId == empresaId).Where(w => w.EntidadId == multId).ToList();            

            if(listaMultimedia != null)
            {
                foreach (var item in listaMultimedia)
                {
                    Multimedia objMultimedia = new Multimedia();
                    objMultimedia.MultimediaTipoId = item.MultimediaTipoId;
                    objMultimedia.Peso = item.Peso;
                    objMultimedia.RutaArchivo = item.RutaArchivo;          
                    attachmentsList.Add(objMultimedia);
                }
            }                  
            return Json(new { Data = attachmentsList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Galeria galeria = db.Galerias.Where(w => w.EmpresaId == empresaId).Single(s => s.Id == id);

            if (galeria == null)
            {
                return HttpNotFound();
            }
            List<Categoria> lsCat = new List<Categoria>();
            List<Categoria> list = db.Categorias
                                        .Where(w => w.EmpresaId == empresaId)
                                        .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                        .Where(w => w.Habilitada).ToList();
            Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

            SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
            foreach (var item in lsCategoriaPadre)
            {
                if (galeria.CategoriaId == Convert.ToInt32(item.Value))
                {
                    item.Selected = true;
                }
            }
            ViewBag.CategoriaId = lsCategoriaPadre;

            var viewModel = new GaleriaViewModel();
            viewModel.Galeria = galeria;
            viewModel.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
            viewModel.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, galeria.Id);

            TempData["galeriaId"] = galeria.Id;
            HttpContext.Session.Clear();
            return View(viewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GaleriaViewModel model, string foto)
        {
            if (model.Galeria.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");
            try
            {
                model.Galeria.ImagenPortada = (string.IsNullOrEmpty(foto)) ? string.Empty : foto;
                if (model.Galeria.ImagenPortada == "")
                    model.Galeria.ImagenPortada = null;
                if (!ModelState.IsValid)
                {
                    List<Categoria> lsCat = new List<Categoria>();
                    List<Categoria> list = db.Categorias
                                                .Where(w => w.EmpresaId == empresaId)
                                                .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                                .Where(w => w.Habilitada).ToList();
                    Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

                    SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                    foreach (var item in lsCategoriaPadre)
                    {
                        if (model.Galeria.CategoriaId == Convert.ToInt32(item.Value))
                        {
                            item.Selected = true;
                        }
                    }
                    ViewBag.CategoriaId = lsCategoriaPadre;
                    model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                    model.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, model.Galeria.Id);
                    return View(model);
                }
                UpdateModel(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta.  \n " + ex.Message);
                List<Categoria> lsCat = new List<Categoria>();
                List<Categoria> list = db.Categorias
                                            .Where(w => w.EmpresaId == empresaId)
                                            .Where(w => w.ModuloId == (int)Enums.Modulos.Galerias)
                                            .Where(w => w.Habilitada).ToList();
                Common.FormatterHelpers.CategoriasToTree(list, ref lsCat, null, "", true);

                SelectList lsCategoriaPadre = new SelectList(lsCat, "Id", "Nombre");
                foreach (var item in lsCategoriaPadre)
                {
                    if (model.Galeria.CategoriaId == Convert.ToInt32(item.Value))
                    {
                        item.Selected = true;
                    }
                }
                ViewBag.CategoriaId = lsCategoriaPadre;
                model.FiltroConfig.Filtros = _filterService.GetFiltros(empresaId);
                model.FiltroConfig.Segmentacion = _filterService.GetSegmentacion(empresaId, moduloId, model.Galeria.Id);
                return View(model);
            }
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Galeria galeria = db.Galerias.Find(id);
            if (galeria == null)
            {
                return HttpNotFound();
            }
            return View(galeria);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Galeria galeria = db.Galerias.Where(w => w.EmpresaId == empresaId).Single(s => s.Id == id);
            galeria.Habilitada = false;
            //db.Galerias.Remove(galeria);
            db.SaveChanges();
            return RedirectToAction("Index");

        }


        [HttpPost]
        public ActionResult LeavePage(string mensaje)
        {
            var tabla = HttpContext.Session[User.Identity.GetUserId()] as Dictionary<string, FileMetadata>;
            var cont = tabla.Count;
            if(tabla != null)
            {
                for (int i = 0; i < cont; i++)
                {
                    foreach (var item in tabla)
                    {
                        string[] soloElNombre;
                        soloElNombre = item.Key.Split('/');
                        var resul = DeleteFileTemp(soloElNombre[soloElNombre.Length - 1]);
                        if (resul)
                        {
                            //elimino y actualizo la tabla
                            tabla.Remove(item.Key);
                            HttpContext.Session[User.Identity.GetUserId()] = tabla;
                            HttpContext.Session.Clear();
                            break;
                        }
                    }
                }

               
            }
            HttpContext.Session.Clear();
            return Json(new
            {
                Message = "Pasó por LeavePage"
            });
        }

        #region Internal members

        private void GuardarEnSesionGaleriaActual(FileMetadata fileMetadata)
        {
            //buscar la session para el usuario actual
            var tabla = Session[User.Identity.GetUserId()];
            Dictionary<string, FileMetadata> tablaTemp = new Dictionary<string, FileMetadata>();
            if (tabla == null)
            {
                //es la primera vez
                tablaTemp.Add(fileMetadata.RutaArchivo, fileMetadata);
            }
            else
            {
                //ya existe la tabla, actualizarla
                tablaTemp = tabla as Dictionary<string, FileMetadata>;
                tablaTemp.Add(fileMetadata.RutaArchivo, fileMetadata);
            }
            //armar tabla, actualizarla con la nueva ruta (datos)
            Session[User.Identity.GetUserId()] = tablaTemp;
        }
      

        private bool DeleteFileTemp(string file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            string filename = file;
            var container = parametros["ContainerGalerias"];
            return fileStorage.Delete(filename, container);
        }

        private string GetExtension(string tipo)
        {
            string extension = string.Empty;
            switch (tipo)
            {
                case "image/gif":
                case "image/jpg":
                case "image/png":
                case "image/bmp":
                case "image/jpeg":
                    extension = "IMG";
                    break;
                case "video/mp4":
                case "video/mpg":
                case "video/mpeg":
                case "video/quicktime":
                case "video/mov":
                    extension = "VID";
                    break;
            }
            return extension;
        }

        private FileMetadata SaveFile(HttpPostedFileBase file)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerGalerias"];

            return new FileMetadata
            {
                Nombre = file.FileName,
                RutaArchivo = fileStorage.Save(file.InputStream, filename.ToLower(), container),
                Peso = file.ContentLength,
                Tipo = file.ContentType
            };
        }

        private FileMetadata SaveImage(HttpPostedFileBase file, int width, int height)
        {
            var parametros = _settingsService.GetParametrosGlobales();
            var fileStorage = _fileStorageFactory.CreateFileStorage(parametros);
            var filename = FileStorageHelper.FormatFilename(file, parametros);
            var container = parametros["ContainerGalerias"];

            Stream imageStream = _imageResizer.Resize(file.InputStream, width, height);
            return new FileMetadata
            {
                Nombre = file.FileName,
                RutaArchivo = fileStorage.Save(imageStream, filename.ToLower(), container),
                Peso = (int)imageStream.Length,
                Tipo = file.ContentType
            };
        }

        private void SaveModel(GaleriaViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        FileMetadata fileMetadata = (file.IsImage()) ? SaveImage(file, 1280, 720) : SaveFile(file);
                        model.Galeria.ImagenPortada = fileMetadata.RutaArchivo;
                    }
                    model.Galeria.AspNetUsersIdAlta = User.Identity.GetUserId();
                    model.Galeria.EmpresaId = empresaId;
                    model.Galeria.FechaAlta = DateTime.Now;
                    model.Galeria.Habilitada = true;

                    db.Galerias.Add(model.Galeria);
                    db.SaveChanges();

                    int id = model.Galeria.Id;
                    var multimediaTemp = HttpContext.Session[User.Identity.GetUserId()] as Dictionary<string, FileMetadata>;

                    Multimedia objMultimedia = new Multimedia();
                    if (multimediaTemp.Count > 0)
                    {
                        foreach (var item in multimediaTemp)
                        {
                            var tMultim = GetExtension(item.Value.Tipo);
                            var modulId = db.Modulos.FirstOrDefault(w => w.Nemonico == "GALER");
                            var tipoMultimediaId = db.MultimediaTipoes.FirstOrDefault(w => w.Nemonico == tMultim);
                            objMultimedia.EmpresaId = empresaId;
                            objMultimedia.EntidadId = id;
                            objMultimedia.FechaAlta = DateTime.Now;
                            objMultimedia.ModuloId = moduloId;
                            objMultimedia.RutaArchivo = item.Key.ToLower();
                            objMultimedia.Peso = item.Value.Peso;
                            objMultimedia.MultimediaTipoId = tipoMultimediaId.Id;
                            db.Multimedias.Add(objMultimedia);
                            db.SaveChanges();
                        }
                    }

                    //Segmentación
                    foreach (var item in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (item.Action == DELETE_MARK)
                            continue;
                        var segmentoDb = new Segmentacion
                        {
                            EmpresaId = empresaId,
                            ModuloId = moduloId,
                            EntidadId = model.Galeria.Id,
                            FiltroId = item.TipoFiltroId,
                            FiltroValor = item.ValorFiltroId
                        };
                        db.Segmentacions.Add(segmentoDb);
                    }
                    HttpContext.Session.Abandon();
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        private void UpdateModel(GaleriaViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["ImageFile"];
                    if (file != null && file.ContentLength != 0)
                    {
                        FileMetadata fileMetadata = (file.IsImage()) ? SaveImage(file, 1280, 720) : SaveFile(file);
                        model.Galeria.ImagenPortada = fileMetadata.RutaArchivo;
                    };

                    model.Galeria.AspNetUsersIdAlta = User.Identity.GetUserId();
                    model.Galeria.EmpresaId = empresaId;
                    model.Galeria.FechaAlta = DateTime.Now;
                    model.Galeria.Habilitada = true;
                    db.Entry(model.Galeria).State = EntityState.Modified;
                    db.SaveChanges();

                    //recuperar los que ya estan                 
                    var lstMultimedia = db.Multimedias.Where(w => w.EmpresaId == empresaId).Where(w => w.EntidadId == model.Galeria.Id);
                    if (lstMultimedia != null)
                    {
                        foreach (var item in lstMultimedia)
                        {
                            model.Galeria.Multimedias.Add(item);
                            db.Entry(model.Galeria).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    //recuperar los nuevos 
                    var multimediaTemp = HttpContext.Session[User.Identity.GetUserId()] as Dictionary<string, FileMetadata>;
                    Multimedia objMultimedia = new Multimedia();
                    if (multimediaTemp != null)
                    {
                        foreach (var item in multimediaTemp)
                        {
                            var tMultim = GetExtension(item.Value.Tipo);
                            var modulId = db.Modulos.FirstOrDefault(w => w.Nemonico == "GALER");
                            var tipoMultimediaId = db.MultimediaTipoes.FirstOrDefault(w => w.Nemonico == tMultim);
                            objMultimedia.EmpresaId = empresaId;
                            objMultimedia.EntidadId = model.Galeria.Id;
                            objMultimedia.FechaAlta = DateTime.Now;
                            objMultimedia.ModuloId = moduloId;
                            objMultimedia.RutaArchivo = item.Key.ToLower();
                            objMultimedia.Peso = item.Value.Peso;
                            objMultimedia.MultimediaTipoId = tipoMultimediaId.Id;
                            db.Multimedias.Add(objMultimedia);
                            db.SaveChanges();
                        }
                    }

                    // Segmentación
                    db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Galeria.Id));
                    foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                    {
                        if (segmento.Action != DELETE_MARK)
                        {
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Galeria.Id,
                                FiltroId = segmento.TipoFiltroId,
                                FiltroValor = segmento.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }
                    HttpContext.Session.Abandon();
                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion
    }
}
