﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QavantWeb.Models;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using QavantWeb.Services;
using QavantWeb.Services.Settings;
using QavantWeb.Infrastructure.FileStorage;
using QavantWeb.ViewModels;

namespace QavantWeb.Controllers
{
    public class FormsController : Controller
    {
        private const string DELETE_MARK = "B";
        private QavantEntities db = new QavantEntities();
        private int empresaId = SecurityHelpers.getEmpresaId();

        private int moduloEncuestaId = (int)Enums.Modulos.Encuestas;
        private int moduloFormGralId = (int)Enums.Modulos.FormulariosGenerales;

        private readonly IFiltroService _filtroService;
        private readonly ISettingsService _settingsService;
        private readonly IFileStorageFactory _fileStorageFactory;

        public FormsController() : this(new SettingsService(Enums.Modulos.FormulariosGenerales), new FiltroService(), new FileStorageFactory()) { }

        public FormsController(ISettingsService settingsService, IFiltroService filtroService, IFileStorageFactory fileStorageFactory)
        {
            _settingsService = settingsService;
            _fileStorageFactory = fileStorageFactory;
            _filtroService = filtroService;
        }

        // GET: Forms
        [Authorize(Roles = "Administrador")]
        public ActionResult Index(byte iFormTypeId)
        {
            ViewBag.btnCreate = getTxtBtnCrear((int)iFormTypeId);
            ViewBag.btnDelete = getTxtBtnDelete((int)iFormTypeId);
            ViewBag.SubTitle = getSubTitle((int)iFormTypeId);
            ViewBag.iFormTypeId = iFormTypeId;
            var forms = db.Forms.Where(w => w.EmpresaId == empresaId).Where(o => o.Deleted == false && o.FormTypeId == iFormTypeId).OrderByDescending(o => o.CreationDate);
            return View(forms.ToList());
        }

        private string getSubTitle(int iFormTypeId)
        {
            string ret = "";
            switch (iFormTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = " - Encuestas";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = " - Evaluaciones";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = " - Generales";
                    break;
            }
            return ret;
        }

        private string getSubtitleSingular(int formTypeId)
        {
            string ret = "";
            switch (formTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = " - Encuesta";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = " - Evaluación";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = " - General";
                    break;
            }
            return ret;
        }


        private string GetFormTypeDescription(int formTypeId)
        {
            string ret = "";
            switch (formTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = "la encuesta";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = "la evaluación";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = "el formulario general";
                    break;
            }
            return ret;
        }

        private string getTxtBtnCrear(int iFormTypeId)
        {
            string ret = "";
            switch (iFormTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = " Nueva encuesta";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = " Nueva evaluación";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = " Nuevo formulario";
                    break;
            }
            return ret;
        }
        private string getTxtBtnDelete(int iFormTypeId)
        {
            string ret = "";
            switch (iFormTypeId)
            {
                case (int)Enums.FormsTypes.Encuesta:
                    ret = " Encuestas eliminadas";
                    break;
                case (int)Enums.FormsTypes.Evaluacion:
                    ret = " Evaluaciones eliminadas";
                    break;
                case (int)Enums.FormsTypes.Formulario:
                    ret = " Formularios eliminados";
                    break;
            }
            return ret;
        }

        // GET: Forms/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(byte iFormTypeId)
        {
            Form oForm = new Form();
            oForm.FormTypeId = iFormTypeId;
            //by default the form can be autoevaluated
            oForm.CanTheySelfEvaluate = true;
            ViewBag.SubTitle = getSubTitle((int)iFormTypeId);
            ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name");

            var viewModel = new FormViewModel();
            viewModel.Formulario = oForm;
            if (oForm.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
            { 
                viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
            }

            return View(viewModel);
        }

        // POST: Forms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(FormViewModel model)
        {
            byte moduloId = 0;
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name", model.Formulario.FormTypeId);
                    ViewBag.SubTitle = getSubTitle((byte)model.Formulario.FormTypeId);
                    if (model.Formulario.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
                    {
                        model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                    }
                    return View(model);
                }
                switch (model.Formulario.FormTypeId)
                {
                    case 1:
                        {
                            moduloId = (byte)Enums.Modulos.Encuestas;
                            break;
                        }
                    case 2:
                        {
                            moduloId = (byte)Enums.Modulos.FormulariosGenerales;
                            break;
                        }
                    case 3:
                        {
                            moduloId = (byte)Enums.Modulos.Capacitaciones;
                            break;
                        }
                }
                SaveModel(model, moduloId);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el alta. \n " + ex.Message);
                ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name", model.Formulario.FormTypeId);
                ViewBag.SubTitle = getSubTitle((byte)model.Formulario.FormTypeId);
                if (model.Formulario.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
                {
                    model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                }
                return View(model);
            }
            return RedirectToAction("Index", new { iFormTypeId = model.Formulario.FormTypeId });
        }

        private void SaveModel(FormViewModel model, byte moduloId)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {

                    model.Formulario.EmpresaId = empresaId;
                    model.Formulario.CreationUserId = User.Identity.GetUserId();
                    model.Formulario.CreationDate = DateTime.Now;
                    if (model.Formulario.FormTypeId == null)
                        model.Formulario.FormTypeId = moduloId;

                    db.Forms.Add(model.Formulario);
                    db.SaveChanges();

                    //Segmentación

                    if (model.Formulario.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
                    {
                        foreach (var item in model.FiltroConfig.Segmentacion.SegmentoCollection)
                        {
                            if (item.Action == DELETE_MARK)
                                continue;
                            var segmentoDb = new Segmentacion
                            {
                                EmpresaId = empresaId,
                                ModuloId = moduloId,
                                EntidadId = model.Formulario.Id,
                                FiltroId = item.TipoFiltroId,
                                FiltroValor = item.ValorFiltroId
                            };
                            db.Segmentacions.Add(segmentoDb);
                        }
                    }


                    db.SaveChanges();
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        // GET: Forms/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id, byte iFormTypeId)
        {
            byte moduloId = 0;

            switch (iFormTypeId)
            {
                case 1:
                    {
                        moduloId = (byte)Enums.Modulos.Encuestas;
                        break;
                    }
                case 2:
                    {
                        moduloId = (byte)Enums.Modulos.FormulariosGenerales;
                        break;
                    }
                case 3:
                    {
                        moduloId = (byte)Enums.Modulos.Capacitaciones;
                        break;
                    }
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Form form = db.Forms.Where(w => w.EmpresaId == empresaId).Single(f => f.Id == id);
            if (form == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubTitle = getSubTitle((byte)form.FormTypeId);
            ViewBag.formsQuestionsType = db.FormsQuestionsTypes.OrderBy(x => x.Name).ToList();
            ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name", form.FormTypeId);

            var viewModel = new FormViewModel();
            viewModel.Formulario = form;

            if (form.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
            { 
                viewModel.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                viewModel.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, form.Id);
            }

            return View(viewModel);
        }

        // POST: Forms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(FormViewModel model, byte iFormTypeId)
        {
            byte moduloId = 0;

            switch (iFormTypeId)
            {
                case 1:
                    {
                        moduloId = (byte)Enums.Modulos.Encuestas;
                        break;
                    }
                case 2:
                    {
                        moduloId = (byte)Enums.Modulos.FormulariosGenerales;
                        break;
                    }
                case 3:
                    {
                        moduloId = (byte)Enums.Modulos.Capacitaciones;
                        break;
                    }
            }
            if (model.Formulario.EmpresaId != empresaId)
                ModelState.AddModelError("", "Error al actualizar el registro.");

            if (!ModelState.IsValid)
            {
                ViewBag.SubTitle = getSubTitle((byte)model.Formulario.FormTypeId);
                ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name", model.Formulario.FormTypeId);
                if (model.Formulario.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
                { 
                    model.FiltroConfig.Filtros = _filtroService.GetFiltros(empresaId);
                    model.FiltroConfig.Segmentacion = _filtroService.GetSegmentacion(empresaId, moduloId, model.Formulario.Id);
                }
                return View(model);
            }
            UpdateModel(model);
            return RedirectToAction("Index", new { iFormTypeId = model.Formulario.FormTypeId });
        }

        private void UpdateModel(FormViewModel model)
        {
            using (DbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    byte moduloId = 0;
                    switch (model.Formulario.FormTypeId)
                    {
                        case 1:
                            {
                                moduloId = (byte)Enums.Modulos.Encuestas;
                                break;
                            }
                        case 2:
                            {
                                moduloId = (byte)Enums.Modulos.FormulariosGenerales;
                                break;
                            }
                        case 3:
                            {
                                moduloId = (byte)Enums.Modulos.Capacitaciones;
                                break;
                            }
                    }
                    model.Formulario.CreationUserId = User.Identity.GetUserId();
                    model.Formulario.CreationDate = DateTime.Now;
                    if (model.Formulario.FormTypeId == null)
                        model.Formulario.FormTypeId = (Byte)Enums.FormsTypes.Encuesta;
                    db.Entry(model.Formulario).State = EntityState.Modified;
                    db.SaveChanges();

                    // Segmentación
                    if (model.Formulario.FormTypeId != (int)Enums.FormsTypes.Evaluacion)
                    {
                        db.Segmentacions.RemoveRange(db.Segmentacions.Where(x => x.EmpresaId == empresaId && x.ModuloId == moduloId && x.EntidadId == model.Formulario.Id));
                        foreach (var segmento in model.FiltroConfig.Segmentacion.SegmentoCollection)
                        {
                            if (segmento.Action != DELETE_MARK)
                            {
                                var segmentoDb = new Segmentacion
                                {
                                    EmpresaId = empresaId,
                                    ModuloId = moduloId,
                                    EntidadId = model.Formulario.Id,
                                    FiltroId = segmento.TipoFiltroId,
                                    FiltroValor = segmento.ValorFiltroId
                                };
                                db.Segmentacions.Add(segmentoDb);
                            }
                        }
                    }


                    db.SaveChanges();

                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    throw;
                }
            }
        }


        // GET: Forms
        [Authorize(Roles = "Administrador")]
        public ActionResult Reporting(byte iFormTypeId)
        {
            List<SelectListItem> lsForms = new SelectList(
                db.Forms
                .Where(w => w.EmpresaId == empresaId)
                .Where(x => x.Enabled == true && x.FormTypeId == iFormTypeId)
                .OrderBy(x => x.Id), "Id", "Title"
            ).ToList();

            lsForms.Insert(0, new SelectListItem { Text = " Seleccionar", Value = "0" });

            List<SelectListItem> lsTipo = new List<SelectListItem>();
            lsTipo.Insert(0, new SelectListItem { Text = " Detallado", Value = "2" });
            lsTipo.Insert(0, new SelectListItem { Text = " Agrupado", Value = "1" });
            lsTipo.Insert(0, new SelectListItem { Text = " Seleccionar", Value = "0" });

            ViewBag.FormId = lsForms;
            ViewBag.FormTypeId = lsTipo;
            if (iFormTypeId != null)
            {
                ViewBag.SubTitle = getSubTitle(iFormTypeId);
                ViewBag.TipoForm = iFormTypeId;
            }

            var forms = db.Forms.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Id).ToList();
            return View(forms);
        }

        // GET: Forms
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FormId">Id de instancia de formulario</param>
        /// <param name="FormTypeId">Id de tipo de reporte</param>
        /// <param name="TipoForm">Tipo de formulario</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Reporting(int FormId, int FormTypeId, Enums.FormsTypes? TipoForm, byte iFormTypeId)
        {
            if (FormId != 0)
            {
                if (db.Forms.FirstOrDefault(f => f.Id == FormId).EmpresaId != empresaId)
                    //Intenton hackear por empresaId
                    return RedirectToAction("Index", new { iFormTypeId = iFormTypeId });

            }
            if (FormId > 0 && FormTypeId > 0 && TipoForm != null)
            {
                ViewBag.TipoForm = TipoForm;
                ViewBag.FormReportGrouped = null;
                ViewBag.FormReportDetailed = null;
                switch (FormTypeId)
                {
                    case 1: //Agrupado                        
                        List<FormReportingGrouppedViewModel> lsGroupped = db.FormsQuestionsOptions
                                                                            .Where(x => x.Enabled)
                                                                            .Include(i => i.FormsQuestion)
                                                                            .Include(i => i.FormsRespDetalles)
                                                                            .Where(w => w.FormsQuestion.Enabled)
                                                                            .Where(w => w.FormsQuestion.Form_Id == FormId)
                                                                            .GroupBy(g => new
                                                                            {
                                                                                g.Id,
                                                                                g.FormsQuestion.Order,
                                                                                g.FormsQuestion.Question,
                                                                                g.Description
                                                                            })
                                                                            .OrderBy(o => o.Key.Order)
                                                                            .Select(x => new FormReportingGrouppedViewModel
                                                                            {
                                                                                Pregunta = x.Key.Order.ToString() + "- " + x.Key.Question,
                                                                                Respuesta = (x.Key.Question == null ?
                                                                                                "Texto Libre"
                                                                                                : x.Key.Description),
                                                                                Cantidad = db.FormsRespDetalles.Where(w => w.FormsQuestionsOptionId == x.Key.Id).Count()
                                                                            })
                                                                            .ToList();

                        ViewBag.FormReportGrouped = lsGroupped;
                        ViewBag.FormAnswerCount = db.FormsRespDetalles.Where(w => w.FormsRespCabecera.Form.EmpresaId == empresaId).Where(s => s.FormsRespCabecera.Form.AspNetUser.Id == FormId.ToString())
                            .Select(s => new
                            {
                                s.FormsRespCabecera.Form.AspNetUser.Id,
                                s.FormsRespCabecera.FechaCreacion
                            }).Distinct().Count();
                        break;
                    case 2: //Detallado
                        List<FormReportingDetailedHeaderViewModel> lsHeaders = db.FormsQuestions
                                                                                    .Where(w => w.Enabled)
                                                                                    .Where(w => w.Form_Id == FormId)
                                                                                    .OrderBy(o => o.Order)
                                                                                    .Select(x => new FormReportingDetailedHeaderViewModel
                                                                                    {
                                                                                        PreguntaId = x.Id,
                                                                                        Pregunta = x.Order.ToString() + "- " + x.Question
                                                                                    })
                                                                                    .ToList();
                        ViewBag.FormReportDetailedHeaders = lsHeaders;
                        ViewBag.RespuestasUsuarios = db.FormsRespDetalles
                                                 .Include(i => i.FormsRespCabecera)
                                                 .Where(w => w.FormsRespCabecera.FormsId == FormId)
                                                 .OrderBy(c => c.FormsRespCabecera.Form.AspNetUser.Id).ThenBy(n => n.FormsRespCabecera.FechaCreacion)
                                                 .ToList();

                        ViewBag.FormAnswerCount = db.FormsRespDetalles.Where(w => w.FormsRespCabecera.Form.EmpresaId == empresaId).Where(s => s.FormsRespCabecera.FormsId == FormId)
                            .Select(s => new
                            {
                                s.FormsRespCabecera.Form.AspNetUser.Id,
                                s.FormsRespCabecera.FechaCreacion
                            }).Distinct().Count();
                        break;
                }
                var forms = db.Forms.Where(w => w.EmpresaId == empresaId).OrderBy(o => o.Id);
            }

            List<Form> instanciasForm = new List<Form>();
            int tipoFormId = Convert.ToInt32(TipoForm);
            instanciasForm = db.Forms.Where(w => w.EmpresaId == empresaId).Where(x => x.Enabled == true && x.FormTypeId == iFormTypeId).OrderBy(x => x.Id).ToList();

            List<SelectListItem> lsForms = new SelectList(instanciasForm, "Id", "Title").ToList();
            lsForms.Insert(0, new SelectListItem { Text = " Seleccionar", Value = "0" });
            ViewBag.FormId = lsForms;

            List<SelectListItem> lsTipo = new List<SelectListItem>();
            lsTipo.Insert(0, new SelectListItem { Text = " Detallado", Value = "2" });
            lsTipo.Insert(0, new SelectListItem { Text = " Agrupado", Value = "1" });
            lsTipo.Insert(0, new SelectListItem { Text = " Seleccionar", Value = "0" });
            ViewBag.FormTypeId = lsTipo;
            if (iFormTypeId != null)
            {
                ViewBag.SubTitle = getSubTitle(iFormTypeId);
                ViewBag.TipoForm = iFormTypeId;

            }
            return View();

        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Form form = db.Forms.Where(w => w.EmpresaId == empresaId).Include("Courses").FirstOrDefault(f => f.Id == id);
            if (form == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubTitle = getSubtitleSingular((byte)form.FormTypeId);
            ViewBag.FormTypeId = new SelectList(db.FormsTypes.Where(w => w.Enabled).Where(w => w.ChosenByUser), "Id", "Name", form.FormTypeId);
            return View(form);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            Form form = db.Forms.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            form.CreationUserId = User.Identity.GetUserId();
            form.Deleted = true;
            form.Enabled = false;
            db.SaveChanges();
            return RedirectToAction("Index", new { iFormTypeId = form.FormTypeId });
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult IndexDeleted(byte iFormTypeId)
        {
            //ViewBag.SubTitle = getSubTitle(iFormTypeId);
            var subtitle = getSubTitle(iFormTypeId);
            //concatenar con un <br >

            switch (subtitle)
            {
                case " - Encuestas":
                    ViewBag.SubTitle = " - Encuestas - Eliminadas";
                    break;
                case " - Evaluaciones":
                    ViewBag.SubTitle = " - Evaluaciones - Eliminadas";
                    break;
                case " - Generales":
                    ViewBag.SubTitle = " - Generales - Eliminados";
                    break;
            }

            ViewBag.iFormTypeId = iFormTypeId;
            ViewBag.FormTypeDescription = GetFormTypeDescription((int)iFormTypeId);
            return View(db.Forms.Where(w => w.EmpresaId == empresaId).Where(x => x.Deleted == true).Where(w => w.FormTypeId == iFormTypeId).ToList().OrderByDescending(x => x.CreationDate));
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Enable(int id)
        {
            Form form = db.Forms.Where(w => w.EmpresaId == empresaId).FirstOrDefault(f => f.Id == id);
            form.CreationUserId = User.Identity.GetUserId();
            form.Deleted = false;
            db.SaveChanges();
            return RedirectToAction("Index", new { iFormTypeId = form.FormTypeId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
