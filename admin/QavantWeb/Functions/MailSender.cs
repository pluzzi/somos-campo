﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Linq;
using System.Web;
using System.Configuration;

namespace QavantWeb.Functions
{
    public class MailSender
    {
        public static void SendMails(IList<QueueItem> mails)
        {
            foreach (var queueMail in mails)
            {
                var fromAddress = new MailAddress(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["MailFromDescription"]);
                var toAddress = queueMail.To; //new MailAddress(queueMail.To);
                var fromPassword = ConfigurationManager.AppSettings["MailFromPassword"].ToString();

                var subject = queueMail.Subject;
                var body = queueMail.HtmlMessage;

                var smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings["SmtpHost"],
                    Port = int.Parse(ConfigurationManager.AppSettings["SmtpPort"]),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage message = new MailMessage();
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                message.From = fromAddress;

                foreach (var address in toAddress.ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.To.Add(address);
                }

                smtp.Send(message);

            }
        }
    }
}