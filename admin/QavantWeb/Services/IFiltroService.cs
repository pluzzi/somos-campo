﻿using QavantWeb.Models;
using QavantWeb.ViewModels;

namespace QavantWeb.Services
{
    public interface IFiltroService
    {
        FiltroCollectionViewModel GetFiltros(int empresaId);
        SegmentacionViewModel GetSegmentacion(int empresaId, int moduloId, int entidadId);
        int GetSegmentationCountByFilterValue(int empresaId, Enums.Filtros filterId, int filtroValor);
    }
}