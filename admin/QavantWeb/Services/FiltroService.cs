﻿using QavantWeb.Models;
using QavantWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QavantWeb.Services
{
    public class FiltroService : IFiltroService
    {
        private QavantEntities db = new QavantEntities();

        public FiltroCollectionViewModel GetFiltros(int empresaId)
        {
            var filtros = new FiltroCollectionViewModel();
            var filtrosDb = db.seg_GetFiltros(empresaId).ToList();
            
            int count = 0;
            while (count < filtrosDb.Count)
            {
                int? filtroTipoIdAnt = filtrosDb[count].FiltroTipoId;
                string filtroTipoDescripcion = filtrosDb[count].FiltroTipoDescripcion;
                Dictionary<string, string> opciones = new Dictionary<string, string>();
                while (count < filtrosDb.Count && filtroTipoIdAnt == filtrosDb[count].FiltroTipoId)
                {
                    string key = Convert.ToString(filtrosDb[count].FiltroId);
                    string value = Convert.ToString(filtrosDb[count].FiltroDescripcion);
                    opciones.Add(key, value);
                    count++;
                }
                var filtro = new FiltroViewModel
                {
                    Tipo = new TipoFiltroViewModel { Id = filtroTipoIdAnt, Nombre = filtroTipoDescripcion },
                    Opciones = opciones
                };
                filtros.FiltroCollection.Add(filtro);
            }
            return filtros;
        }

        public SegmentacionViewModel GetSegmentacion(int empresaId, int moduloId, int entidadId)
        {
            var segmentacion = new SegmentacionViewModel();
            var segmentacionDb = db.seg_GetSegmentacion(empresaId, entidadId, moduloId).ToList();
            foreach (var item in segmentacionDb)
            {
                segmentacion.SegmentoCollection.Add(new SegmentoViewModel()
                {
                    Id = item.id,
                    TipoFiltroId = item.filtroId,
                    TipoFiltroDescripcion = item.Descripcion,
                    ValorFiltroId = item.FiltroValor,
                    ValorFiltroDescripcion = item.FiltroDescripcion
                });
            }
            return segmentacion;
        }


        public int GetSegmentationCountByFilterValue(int empresaId, Enums.Filtros filterId, int filtroValor)
        {
            var count = db.Segmentacions.Count(seg => seg.EmpresaId == empresaId && 
                                                        seg.FiltroId == (int) filterId && 
                                                        seg.FiltroValor == filtroValor);

            return count;
        }
    }
}