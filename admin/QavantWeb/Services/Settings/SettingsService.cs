﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Services.Settings
{
    public class SettingsService : ISettingsService
    {
        private Enums.Modulos _modulo;

        public SettingsService(Enums.Modulos modulo)
        {
            _modulo = modulo;
        }

        public Dictionary<string, string> GetParametrosGlobales()
        {
            Dictionary<string, string> parametros = new Dictionary<string, string>();
            QavantEntities db = new QavantEntities();
            int empresaId = HttpContext.Current.GetOwinContext()
                                    .GetUserManager<ApplicationUserManager>()
                                    .FindById(HttpContext.Current.User.Identity.GetUserId()).EmpresaId;
            Empresa empresa = db.Empresas.Where(w => w.Id == empresaId).FirstOrDefault();

            parametros.Add("EmpresaNemonico", empresa.Nemonico);
            parametros.Add("SiteUrl", empresa.DireccionWeb);
            parametros.Add("NombreModulo", _modulo.ToString());

            var results = from paramEmpresa in db.ParametrosEmpresasCanals
                          join parametro in db.Parametros on paramEmpresa.ParametroId equals parametro.Id
                          join modulo in db.Modulos on parametro.ModuloId equals modulo.Id
                          where (paramEmpresa.EmpresaId == empresa.Id) && (modulo.Nemonico == "PARAM") && (paramEmpresa.CanalId == (int)Enums.Canales.Admin)
                          select new
                          {
                              clave = parametro.Nemonico,
                              valor = paramEmpresa.Valor
                          };
            var raw = results.ToList();
            foreach (var item in raw)
            {
                parametros.Add(item.clave, item.valor);
            }
            return parametros;
        }
    }
}


//parametros.Add("TypeFileStorage", "AzureStorage");
//parameters.Add("TypeFileStorage", "FileSystem");
//parameters.Add("PathNewsImages", "/Uploads/News/");
//parameters.Add("PathBenefitsImages", "/Uploads/Users/");
//parameters.Add("PathDocumentsFiles", "/Uploads/Files/");
//parameters.Add("PathInfoUtilImages", "/Uploads/InfoUtil/");
