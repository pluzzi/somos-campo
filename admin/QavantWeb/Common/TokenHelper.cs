﻿using JWT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using static QavantWeb.Models.Enums;

namespace QavantWeb.Common
{
    public static class TokenHelper
    {
        public static TokenInfo GetTokenInfo(string decodedToken)
        {
            dynamic tokenData = JsonConvert.DeserializeObject<dynamic>(decodedToken);
            var info = new TokenInfo
            {
                TenantId = tokenData.EmpresaId,
                UserId = tokenData.UserId,
                UserName = tokenData.UserName,
                Canal = tokenData.Canal
            };
            return info;
        }

        public static string CreateToken(int empresaId, string userId, string userName, Canales canal)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtKey");
            var payload = new Dictionary<string, object>
            {
                {"EmpresaId", empresaId },
                {"UserId", userId },
                {"UserName", userName },
                {"Canal", canal }
            };
            var token = JsonWebToken.Encode(payload, secret, JwtHashAlgorithm.HS256);
            var cryptography = new QavantTripleDES();
            return cryptography.Encrypt(token, true);
        }

        public static string DecodeToken(string encryptedToken)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtKey");
            var cryptography = new QavantTripleDES();
            string jwt = cryptography.Decrypt(encryptedToken, true);
            var decodedToken = JsonWebToken.Decode(jwt, secret);
            if (string.IsNullOrEmpty(decodedToken))
                throw new Exception("Problemas en la decodificación del token.");

            return decodedToken;
        }
    }

    public class TokenInfo
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int TenantId { get; set; }
        public Canales Canal { get; set; }
    }

    public class QavantTripleDES
    {
        public string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader =
                                                new AppSettingsReader();
            // Get the key from config file

            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

    }
}