﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using QavantWeb.Common;
using Microsoft.AspNet.Identity.Owin;
using QavantWeb.Models;

namespace QavantWeb.Common
{
    public static class EmpresayHelpers
    {
        public static Empresa getEmpresa()
        {
            try
            {
                QavantEntities db = new QavantEntities();
                int empresaId = HttpContext.Current.GetOwinContext()
                                        .GetUserManager<ApplicationUserManager>()
                                        .FindById(HttpContext.Current.User.Identity.GetUserId()).EmpresaId;
                Empresa oEmp = db.Empresas.Where(w => w.Id == empresaId).FirstOrDefault();
                return oEmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static String getEmpresaNemonico()
        {
            try
            {
                var nemonico = getEmpresa().Nemonico;
                return nemonico;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}