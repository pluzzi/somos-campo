﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Common
{
    public static class AzureBlobHelper
    {

        public static string uploadContent(HttpPostedFileBase pFile, string pModulo)
        {
            string strURL = "";
            try
            {
                string accountName = WebConfigurationManager.AppSettings["AzureBlobAccount"].ToString();
                string accountKey = WebConfigurationManager.AppSettings["AzureBlobKey"].ToString();
                string accountCS = WebConfigurationManager.AppSettings["AzureBlobConnectionString"].ToString();
                string containerName = WebConfigurationManager.AppSettings["AzureBlobContainerName"].ToString();

                string empresaKey = EmpresayHelpers.getEmpresa().Nemonico;


                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(accountCS);

                CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer storageContainer = BlobClient.GetContainerReference(containerName);

                if (pFile?.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(pFile.FileName);
                    fileName = empresaKey + "-" + pModulo + "-" + Guid.NewGuid().ToString() + Path.GetExtension(pFile.FileName);
                    // Azure Storage
                    CloudBlockBlob blockBlob = storageContainer.GetBlockBlobReference(fileName);
                    blockBlob.UploadFromStream(pFile.InputStream);
                    strURL = blockBlob.Uri.AbsoluteUri;
                }

            }
            catch (Exception ex)
            {
                strURL = "";
            }
            return strURL;
        }
    }
}