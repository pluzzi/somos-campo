﻿using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Common
{
    public static class FormatterHelpers
    {
        public static List<Categoria> CategoriasToTree(List<Categoria> list, ref List<Categoria> outList, int? parentId = null, string parentPrefix = "", bool soloHabilitados = true)
        {
            string sMsj = "";
            try
            {
                sMsj = "Paso 1";
                List<Categoria> lsToReturn = list
                           .Where(w => w.PadreCategoriaId == parentId)
                           .Where(w => (soloHabilitados && w.Habilitada) || (!soloHabilitados))
                           .Select(s => new Categoria
                           {
                               Id = s.Id,
                               Nombre = parentPrefix + s.Nombre,
                               PadreCategoriaId = s.PadreCategoriaId,
                               Habilitada = s.Habilitada,
                               ModuloId = s.ModuloId,
                               Modulo = s.Modulo
                           })
                           .ToList()
                           .OrderBy(o => o.Nombre).ToList();

                sMsj = "Paso 2";
                if (lsToReturn.Count > 0)
                {
                    int index = 0;
                    if (parentId != null)
                        index = outList.FindIndex(w => w.Id == parentId) + 1;
                    outList.InsertRange(index, lsToReturn);
                    foreach (Categoria cat in lsToReturn)
                        CategoriasToTree(list, ref outList, cat.Id, "--" + parentPrefix, soloHabilitados);
                }

                return lsToReturn;
            }
            catch (Exception ex)
            {
                string sMsjError = "CategoriasToTree --> " + sMsj + " - " + ex.Message;
                if (ex.InnerException != null) { sMsjError = sMsjError + " - " + ex.InnerException.Message; }
                throw new Exception(sMsjError);

            }


        }

        public static List<Area> AreasToTree(List<Area> list, ref List<Area> outList, int? parentId = null, string parentPrefix = "", bool soloHabilitados = true)
        {
            string sMsj = "";
            try
            {
                sMsj = "Paso 1";
                List<Area> lsToReturn = list
                           .Where(w => w.AreaPadreId == parentId)
                           .Where(w => (soloHabilitados && w.Habilitada) || (!soloHabilitados))
                           .Select(s => new Area
                           {
                               Id = s.Id,
                               Nombre = parentPrefix + s.Nombre,
                               AreaPadreId = s.AreaPadreId,
                               Habilitada = s.Habilitada
                           })
                           .ToList()
                           .OrderBy(o => o.Nombre).ToList();

                sMsj = "Paso 2";
                if (lsToReturn.Count > 0)
                {
                    int index = 0;
                    if (parentId != null)
                        index = outList.FindIndex(w => w.Id == parentId) + 1;
                    outList.InsertRange(index, lsToReturn);
                    foreach (Area cat in lsToReturn)
                        AreasToTree(list, ref outList, cat.Id, "--" + parentPrefix, soloHabilitados);
                }

                return lsToReturn;
            }
            catch (Exception ex)
            {
                string sMsjError = "AreasToTree --> " + sMsj + " - " + ex.Message;
                if (ex.InnerException != null) { sMsjError = sMsjError + " - " + ex.InnerException.Message; }
                throw new Exception(sMsjError);

            }


        }

    }
}