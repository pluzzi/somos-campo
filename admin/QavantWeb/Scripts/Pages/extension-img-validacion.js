﻿function validateImageExtension(fileInput) {
    var permitida = false;
    var extensiones_permitidas = new Array("/gif", "/jpg", "/png", "/bmp", "/jpeg");

    if (fileInput.files.length > 0) {
        var imgExtension = fileInput.files[0].type;
        var extension = (imgExtension.substring(imgExtension.lastIndexOf("/"))).toLowerCase();
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (!permitida) {
            alert("La imagen no es válida. Los formatos aceptados son: .gif, .jpg, .png, .bmp, .jpeg ");
            fileInput.value = "";
        };
    }


}