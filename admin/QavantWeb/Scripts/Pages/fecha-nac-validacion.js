﻿$(".close").click(function () {
    $("#myAlert").alert().hide();
});

$("#btn_guardar").on('click', function () {
    $("#myAlert").alert().hide();
    Mensaje = "";
    var anioMin = "1900-01-01";

    var hoy = new Date();
    var dd = hoy.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    var mm = hoy.getMonth() + 1;
    if (mm < 10) {
        mm = '0' + mm
    }
    var yyyy = hoy.getFullYear();
    var fechaActual = yyyy + "-" + mm + "-" + dd;
    var fechaActualFormateada = dd + "/" + mm + "/" + yyyy;

    //validar fechas
    if ($("#from_Id").val() != "") {
        if ($("#from_Id").val() < anioMin)
            Mensaje = Mensaje + "- Por favor, ingrese una fecha mayor o igual a 01/01/1900. <br>";
        if ($("#from_Id").val() > fechaActual)
            Mensaje = Mensaje + "- Por favor, ingrese una fecha menor o igual a " + fechaActualFormateada + ". <br>";
    }
    if ($("#from_Id").val() == "")
        Mensaje = Mensaje + "- Debe ingresar una fecha de nacimiento. <br>";
    //mostrar mensaje
    if (Mensaje != "") {
        $("#msgAlert").html("<strong>Controle los siguientes errores:</strong><br>" + Mensaje);
        $("#myAlert").alert().show();
        return false;
    }
})