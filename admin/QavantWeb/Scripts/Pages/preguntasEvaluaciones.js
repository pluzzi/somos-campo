﻿$(document).ready(function () {
    $('#create').click(function () {
        var type = $('#FormsQuestionsType_Id').val();
        if (type == 'DROPDOWN' || type == 'MULTIPLE' || type == 'OPTION') {
            var elems = $('#tbl_optQuestionOptions tbody tr').length;
            if (elems == 0) {
                alert('Ingrese al menos una respuesta como opción.');
                return false;
            }

            var checks = $('#tbl_optQuestionOptions tbody input:checkbox');
            var thereIsRightAnswer = false;

            for (var i = 0; i < checks.length; i++) {
                if (thereIsRightAnswer != checks[i].checked) {
                    thereIsRightAnswer = true;
                }
            }
            if (!thereIsRightAnswer) {
                var r = confirm('No seleccionó ninguna respuesta como correcta, ¿desea continuar?');
                if (r == true) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    });
});