﻿$(document).ready(function () {
    setDataRelated();
    setFormRelated();

    $("#Capacitacion_CourseType_Id").on('change', function () {
        setDataRelated();
        return true;
    });

    $("#Capacitacion_RequiresEvaluation").on('change', function () {
        setFormRelated();
    });

    $("#Capacitacion_ApprovedPerc").TouchSpin({
        min: 0,
        max: 100,
        step: 1,
        decimals: 0,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });



    $("#btn_guardar").click(function () {
        var isValid = true;
        var courseTypeId = $("#Capacitacion_CourseType_Id").val();
        switch (courseTypeId) {
            case "2"://Documento.
                if ($("#File").val() == '' && $("#hfAction").val() == "create") //No hay documento seleccionado / create
                    isValid = false;
                if ($("#File").val() == '' && $("#hfAction").val() == "edit" && $("#aLinkDoc").text() == "") // No hay documento seleccionado / edit
                    isValid = false;
                if (!isValid) {
                    addToValidationSummaryCursos("El archivo es requerido para este tipo de capacitación.");
                }
                break;
            case "1"://Video
                if ($("#Capacitacion_VideoURL").val() == '') {
                    isValid = false;
                    addToValidationSummaryCursos("El id del video es requerido para este tipo de capacitación.");
                }
                break;
            case "3"://URL
                if ($("#Capacitacion_LinkURL").val() == '') {
                    isValid = false;
                    addToValidationSummaryCursos("La URL del link es requerida para este tipo de capacitación.");
                }
                break;
        }

        return isValid;
    });
});



function addToValidationSummaryCursos(message) {
    if ($('#myAlert > ul > li:contains("' + message + '")').length > 0)
        return;
    $('#myAlert > ul').append('<li>' + message + '</li>');
}


function setDataRelated() {
    $(".dataRelated").hide();
    switch ($("#Capacitacion_CourseType_Id option:selected").text()) {
        case "DOCUMENTO":
            $("#Document_group").show();
            break;
        case 'VIDEO':
            $("#Video_group").show();
            break;
        case 'URL':
            $("#URL_group").show();
            break;
    }
}

function setFormRelated() {
    $(".formRelated").hide();
    if ($('#Capacitacion_RequiresEvaluation').is(':checked')) {
        $(".formRelated").show();
    }
}

