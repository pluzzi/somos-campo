﻿$("#btn_guardar").on('click', function () {
    var infoFieldHasText = isCKEditorValid();
    var IsThereImage = isThereImage();
    var isValid = false;
    if (infoFieldHasText || IsThereImage) {
        isValid = true;
    }
    else {
        isValid = false;
    }
    if (!isValid) {
        addToValidationSummary("Es obligatorio completar los campos 'Imagen' y/o 'Información'.");
    }
    return isValid;
});

function isThereImage() {
    var hasImage = true;
    if ($("#ImageFile").val() == '' && $("#hfAction").val() == "create") //No hay documento seleccionado / create
        hasImage = false;
    if ($("#ImageFile").val() == '' && $("#hfAction").val() == "edit" && $("#Image").attr("src") == '') // No hay documento seleccionado / edit
        hasImage = false;
    return hasImage;
}