﻿$(document).ready(function () {
    switch ($("#FormsQuestionsType_Id").val()) {
        case 'DROPDOWN':
        case 'MULTIPLE':
        case 'OPTION':
            $("#pnlOpciones").show();
            break;
        default:
            $("#pnlOpciones").hide();
            break;
    }

    if ($("#Formulario_SendMails:checked").length > 0) {
        $("#pnlMailOptions").show();
    }
    else {
        $("#pnlMailOptions").hide();
    }

    $("#FormsQuestionsType_Id").on('change', function () {
        switch ($(this).val()) {
            case 'DROPDOWN':
            case 'MULTIPLE':
            case 'OPTION':
                $("#pnlOpciones").show();
                break;
            default:
                $("#pnlOpciones").hide();
                break;
        }
    });

    $("#cboQuestionType").on('change', function () {
        switch ($(this).val()) {
            case 'DROPDOWN':
            case 'OPTION':
            case 'MULTIPLE':
                $("#pnlOpciones").show();
                break;
            default:
                $("#pnlOpciones").hide();
                break;
        }
    });

    $("#btn_optAdd").on('click', function () {
        var enterNumberValue = $("#txt_optNumberValue").length;
        if ($("#txt_optDescription").val() == '' || $("#txt_optOrder").val() == '' || (enterNumberValue && $("#txt_optNumberValue").val() == '')) {
            alert('Todos los campos son obligatorios');
        }
        else if ($("#txt_optOrder").val() <= 0) {
            alert('El campo "Orden" tiene que ser mayor que 0');
        }
        //else if (enterNumberValue && $("#txt_optNumberValue").val() < 0) {
        //    alert('El campo "Valor" no puede ser negativo');
        //}
        else {

            itemNro = $("#tbl_optQuestionOptions tbody tr").length;
            existeOrden = false;
            existeNumberValue = false;
            agregarOpcion = true;

            for (i = 0; i <= itemNro - 1; i++) {
                if ($("#hdn_optOrder_" + i).val() == $("#txt_optOrder").val()) {
                    existeOrden = true;
                }
                if (enterNumberValue && $("#hdn_optNumberValue_" + i).val() == $("#txt_optNumberValue").val()) {
                    existeNumberValue = true;
                }
            }

            if (existeOrden && !alert('Ya existe otra opción con el mismo número de orden.\nElija un número de orden distinto')) {
                agregarOpcion = false;
            }

            if (enterNumberValue && existeNumberValue && !confirm('Ya existe otra opción con el mismo valor.\n¿Desea continuar de todas maneras?')) {
                agregarOpcion = false;
            }

            if (agregarOpcion) {
                description = $("#txt_optDescription").val();
                if (enterNumberValue)
                    numbervalue = $('#txt_optNumberValue').val();
                order = $("#txt_optOrder").val();
                if ($("#chk_IsRightOne").length == 0)
                    isRightAnswer = false;
                else
                    isRightAnswer = $("#chk_IsRightOne").is(':checked');
                var RightChecked = "";
                if ($("#chk_IsRightOne").length != 0) {
                    if (isRightAnswer) {
                        if ($("#FormsQuestionsType_Id").val() != "MULTIPLE") {
                            $(".hidIsRightAnswer").val(false);
                            $(".chkIsRightAnswer").prop('checked', false);
                        }

                        RightChecked = "<input type=\"checkbox\" disabled readonly class=\"chkIsRightAnswer\" checked />";
                    }
                    else
                        RightChecked = "<input type=\"checkbox\" disabled readonly class=\"chkIsRightAnswer\" />";
                }
                else {
                    RightChecked = "<span></span>";
                }
                questionId = $("#Id").val();

                if (questionId == undefined) {
                    questionId = '0';
                }
                action = 'A';

                newQO = "<tr id=\"tr_opt_" + itemNro + "\">";
                newQO += "<td>" + description + "</td>";
                if (enterNumberValue)
                    newQO += "<td>" + numbervalue + "</td>";
                newQO += "<td>" + order + "</td>";
                newQO += "<td>" + RightChecked + "</td>";

                newQO += "<td> ";
                newQO += "<button id=\"btn_optDelete\" actionid=\"" + itemNro + "\" type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>";

                newQO += "<input type=\"hidden\" id=\"hdn_optId_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].Id\" value=\"0\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optFormsQuestionId" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].FormsQuestion_Id\" value=\"" + questionId + "\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optDescription_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].Description\" value=\"" + description + "\" />";
                if (enterNumberValue)
                    newQO += "<input type=\"hidden\" id=\"hdn_optNumberValue_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].NumberValue\" value=\"" + numbervalue + "\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optOrder_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].Order\" value=\"" + order + "\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optIsRightAnswer_" + itemNro + "\" class=\"hidIsRightAnswer\" name=\"FormsQuestionsOptions[" + itemNro + "].IsRightAnswer\" value=\"" + isRightAnswer + "\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optEnabled_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].Habilitado\" value=\"1\" />";
                newQO += "<input type=\"hidden\" id=\"hdn_optAction_" + itemNro + "\" name=\"FormsQuestionsOptions[" + itemNro + "].Action\" value=\"" + action + "\" />";
                newQO += "</td></tr>";

                $("#tbl_optQuestionOptions tbody").append(newQO);

                limpiarCampos();

                $("#btn_optAdd").show();
                $("#btn_optModify").hide();

                $("#txt_optDescription").focus();
            }
        }
    });

    $("#btn_optModify").on('click', function () {
        var enterNumberValue = $("#txt_optNumberValue").length;
        var index = $("#hdn_Edit").val();
        action = 'M';

        if ($("#chk_IsRightOne").length != 0) {
            var isRightAnswer = $("#chk_IsRightOne").is(':checked');
            if (isRightAnswer) {
                if ($("#FormsQuestionsType_Id").val() != "MULTIPLE") {
                    $(".hidIsRightAnswer").val(false);
                    $(".chkIsRightAnswer").prop('checked', false);
                }
            }
            $("#hdn_optIsRightAnswer_" + index).val(isRightAnswer);
            $("#tr_opt_" + index + " td .chkIsRightAnswer").prop('checked', isRightAnswer);
        }
        $("#tr_opt_" + index + " td").eq(0).text($("#txt_optDescription").val());
        if (enterNumberValue)
            $("#tr_opt_" + index + " td").eq(1).text($("#txt_optNumberValue").val());
        $("#tr_opt_" + index + " td").eq(2).text($("#txt_optOrder").val());

        $("#hdn_optDescription_" + index).val($("#txt_optDescription").val());
        if (enterNumberValue)
            $("#hdn_optNumberValue_" + index).val($("#txt_optNumberValue").val());
        $("#hdn_optOrder_" + index).val($("#txt_optOrder").val());
        $("#hdn_optAction_" + index).val(action);



        limpiarCampos();

        $("#hdn_Edit").val(0);
        $("#btn_optAdd").show();
        $("#btn_optModify").hide();
    });

    $(document).on('click', "#btn_optEdit", function () {
        var enterNumberValue = $("#txt_optNumberValue").length;
        var index = $(this).attr('actionid');

        $("#txt_optDescription").val($("#hdn_optDescription_" + index).val());
        if (enterNumberValue)
            $("#txt_optNumberValue").val($("#hdn_optNumberValue_" + index).val());
        $("#txt_optOrder").val($("#hdn_optOrder_" + index).val());
        if ($("#chk_IsRightOne").length != 0) {
            $("#chk_IsRightOne").prop('checked', ($("#hdn_optIsRightAnswer_" + index).val().toLowerCase() == "true"));
        }
        $("#hdn_Edit").val(index);

        $("#btn_optAdd").hide();
        $("#btn_optModify").show();

    });

    $(document).on('click', "#btn_optDelete", function () {
        var index = $(this).attr('actionid');
        var cont = 0;
        var enterNumberValue = $("#txt_optNumberValue").length;
        var rowCount = $('#tbl_optQuestionOptions >tbody >tr').length;

        if ($("#hdn_optAction_" + index).val() == 'A') {//si se había dado de alta en esta edición, la fila se elimina directamente del dom (no llegará al server).
            $("#tr_opt_" + index).remove();
            $('#tbl_optQuestionOptions tbody tr').each(function () {
                $(this).find('input[id^="hdn_optId"]').attr('name', 'FormsQuestionsOptions[' + cont + '].Id');
                $(this).find('input[id^="hdn_optFormsQuestionId"]').attr('name', 'FormsQuestionsOptions[' + cont + '].FormsQuestion_Id');
                $(this).find('input[id^="hdn_optDescription"]').attr('name', 'FormsQuestionsOptions[' + cont + '].Description');
                $(this).find('input[id^="hdn_optOrder"]').attr('name', 'FormsQuestionsOptions[' + cont + '].Order');
                $(this).find('input[id^="hdn_optIsRightAnswer"]').attr('name', 'FormsQuestionsOptions[' + cont + '].IsRightAnswer');
                $(this).find('input[id^="hdn_optEnabled"]').attr('name', 'FormsQuestionsOptions[' + cont + '].Habilitado');
                $(this).find('input[id^="hdn_optAction"]').attr('name', 'FormsQuestionsOptions[' + cont + '].Action');
                cont++;
            });
        }
        else {
            action = 'M';

            $("#tr_opt_" + index).hide();
            $("#hdn_optEnabled_" + index).val('0');
            $("#hdn_optAction_" + index).val(action);
            $("#hdn_optOrder_" + index).val('-1');
        }
        var rowCountAfter = $('#tbl_optQuestionOptions >tbody >tr').length;
    });

    $("#Formulario_SendMails").on('click', function () {
        $("#pnlMailOptions").toggle();
    });

    function limpiarCampos() {
        var enterNumberValue = $("#txt_optNumberValue").length;
        $("#txt_optDescription").val('');
        if (enterNumberValue)
            $("#txt_optNumberValue").val('');
        $("#txt_optOrder").val('');
        if ($("#chk_IsRightOne").length != 0) {
            $("#chk_IsRightOne").prop('checked', false);
        }
    }



    $("#btnSave").on('click', function () {
        return validateOptionsCount();
    });

    function validateOptionsCount() {
        var MINIMUN_AMOUNT_OF_OPTIONS = 2;
        var isValid = false;
        switch ($("#FormsQuestionsType_Id").val()) {
            case 'DROPDOWN':
            case 'MULTIPLE':
            case 'OPTION':
                var optionsVisible = $("#tbl_optQuestionOptions > tbody > tr:visible").length;
                isValid = optionsVisible >= MINIMUN_AMOUNT_OF_OPTIONS;
                break;
            default:
                isValid = true;
                break;
        }

        if (!isValid)
            addErrorToValidationSummary("Ingrese al menos dos opciones para este tipo de pregunta.");

        return isValid;
    }


    function addErrorToValidationSummary(message) {
        var newUl = "<li>" + message + "</li>"
        $("#myAlert ul").empty();
        $("#myAlert ul").append(newUl);
    }


})