﻿$(document).ready(function () {
    $('#tooltipUsersInGroup').tooltip(
        {
            placement: 'right',
            html: 'true',
            title: '<strong>Nota:</strong> Solo podrá seleccionar usuarios habilitados y activos.'
        }
    );

});
