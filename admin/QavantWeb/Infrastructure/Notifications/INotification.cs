﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QavantWeb.Infrastructure.Notifications
{
    public interface INotification
    {
        int Send(NotificationSettings settings);
    }
}
