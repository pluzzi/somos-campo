﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Infrastructure.Notifications
{
    public class NotificationSettings
    {
        public string ServerKey { get; set; }
        public string Endpoint { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string RedirectTo { get; set; }
        public List<string> Tokens { get; set; }
    }
}