﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Infrastructure.Notifications
{
    public class FirebaseNotification : INotification
    {
        private string _serverKey;
        private string _endpoint;
        private string _title;
        private string _message;
        private string _redirectTo;
        private int _size = 1000;

        public int Send(NotificationSettings settings)
        {
            _serverKey = settings.ServerKey;
            _endpoint = settings.Endpoint;
            _title = settings.Title;
            _message = settings.Message;
            _redirectTo = settings.RedirectTo;

            int result = default(int);
            List<List<string>> chunks = settings.Tokens.SplitList(_size);
            foreach (List<string> chunk in chunks)
            {
                result += ProcessChunk(chunk);
            }
            return result;
        }

        private int ProcessChunk(List<string> chunk)
        {
            var result = string.Empty;
            try
            {
                string data = "{ \"registration_ids\":[" + GetFormattedTokenCollection(chunk) + "],\"notification\":{\"title\":\"" + _title + "\",\"body\":\"" + _message + "\", \"sound\": \"default\",  \"icon\": \"fcm_push_icon\"}, \"data\": { \"title\": \"" + _title + "\", \"body\": \"" + _message + "\", \"state\": \"" + _redirectTo + "\" }}";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_endpoint);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=" + _serverKey);
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string strNJson = data;
                    streamWriter.Write(strNJson);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                dynamic json = JsonConvert.DeserializeObject(result);
                string devSended = json["success"]; //failure
                return int.Parse(devSended);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private string GetFormattedTokenCollection(List<string> tokens)
        {
            string tokenCollection = string.Empty;
            foreach (var token in tokens)
            {
                tokenCollection += "\"" + token + "\",";
            }
            tokenCollection = tokenCollection.Substring(0, tokenCollection.Length - 1);
            return tokenCollection;
        }
    }

    public static class ListExtensions
    {
        public static List<List<T>> SplitList<T>(this List<T> me, int size = 50)
        {
            var list = new List<List<T>>();
            for (int i = 0; i < me.Count; i += size)
                list.Add(me.GetRange(i, Math.Min(size, me.Count - i)));
            return list;
        }
    }
}