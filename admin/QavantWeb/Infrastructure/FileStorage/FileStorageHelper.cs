﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace QavantWeb.Infrastructure.FileStorage
{
    public class FileStorageHelper
    {
        public static string FormatFilename(HttpPostedFileBase file, Dictionary<string, string> parametros)
        {
            string empresaNemonico = parametros["EmpresaNemonico"];
            string nombreModulo = parametros["NombreModulo"];
            string filename = $"{empresaNemonico}-{nombreModulo}-{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
            return filename;
        }

    }
}


