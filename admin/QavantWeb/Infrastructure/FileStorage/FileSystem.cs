﻿using QavantWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Infrastructure.FileStorage
{
    public class FileSystem : IFileStorage
    {
        private Dictionary<string, string> _parameters;

        public FileSystem(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public string Save(Stream stream, string filename, string container)
        {
            string targetFolder = HttpContext.Current.Server.MapPath(container);
            string targetPath = Path.Combine(targetFolder, filename);
            using (var fileStream = new FileStream(targetPath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
                var absoluteUrl = GenerateAbsoluteUrl($"{container}{filename}");
                return absoluteUrl;
            }
        }

        public string Save(HttpPostedFileBase file, string filename, string container)
        {
            string targetFolder = HttpContext.Current.Server.MapPath(container);
            string targetPath = Path.Combine(targetFolder, filename);
            file.SaveAs(targetPath);            
            var absoluteUrl = GenerateAbsoluteUrl($"{container}{filename}");
            return absoluteUrl;
        }

        public static string GenerateAbsoluteUrl(string path, bool forceHttps = false)
        {
            const string HTTPS = "https";
            var uri = HttpContext.Current.Request.Url;
            var scheme = forceHttps ? HTTPS : uri.Scheme;
            var host = uri.Host;
            var port = (forceHttps || uri.Scheme == HTTPS) ? string.Empty : (uri.Port == 80 ? string.Empty : ":" + uri.Port);
            return string.Format("{0}://{1}{2}/{3}", scheme, host, port, string.IsNullOrEmpty(path) ? string.Empty : path.TrimStart('/'));
        }

        public bool Delete(string filename, string container)
        {
            bool wasRemoved = false;
            string targetFolder = HttpContext.Current.Server.MapPath(container);
            string targetPath = Path.Combine(targetFolder, filename);

            if (File.Exists(targetPath))
            {
                File.Delete(targetPath);
                wasRemoved = true;
            }

            return wasRemoved;
        }
    }
}