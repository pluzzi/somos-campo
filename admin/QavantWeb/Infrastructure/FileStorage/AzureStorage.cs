﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using QavantWeb.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Infrastructure.FileStorage
{
    public class AzureStorage : IFileStorage
    {
        private Dictionary<string, string> _parameters;

        public AzureStorage(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public string Save(Stream stream, string filename, string container)
        {
            return SaveOnAzure(stream, filename, container);
        }

        public string Save(HttpPostedFileBase file, string filename, string container)
        {
            return SaveOnAzure(file.InputStream, filename, container);
        }

        public bool Delete(string filename, string container)
        {
            bool wasRemoved = false;
            try
            {
                string accountName = WebConfigurationManager.AppSettings["AzureBlobAccount"].ToString();
                string accountKey = WebConfigurationManager.AppSettings["AzureBlobKey"].ToString();
                string accountCS = WebConfigurationManager.AppSettings["AzureBlobConnectionString"].ToString();

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(accountCS);
                CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer storageContainer = BlobClient.GetContainerReference(container);

                //Gets List of Blobs
                //var list = storageContainer.ListBlobs();
                if (filename?.Length > 0)
                {
                    CloudBlockBlob blockBlob = storageContainer.GetBlockBlobReference(filename);
                    bool existsFile = blockBlob.DeleteIfExists();
                    if (existsFile)
                        wasRemoved = true;                  
                }
            }
            catch (Exception ex)
            {
                wasRemoved = false;
            }
            return wasRemoved;
        }

        private string SaveOnAzure(Stream stream, string filename, string container)
        {
            string strURL = string.Empty;
            try
            {
                string accountName = WebConfigurationManager.AppSettings["AzureBlobAccount"].ToString();
                string accountKey = WebConfigurationManager.AppSettings["AzureBlobKey"].ToString();
                string accountCS = WebConfigurationManager.AppSettings["AzureBlobConnectionString"].ToString();

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(accountCS);
                CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer storageContainer = BlobClient.GetContainerReference(container);

                CloudBlockBlob blockBlob = storageContainer.GetBlockBlobReference(filename);
                blockBlob.UploadFromStream(stream);
                strURL = blockBlob.Uri.AbsoluteUri;
            }
            catch (Exception ex)
            {
                strURL = string.Empty;
            }
            return strURL;
        }
    }
}