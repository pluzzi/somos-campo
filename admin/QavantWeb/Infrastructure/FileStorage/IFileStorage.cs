﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QavantWeb.Infrastructure.FileStorage
{
    public interface IFileStorage
    {
        string Save(Stream stream, string filename, string container);
        bool Delete(string filename, string container);
        string Save(HttpPostedFileBase file, string filename, string container);
    }
}