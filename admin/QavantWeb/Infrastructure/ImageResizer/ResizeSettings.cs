﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantWeb.Infrastructure.ImageResizer
{
    public class ResizeSettings
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}