﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Web;
using System.Drawing.Imaging;
using System.IO;

namespace QavantWeb.Infrastructure.ImageResizer
{
    public class Fotosizer : IImageResizer
    {
        public Stream Resize(Stream stream, int width, int height)
        {
            Image originalImage = Image.FromStream(stream);
            Bitmap resizedImage = Resize(originalImage, width, height, true, false);
            var ms = new MemoryStream();
            resizedImage.Save(ms, ImageFormat.Jpeg);
            ms.Position = 0; // If you're going to read from the stream, you may need to reset the position to the start
            return ms;
        }

        private Bitmap Resize(Image image, int width, int height, bool keepAspectRatio, bool enlargeSmallerImages)
        {
            if (!enlargeSmallerImages && image.Width <= width && image.Height <= height)
            {
                return new Bitmap(image);
            }
            if (!keepAspectRatio)
            {
                return new Bitmap(image, width, height);
            }
            else
            {
                double aspectRatio = image.Width / (double)image.Height;
                double newAspectRatio = width / height;

                if (aspectRatio >= newAspectRatio) //fit horizontally
                {
                    double scale = image.Width / (double)width;
                    int newHeight = (int)(image.Height / scale);
                    return new Bitmap(image, width, newHeight);
                }
                else //fit vertically
                {
                    double scale = image.Height / (double)height;
                    int newWidth = (int)(image.Width / scale);
                    return new Bitmap(image, newWidth, height);
                }
            }
        }
    }
}