﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(FormsQuestionsMetaData))]
    public partial class FormsQuestion
    {
        
    }

    public class FormsQuestionsMetaData
    {
        [Required]
        [StringLength((255), ErrorMessage = "La pregunta no debe superar los 255 caracteres.")]
        [Display(Name = "Pregunta")]
        public string Question { get; set; }

        [Required]
        [Display(Name = "Orden")]
        [Range(1, 9999, ErrorMessage = "El valor de Orden debe estar entre 1 y 9999.")]
        public short Order { get; set; }

        [Display(Name = "Pregunta obligatoria")]
        public bool Required { get; set; }

        [Display(Name = "Habilitada")]
        public bool Enabled { get; set; }

        [Display(Name = "Tipo de pregunta")]
        public string FormsQuestionsType_Id { get; set; }

        //[Required(ErrorMessage = "Ingrese fecha de inicio")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Vigencia Desde")]
        //public Nullable<System.DateTime> DateFrom { get; set; }

        //[Required(ErrorMessage = "Ingrese fecha de fin")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name ="Vigencia Hasta")]
        //public Nullable<System.DateTime> DateTo { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //public System.DateTime CreationDate { get; set; }

    }
}