﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(NoticiaMetaData))]
    public partial class Noticia
    {
    }

    public class NoticiaMetaData
    {
        [Required(ErrorMessage = "Ingrese el título")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres")]
        [Display(Name = "Subtítulo")]
        public string Copete { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Ingrese la fecha")]
        public System.DateTime FechaDesde { get; set; }
        
        public string Contenido { get; set; }


    }
}