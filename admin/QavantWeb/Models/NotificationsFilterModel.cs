﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    public partial class NotificationsFilter
    {

        public string Action { get; set; }
        public string Description
        {
            get
            {
                string filter = this.FilteType;
                QavantEntities db = new QavantEntities();
                string ret = "";
                if (filter == Enums.NotificationsFilterTypes.AREA.ToString())
                {
                    int valId = int.Parse(this.FilterValue);
                    ret = db.Areas.Where(w => w.Id == valId).FirstOrDefault().Nombre;
                }

                if (filter == Enums.NotificationsFilterTypes.GROUP.ToString())
                {
                    int valId = int.Parse(this.FilterValue);
                    ret = db.Groups.Where(w => w.Id == valId).FirstOrDefault().Name;
                }
                if (filter == Enums.NotificationsFilterTypes.SEX.ToString())
                {
                    switch (this.FilterValue)
                    {
                        case "M":
                            ret = "Masculinos";
                            break;
                        case "F":
                            ret = "Femeninos";
                            break;
                    }
                }
                db.Dispose();
                db = null;
                return ret;
            }

        }
    }

}