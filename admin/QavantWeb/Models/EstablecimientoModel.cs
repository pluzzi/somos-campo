﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(EstablecimientoMetaData))]
    public partial class Establecimiento
    {
    }

    public class EstablecimientoMetaData
    {

        [Required(ErrorMessage = "Ingrese Nombre")]
        [StringLength((100), ErrorMessage = "El nombre no debe superar los 100 caracteres.")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

    }
}