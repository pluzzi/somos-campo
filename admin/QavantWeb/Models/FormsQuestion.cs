//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FormsQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FormsQuestion()
        {
            this.FormsRespDetalles = new HashSet<FormsRespDetalle>();
            this.FormsQuestionsOptions = new HashSet<FormsQuestionsOption>();
        }
    
        public int Id { get; set; }
        public int Form_Id { get; set; }
        public string Question { get; set; }
        public string FormsQuestionsType_Id { get; set; }
        public short Order { get; set; }
        public bool Enabled { get; set; }
        public bool Required { get; set; }
    
        public virtual Form Form { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormsRespDetalle> FormsRespDetalles { get; set; }
        public virtual FormsQuestionsType FormsQuestionsType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormsQuestionsOption> FormsQuestionsOptions { get; set; }
    }
}
