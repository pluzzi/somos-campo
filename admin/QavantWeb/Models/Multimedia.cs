//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Multimedia
    {
        public int Id { get; set; }
        public short MultimediaTipoId { get; set; }
        public int EntidadId { get; set; }
        public int EmpresaId { get; set; }
        public int ModuloId { get; set; }
        public string RutaArchivo { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public Nullable<int> Peso { get; set; }
    
        public virtual Galeria Galeria { get; set; }
        public virtual Modulo Modulo { get; set; }
    }
}
