﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    public partial class ReportsRankingViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Cantidad { get; set; }
    }
}