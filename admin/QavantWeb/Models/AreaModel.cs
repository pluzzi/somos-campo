﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(AreaModel))]
    public partial class Area
    {
    }

    public class AreaModel
    {
        [Required(ErrorMessage = "Ingrese nombre")]
        public string Nombre { get; set; }
    }
}