﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(FormMetaData))]
    public partial class Form
    {
        
    }

    public class FormMetaData
    {
        [Required(ErrorMessage = "Ingrese título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres.")]
        [Display(Name = "Título")]
        public string Title { get; set; }

        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres.")]
        [Display(Name = "Subtítulo")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de inicio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Vigencia Desde")]
        public System.DateTime DateFrom { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de fin.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name ="Vigencia Hasta")]
        public System.DateTime DateTo { get; set; }

        [Display(Name = "Habilitado")]
        public bool Enabled { get; set; }

        [Display(Name = "Respuesta única")]
        public bool UniqueReply { get; set; }

        [Display(Name = "Envía mails")]
        public bool SendMails { get; set; }

        [Display(Name = "Para")]
        public string MailsTo { get; set; }

        [Display(Name = "Asunto")]
        public string MailSubject { get; set; }

        [Display(Name = "Título")]
        public string MailTitle { get; set; }

        [Display(Name = "Introducción")]
        public string MailIntro { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime CreationDate { get; set; }

        [Display(Name = "¿Pueden autoevaluarse?")]
        public bool CanTheySelfEvaluate { get; set; }

    }
}