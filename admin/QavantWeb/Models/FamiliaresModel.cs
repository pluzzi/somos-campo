﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(FamiliaresModel))]
    public partial class Familiare
    {        
    }

    public class FamiliaresModel
    {
        [Required(ErrorMessage = "Ingrese nombre")]
        [StringLength((50), ErrorMessage = "El nombre no debe superar los 50 caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Ingrese apellido")]
        [StringLength((50), ErrorMessage = "El apellido no debe superar los 50 caracteres")]
        public string Apellido { get; set; }

    }
}