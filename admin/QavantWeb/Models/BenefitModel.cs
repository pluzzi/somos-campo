﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(BenefitMetaData))]
    public partial class Benefit
    {
        public string FullImageUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings["SiteUrl"].ToString()
                        + WebConfigurationManager.AppSettings["PathNewsImages"].ToString()
                        + Image;
            }
        }
    }

    public class BenefitMetaData
    {
        [Required(ErrorMessage = "Ingrese título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Title { get; set; }
 

        public string Image { get; set; }

        public string Content { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres.")]
        [Display(Name = "Subtítulo")]
        public string Subtitle { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de inicio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> From { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de fin.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> To { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime Date { get; set; }
        
    }
}