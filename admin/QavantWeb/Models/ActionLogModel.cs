﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(ActionLogModel))]
    public partial class ActionLog
    {
        public int BenefitId { get; set; }
        public int NewsId { get; set; }

        public Benefit BenefitRel { get; set; }
        public Noticia NewsRel { get; set; }
        public string ItemTitle { get; set; }

    }

    public class ActionLogModel
    {
    }
}