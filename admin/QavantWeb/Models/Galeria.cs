//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QavantWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Galeria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Galeria()
        {
            this.Multimedias = new HashSet<Multimedia>();
        }
    
        public int Id { get; set; }
        public int EmpresaId { get; set; }
        public int CategoriaId { get; set; }
        public string Titulo { get; set; }
        public string Copete { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public string AspNetUsersIdAlta { get; set; }
        public System.DateTime FechaDesde { get; set; }
        public System.DateTime FechaHasta { get; set; }
        public string ImagenPortada { get; set; }
        public bool Destacada { get; set; }
        public bool Habilitada { get; set; }
    
        public virtual Categoria Categoria { get; set; }
        public virtual Empresa Empresa { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Multimedia> Multimedias { get; set; }
    }
}
