﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType(typeof(CategoriaModel))]
    public partial class Categoria
    {
        public List<Categoria> CategoriasHijas { get; set; }
    }

    public class CategoriaModel
    {
        [Required(ErrorMessage = "Ingrese nombre")]
        [StringLength((50), ErrorMessage = "El nombre no debe superar los 50 caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Ingrese módulo")]
        public int ModuloId { get; set; }
    }
}