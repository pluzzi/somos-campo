﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(CoursMetaData))]
    public partial class Cours
    {

    }

    public class CoursMetaData
    {
        [Required(ErrorMessage = "Ingrese título.")]
        [StringLength((150), ErrorMessage = "El título no debe superar los 150 caracteres")]
        [Display(Name = "Título")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Ingrese subtítulo.")]
        [StringLength((300), ErrorMessage = "El subtítulo no debe superar los 300 caracteres")]
        [Display(Name = "Subtítulo")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de inicio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Vigencia Desde")]        
        public System.DateTime From { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de fin.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name ="Vigencia Hasta")]        
        public System.DateTime To { get; set; }

        [Display(Name = "Habilitado")]
        public bool Enabled { get; set; }

        [Required(ErrorMessage = "Ingrese una categoría")]
        [Display(Name = "Categoría")]
        public int CategoriaId { get; set; }

        [Required(ErrorMessage = "Ingrese un tipo de capacitación.")]
        [Display(Name = "Tipo de Capacitación")]
        public int CourseType_Id { get; set; }

        [Display(Name = "URL del Link")]
        [Url(ErrorMessage ="Ingrese una URL válida. Ejemplo: https://www.google.com.")]
        public string LinkURL { get; set; }

        [Display(Name = "URL del Video")]
        public string VideoURL { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime CreationDate { get; set; }

        [Range(0, 100, ErrorMessage = "El porcentaje de aprobación debe ser un número entre 0 y 100%")]
        [Display(Name = "Aprobado con")]
        public int ApprovedPerc;

        [Display(Name = "Evaluación")]
        public int Form_Id { get; set; }

        [Display(Name = "¿Requiere Evaluación?")]
        public int RequiresEvaluation { get; set; }
        
    }
}