﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(FormsQuestionsOptionMetaData))]
    public partial class FormsQuestionsOption
    {
        public string Action { get; set; }
        public string Habilitado { get; set; }
    }

    public class FormsQuestionsOptionMetaData
    {
        //[Required(ErrorMessage = "Ingrese título")]
        //[StringLength((255), ErrorMessage = "Ingrese un título hasta 255 caracteres")]
        //[Display(Name = "Titulo")]
        //public string Title { get; set; }

        //[StringLength((500), ErrorMessage = "Ingrese una descripción hasta 500 caracteres")]
        //[Display(Name = "Descripción")]
        //[DataType(DataType.MultilineText)]
        //public string Description { get; set; }

        //[Required(ErrorMessage = "Ingrese fecha de inicio")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name = "Vigencia Desde")]
        //public Nullable<System.DateTime> DateFrom { get; set; }

        //[Required(ErrorMessage = "Ingrese fecha de fin")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Display(Name ="Vigencia Hasta")]
        //public Nullable<System.DateTime> DateTo { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //public System.DateTime CreationDate { get; set; }
        
    }
}