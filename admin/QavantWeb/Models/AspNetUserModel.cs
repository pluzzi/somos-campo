﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QavantWeb.Models
{
    [MetadataType (typeof(AspNetUsersMetaData))]
    public partial class AspNetUser
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string UsernamePostfix { get; set; }
    }

    public class AspNetUsersMetaData{

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Ingrese una dirección de correo electrónico válida.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Apellido")]
        public string Surname { get; set; }


        public string Password { get; set; }
        

        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Seleccione un rol.")]
        public virtual ICollection<AspNetRole> AspNetRoles { get; set; }

        [Required]
        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }
    }


    public class AspNetUserCreate : AspNetUser
    {
        [Required]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "Contraseña y Confirmar Contraseña no coinciden.")]
        public string ConfirmPassword { get; set; }
    }

    public class AspNetUserEdit : AspNetUser
    {
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "Contraseña y Confirmar Contraseña no coinciden.")]
        public string ConfirmPassword { get; set; }
    }
}