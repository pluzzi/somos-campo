﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace QavantWeb.Models
{
    [MetadataType(typeof(UserMetaData))]
    public partial class User
    {
        public Puesto puesto { get; set; }
        public string FullImageUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings["SiteUrl"].ToString()
                        + WebConfigurationManager.AppSettings["PathNewsImages"].ToString()
                        + Image;
            }
        }
    }

    public class UserMetaData
    {
        [Required(ErrorMessage = "Ingrese nombre")]
        [StringLength((100), ErrorMessage = "El nombre no debe superar los 100 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Ingrese apellido")]
        [StringLength((100), ErrorMessage = "El apellido no debe superar los 100 caracteres.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Ingrese un usuario")]
        [StringLength((50), ErrorMessage = "El nombre de usuario no debe superar los 50 caracteres.")]
        public string UserName { get; set; }

        [EmailAddress]
        [StringLength((100), ErrorMessage = "El email no debe superar los 100 caracteres.")]
        public string Email { get; set; }

        [StringLength((50), ErrorMessage = "La contraseña no debe superar los 50 caracteres.")]
        public string Password { get; set; }        

        [Required(ErrorMessage = "Ingrese fecha de nacimiento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime BirthDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime RegistrationDate { get; set; }

        [Required(ErrorMessage = "Ingrese un área")]
        public Nullable<int> Area_Id { get; set; }

        [Required(ErrorMessage = "Ingrese región")]
        public Nullable<int> RegionId { get; set; }

        [Required(ErrorMessage = "Ingrese sucursal")]
        public Nullable<int> SucursalId { get; set; }

        [Required(ErrorMessage = "Ingrese compañia")]
        public Nullable<int> CompaniaId { get; set; }

        [StringLength((25), ErrorMessage = "El nro. de teléfono no debe superar los 25 caracteres.")]
        public string Phone { get; set; }

        [Phone(ErrorMessage = "Ingrese un número de celular válido")]
        [StringLength((25), ErrorMessage = "El nro. de celular no debe superar los 25 caracteres.")]
        public string Mobile { get; set; }

        [Phone(ErrorMessage = "Ingrese un número de interno válido")]
        [StringLength((10), ErrorMessage = "El nro. de interno no debe superar los 10 caracteres.")]
        public string Intern { get; set; }

        [StringLength((20), ErrorMessage = "El nro. de legajo no debe superar los 20 caracteres.")]
        public string FileNumber { get; set; }

        [StringLength((20), ErrorMessage = "El documento no debe superar los 20 caracteres.")]
        public string Dni { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaIngreso { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaEgreso { get; set; }

        public bool? Active { get; set; }

        public bool? Enable { get; set; }

        [StringLength((100), ErrorMessage = "El nombre de contacto no debe superar los 100 caracteres.")]
        public string ContactoUrgencias { get; set; }

        [StringLength((20), ErrorMessage = "El nro. de teléfono no debe superar los 20 caracteres.")]
        public string TelefonoPersonal { get; set; }

        [StringLength((20), ErrorMessage = "El nro. de teléfono no debe superar los 20 caracteres.")]
        public string TelefonoUrgencias { get; set; }

        [StringLength((200), ErrorMessage = "La dirección no debe superar los 200 caracteres.")]
        public string Address { get; set; }

        [StringLength((200), ErrorMessage = "La localidad no debe superar los 20 caracteres.")]
        public string City { get; set; }

        [StringLength((20), ErrorMessage = "El nro. de Cuil no debe superar los 20 caracteres.")]
        public string Cuil { get; set; }

        [Required(ErrorMessage = "Ingrese género.")]
        public int GeneroCatalogoId { get; set; }


    }
}