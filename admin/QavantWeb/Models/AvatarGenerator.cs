namespace QavantWeb.Models
{

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

    public partial class AvatarGenerator
    {

        public AvatarGenerator() { }

        public string Generate(string firstName, string lastName, string colorPrincipal, string colorSecundario)
        {
            var avatarString = string.Format("{0}{1}", firstName[0], lastName[0]).ToUpper();
            var bmp = new Bitmap(192, 192);
            var sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            var font = new Font("Arial", 48, FontStyle.Bold, GraphicsUnit.Pixel);
            var graphics = Graphics.FromImage(bmp);

            graphics.Clear((Color)new ColorConverter().ConvertFromString(colorPrincipal));
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            graphics.DrawString(avatarString, font, new SolidBrush(Color.White), new RectangleF(0, 0, 192, 192), sf);
            graphics.Flush();

            var ms = new MemoryStream();
            bmp.Save(ms, ImageFormat.Png);

            byte[] imageBytes = ms.ToArray();

            string base64String = "data:image/jpeg;base64," + Convert.ToBase64String(imageBytes);
            return base64String;

        }
    }

}
