﻿https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

## Requerimientos

#### Guardar imágenes
* Según parametría, guardar las imágenes en el repositorio de archivos elegido por el Tenant: On-premise(FileSystem) o Cloud(Azure)
* Los nombres de los archivos se guardan con el siguiente formato: [EmpresaNemonico]-[Modulo]-[Guid].[extension]
* 


#### Envío de notificaciones
* Aplicar un mecanismo de **Chunking** configurable para cuando haya muchos elementos por procesar. Más info en https://goo.gl/B9Eeka
* Para fines de prueba, se puede utilizar el siguiente SP para generar miles de registros (reemplazar por valores válidos):

```
declare @id int 
select @id = 1
while @id >=1 and @id <= 1001
begin
    insert into [dbo].[Devices](UserId, Uuid, TokenNotification) values( 2, 'a35c38a0ab9f8e82', 'd0YmQ2eKcGY:APA91bEuwnwIyFyc0guo-rtGUUbOjaX9tUxfzMvmq__Q6EymyNWJdxK5EDKZHkkcRM-4U2BVE4l75RQgnwyrf4kLw9mpF-N1SxonoOqxjjigfyrcbzX9R4IuUQFJaP9IlzRKEW1ZLMso')
    select @id = @id + 1
end
```

#### Aplicar filtros de contenido
* Crear un componente reutilizable aplicable a cualquier tipo de contenido: noticias, beneficios, documentos, capacitaciones, notificaciones y futuros módulos.
* Links
  * When should we use Html Helpers, Razor Helpers or Partial Views? https://goo.gl/2TkEfX
