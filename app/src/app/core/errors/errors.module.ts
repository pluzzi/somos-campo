import { NgModule, ErrorHandler } from "@angular/core";
import { GlobalErrorHandler } from "./errors-handler";

@NgModule({
  imports: [],
  declarations: [],
  providers: [GlobalErrorHandler]
})
export class ErrorsModule { }