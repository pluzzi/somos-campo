import { Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Events } from 'ionic-angular';
import { isNullOrUndefined } from 'util';

@Injectable()
export class GlobalErrorHandler  {
  constructor(public events: Events) {}  
  
  public handleError(errorResponse: any) {
    console.log("qmt::" + JSON.stringify(errorResponse));
    let errMsg:string;
    if (errorResponse instanceof HttpErrorResponse) {
        // Server or connection error happened
        if (!navigator.onLine) {
          // Handle offline error
          errMsg = "No hay conexión a Internet";
        } else {
          // Handle Http Error (error.status === 403, 404...)
          if(!isNullOrUndefined(errorResponse.error))
            errMsg = errorResponse.error.title;
          else
            errMsg = errorResponse.message;
        }
    } else {
      // Handle Client Error (Angular Error, ReferenceError...)     
    }        
    console.log(errMsg);
    return Observable.throw(errMsg);
  }
}