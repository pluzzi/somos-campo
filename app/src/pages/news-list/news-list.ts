import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { NoticiaProvider } from '../../providers/noticia/noticia.provider';
import { NewsDetailPage } from '../news-detail/news-detail';
import { Noticia } from '../../models/noticia.model';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-news-list',
  templateUrl: 'news-list.html',
})
export class NewsListPage implements OnInit{
  noticiaList: Noticia[] = [];
  page: number = 1;
  loading: Loading;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public noticiaProvider: NoticiaProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController) { }

  ngOnInit(){}

  ionViewWillEnter(){
    this.getNoticias(1);
  }

  getNoticias(page: number) {
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.noticiaProvider.obtenerNoticias(page).subscribe(
      data => { 
        this.noticiaList = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToDetalleNoticia(news: Noticia){
    this.navCtrl.push(NewsDetailPage, {id: news.id});
  }

  doInfinite(infiniteScroll) {
    this.page++;
    setTimeout(() => {
      this.noticiaProvider.obtenerNoticias(this.page).subscribe(
        data => { 
          for(let i=0; i < data.length; i++) {
            data[i].destacada = false;
            this.noticiaList.push(data[i]);
          }          
        },
        error => {
          this.loading.dismiss();
          this.helper.showError(error);
        }
      );
      infiniteScroll.complete();
    }, 500);
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.page = 1;
      this.getNoticias(this.page);
      refresher.complete();
    }, 2000);
  }
}