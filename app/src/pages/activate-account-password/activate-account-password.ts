import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { LoginPage } from '../login/login';
import { SettingsProvider } from '../../providers/settings/settings.provider';

@Component({
  selector: 'page-activate-account-password',
  templateUrl: 'activate-account-password.html',
})
export class ActivateAccountPasswordPage implements OnInit{
  setPasswordForm: FormGroup;
  loading: Loading;
  username: string;
  empresa: string;
  email: string;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authProvider: AuthenticationProvider,
    public formBuilder: FormBuilder,
    public loadingController:LoadingController,
    public alertController: AlertController,
    public settingsProvider:SettingsProvider
    ) { }

  ngOnInit() {
    this.username = this.navParams.data.username;
    this.empresa = this.navParams.data.empresa;
    this.email = this.navParams.data.email;
    this.setPasswordForm = this.formBuilder.group(
      {
        password: ['', Validators.compose([Validators.required])],
        confirmPassword: ['', Validators.compose([Validators.required])]
      },
      {
        validator: this.matchValidator
      } 
    );
  }

  setPassword() {
    let form = this.setPasswordForm.value;
  	if (form.password != form.confirmPassword) {
			return;
    }

    let data = {
      UserName: this.username,
      Password: form.password, 
      Email: this.email,
      Empresa: this.empresa,
      PasswordHash: this.settingsProvider.getPasswordHash()
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.authProvider.activarUsuario(data).subscribe(
      result => {
        console.log(result);
        this.loading.dismiss();
        this.processResults();
      },
      error => { 
        this.loading.dismiss();
        this.showError(error);
      }
    );
  }

  processResults(){
    let alert = this.alertController.create({
      title: 'Activar usuario',
      message: 'Su usuario se activó correctamente. Inicie sesión con los datos que acaba de ingresar.',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  matchValidator(formGroup: FormGroup) {
    let password = formGroup.controls.password.value;
    let confirmPassword = formGroup.controls.confirmPassword.value;
    if (confirmPassword.length <= 0) {
        return null;
    }
    if (confirmPassword !== password) {
        return {
            doesMatchPassword: true
        };
    }
    return null;
  }  

  showError(errMsg: string){
    let alert = this.alertController.create({
      title: 'Activar usuario',
      message: errMsg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  backToLogin (){
    this.navCtrl.pop();
  }  

}