import { Component, OnInit} from '@angular/core';
import { NavController, NavParams, App, Loading, Events, LoadingController, ToastController} from 'ionic-angular';
import { Publicacion } from '../../models/publicacion.model';
import { Formulario } from '../../models/survey.model';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { MuroProvider } from '../../providers/muro/muro.provider';
import { FormularioProvider } from '../../providers/surveys/surveys.provider';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { SurveysDetailPage } from '../surveys-detail/surveys-detail';
import { FormularioGenericoListPage } from '../formulario-generico-list/formulario-generico-list';
import { TipoFormulario } from '../../models/tipoForm.enum';
import { FiltrosPage } from '../filtros/filtros';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage implements OnInit{
  muroList: Publicacion[] = [];
  formularios: Formulario[] = [];
  loading: Loading;
  isReady: boolean = false;

  constructor(
    private app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public helper: HelperProvider,
    public muroProvider: MuroProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public formularioProvider: FormularioProvider,
    public settingsProvider: SettingsProvider
    ) {
  }

  ngOnInit(){}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  ionViewWillEnter(){
    this.cargarMuro();
    document.getElementById("search").style.display = "block";
  }

  ionViewWillLeave() {
    this.settingsProvider.setPendingRefresh(true);
    document.getElementById("search").style.display = "none";
  }

  goToFormulario(form: Formulario){
    this.app.getRootNav().setRoot(SurveysDetailPage, {id : form.id, title : "Formulario", tipoForm: TipoFormulario.Generico});
  }

  goToFiltroCategoria(){
    this.navCtrl.push(FiltrosPage);
  }

  goToFormularioGenericoList(){
    this.app.getRootNav().setRoot(FormularioGenericoListPage);
  }

  private cargarMuro(){
    window.addEventListener("contextmenu", (e) => { e.preventDefault(); });
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();  

    if( this.settingsProvider.getPendingRefresh() ){
      // Obtenemos settings de tenant
      this.settingsProvider.getSettings().subscribe(
        settings => { 
          this.events.publish('menu:populate', settings.menu);
          this.events.publish('theme:populate', settings.theme);

          // Obtenemos publicaciones del muro
          this.muroProvider.obtenerPublicaciones().subscribe(
            publicaciones => { 
              this.isReady = true;
              this.muroList = publicaciones;

              // Obtenemos los formularios genericos
              this.formularioProvider.getFormulariosGenericos().subscribe(
                formularios => {       
                  this.formularios = formularios;
                  this.loading.dismiss();        
                },
                error => { 
                  this.loading.dismiss();
                  this.helper.showError(error);
                }
              );

            },
            error => { 
              this.loading.dismiss();        
              this.helper.showError(error);;
            }
          );
        },
        error => { 
          this.loading.dismiss();
          this.helper.showError(error);
        }
      );
    } else {
      // Recien logueado: traer solo publicaciones del muro
      this.muroProvider.obtenerPublicaciones().subscribe(
        data => { 
          this.isReady = true;
          this.muroList = data;

          this.formularioProvider.getFormulariosGenericos().subscribe(
            formularios => {       
              this.formularios = formularios;
              this.loading.dismiss();        
            },
            error => { 
              this.loading.dismiss();
              this.helper.showError(error);
            }
          );
        },
        error => { 
          this.loading.dismiss();        
          this.helper.showError(error);
        }
      );
    }
  }

}
