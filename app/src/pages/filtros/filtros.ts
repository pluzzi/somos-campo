import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, App} from 'ionic-angular';
import { MuroPage } from '../muro/muro';

@Component({
  selector: 'page-filtros',
  templateUrl: 'filtros.html',
})
export class FiltrosPage implements OnInit{
  tabs: string

  categories = [
    {name:'Categoria', active: true, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
  ]

  constructor(
    private app: App,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltrosPage');
  }

  ngOnInit(){ 
    this.tabs = 'categoria';
  }

  cargarMuro(){
    this.app.getRootNav().setRoot(MuroPage, {indice: 1});
  }

  categoryToggleClass(event){
    console.log(event);
    event.active = !event.active;
  }

  segmentChanged(event){
    console.log(event);
  }

  goBack(){
    this.app.getRootNav().setRoot(MuroPage, {indice: 1});
  }

  apply(){
    this.app.getRootNav().setRoot(MuroPage, {indice: 1});
  }

}
