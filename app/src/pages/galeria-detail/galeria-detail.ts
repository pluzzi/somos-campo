import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { GaleriaProvider } from '../../providers/galeria/galeria.provider';
import { Galeria } from '../../models/galeria.model';
import { ElementoMultimedia } from '../../models/elementoMultimedia.model';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { StreamingMedia, StreamingVideoOptions} from '@ionic-native/streaming-media';

@Component({
  selector: 'page-galeria-detail',
  templateUrl: 'galeria-detail.html',
})
export class GaleriaDetailPage implements OnInit{
  imagenes: ElementoMultimedia[] = [];
  videos: ElementoMultimedia[] = [];

  galeria: Galeria;
  loading: Loading;
  isReady:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    private photoViewer: PhotoViewer,
    private streamingMedia: StreamingMedia,
    public galeriaProvider: GaleriaProvider) {
  }

  ngOnInit() {
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');

    let galeriaId: number;
    if (localRequestOrigin == LocalRequestOrigin.Menu && params.entidadId) {
      galeriaId = params.entidadId;
    } else {
      galeriaId = this.navParams.data.id;
    }
    this.obtenerGaleriaPorId(galeriaId);
  }

  obtenerGaleriaPorId(id: number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.galeriaProvider.obtenerGaleriaPorId(id).subscribe(
      data => { 
        this.isReady = true;
        this.galeria = data;

        let elementos: ElementoMultimedia[] = data.elementosMultimedia;
        for(let i=0; i < elementos.length; i++) {
          if(elementos[i].tipo == 'IMG') {
            this.imagenes.push(elementos[i]);
          } else if(elementos[i].tipo == 'VID') {
            this.videos.push(elementos[i]);
          }
        } 
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  showItem(elemento: ElementoMultimedia){
    if(elemento.tipo == 'VID'){
      let options: StreamingVideoOptions = {
        successCallback: ()=> {console.log('Video played')},
        errorCallback: (e)=> {console.log('qmt::showItem:: Error streaming: ' + JSON.stringify(e))},
        orientation: 'portrait'
      }
      this.streamingMedia.playVideo(elemento.rutaArchivo, options);

    } else if (elemento.tipo == 'IMG') {
      this.photoViewer.show(elemento.rutaArchivo);
    }
  }
}
