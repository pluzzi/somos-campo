import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit{
  user = {
    name: 'User123',
    email: 'test@test.com',
    phone: '+54 9 341 155659874'
  }

  tabs: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams
    ) { }

  ngOnInit() {
    this.tabs = "activas";
   }

  segmentChanged(event){

  }

}