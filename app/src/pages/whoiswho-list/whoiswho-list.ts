import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user.provider';
import { UserDetailPage } from '../user-detail/user-detail';
import { Usuario } from '../../models/user.model';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-whoiswho-list',
  templateUrl: 'whoiswho-list.html',
})
export class WhoiswhoListPage implements OnInit {
  readonly MIN_CHARS = 2;
  searchResultsEnabled: boolean = false;
  searchTerm: string = '';
  users: Usuario[] = [];
  pageNumberList = 1;
  pageNumberSearch: number = 1;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public userProvider: UserProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController) { }

  ngOnInit() {
    this.getUsers(this.pageNumberList);
  }

  search(event: any){
    let searchTerm: string = (event.target.value != undefined) ? event.target.value : '';
    if (searchTerm.length == 0) {
      this.getUsers(1);
    } else if (searchTerm.length >= this.MIN_CHARS){
      this.searchByName(searchTerm, 1);
    }
  }

  getUsers(page: number) {
    this.loading = this.loadingController.create({content : "Cargando, por favor espere..."});
    this.loading.present();      
    this.userProvider.getUsers(page).subscribe(
      data => { 
        this.loading.dismiss();
        this.searchResultsEnabled = false;
        this.users = data;
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  searchByName(term: string, page: number){
    this.userProvider.searchByName(term,page).subscribe(
      data => {         
        this.searchResultsEnabled = true;
        this.users = data;        
      },
      error => { 
        this.helper.showError(error);
      }
    );
  }
 
  doInfinite(infiniteScroll) {
    //TODO: refactorizar
    if (this.searchResultsEnabled){
      this.pageNumberSearch++;
      setTimeout(() => {
        this.userProvider.searchByName(this.searchTerm, this.pageNumberSearch).subscribe(
          data => {
            this.searchResultsEnabled = true;
            for(let i=0; i < data.length; i++) {
              this.users.push(data[i]);
            }
          },
          error => { 
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );        
        infiniteScroll.complete();
      }, 500);
    } else {
      this.pageNumberList++;
      setTimeout(() => {
        this.userProvider.getUsers(this.pageNumberList).subscribe(
          data => { 
            this.searchResultsEnabled = false;
            for(let i=0; i < data.length; i++) {
              this.users.push(data[i]);
            }
          },
          error => { this.helper.showError(error); }
        );
        infiniteScroll.complete();
      }, 500);      
    }
  }  

  goToUserDetail(user: Usuario){
    this.navCtrl.push(UserDetailPage, {userId: user.id});
  }

}