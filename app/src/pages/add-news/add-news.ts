import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, App } from 'ionic-angular';
import { MuroPage } from '../muro/muro';


@Component({
  selector: 'page-add-news',
  templateUrl: 'add-news.html',
})
export class AddNewsPage implements OnInit {
  indice = 0;
  showBack = false;
  labelNext = "Siguiente"

  pages = [
    'tipo',
    'detalles',
    'imagenes',
    'zona',
    'categorias',
    'vista'
  ];

  tabs: string;
  vistas: string;

  items = [
    {name:'1', active: true, img: './assets/img/vaca.jpg'},
    {name:'2', active: false, img: ''},
    {name:'3', active: false, img: ''},
    {name:'4', active: false, img: ''},
    {name:'5', active: false, img: ''},
    {name:'6', active: false, img: ''},
    {name:'7', active: false, img: ''},
    {name:'8', active: false, img: ''},
    {name:'9', active: false, img: ''},
    {name:'10', active: false, img: ''},
    {name:'11', active: false, img: ''},
    {name:'12', active: false, img: ''},
    {name:'13', active: false, img: ''},
    {name:'14', active: false, img: ''},
    {name:'15', active: false, img: ''},
    {name:'16', active: false, img: ''}
  ];

  categories = [
    {name:'Categoria', active: true, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
    {name:'Categoria', active: false, img: './assets/icon/icon-round.png'},
  ]

  constructor(
    private app: App,
    public navCtrl: NavController,
    public navParams: NavParams) {

  }

  ngOnInit(){
    this.tabs = this.pages[this.indice];
    this.vistas = "previa";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewsPage');
  }

  goBack(){
    this.indice--;
    this.tabs = this.pages[this.indice];
    if(this.indice==0){
      this.showBack = false;
    }
    if(this.indice<4){
      this.labelNext = "Siguiente";
    }
    if(this.indice==4){
      this.labelNext = "Aplicar";
    }
    console.log(this.showBack);
  }

  goNext(){
    this.indice++;
    this.tabs = this.pages[this.indice];
    if(this.indice!=0){
      this.showBack = true;
    }
    if(this.indice==4){
      this.labelNext = "Aplicar";
    }
    if(this.indice==5){
      this.labelNext = "Pagar y publicar";
    }
    if(this.indice>5){
      this.app.getRootNav().setRoot(MuroPage, {indice: 1});
    }
    console.log(this.showBack);
  }

  toggleClass(item){
    item.active = !item.active;
  }

  categoryToggleClass(category){
    category.active = !category.active;
  }

  vistaChanged(vista){
    //this.vistas = vista;
  }
}
