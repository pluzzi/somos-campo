import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { GaleriaProvider } from '../../providers/galeria/galeria.provider';
import { CategoriaProvider } from '../../providers/categoria/categoria';
import { Categoria } from '../../models/categorias';
import { HelperProvider } from '../../providers/helper/helper.provider';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { Galeria } from '../../models/galeria.model';
import { GaleriaDetailPage } from '../galeria-detail/galeria-detail';

@Component({
  selector: 'page-galeria-list',
  templateUrl: 'galeria-list.html',
})

export class GaleriaListPage implements OnInit{
  public selectedCat;
  categoriaList: Categoria[] = [];
  loading: Loading;
  modulo: string = "GALER";
  galerias: Galeria[] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingController: LoadingController,
    public helper: HelperProvider,
    public galeriaProvider: GaleriaProvider,
    public categoriaProvider: CategoriaProvider) {
  }
  
  ngOnInit() { 
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');
    if (localRequestOrigin == LocalRequestOrigin.Menu && params.categoriaId) {
      this.obtenerCategorias(this.modulo, params.categoriaId);
      this.obtenerGaleriasPorCategoria(params.categoriaId);
    } else {
      this.obtenerCategorias(this.modulo);
      this.obtenerGalerias();
    }
  }

  obtenerGalerias(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();   
    this.galeriaProvider.obtenerTodoGalerias().subscribe(
     data => {
       this.galerias = data;
       this.loading.dismiss();       
     },
     error => {
       this.loading.dismiss();
       this.helper.showError(error);
     }
   );
  }

  obtenerGaleriasPorCategoria(categoriaId:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present(); 
    this.galeriaProvider.obtenerGaleriasPorCategoria(categoriaId).subscribe(
      data => {  
        this.loading.dismiss();          
        this.galerias = data;         
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      })    
  }

  obtenerCategorias(modulo: string, selected?:number){
    this.categoriaProvider.obtenerCategorias(modulo).subscribe(
      data => {     
        this.categoriaList = data;
        if(selected) {
          this.selectedCat = selected;
        }
      },
      error => {
        this.helper.showError(error);
      }
    )
  }

  goToDetail(galeria: Galeria){
    this.navCtrl.push(GaleriaDetailPage, {id: galeria.id});
  }

  onChange(categoriaId: number){
    this.galerias = [];
    if(categoriaId == 0) {
      this.obtenerGalerias();
    } else{
      this.obtenerGaleriasPorCategoria(categoriaId);
    }
  }
}