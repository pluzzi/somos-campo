import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { BeneficioProvider } from '../../providers/benefits/benefits.provider';

import { BenefitsDetailPage } from '../benefits-detail/benefits-detail';
import { Beneficio } from '../../models/benefits.model';
import { BeneficioCategoria } from '../../models/benefitCategory.model';
import { LocalRequestOrigin } from '../../models/LocalRequestOrigin.enum';
import { HelperProvider } from '../../providers/helper/helper.provider';


@Component({
  selector: 'page-benefits-list',
  templateUrl: 'benefits-list.html',
})
export class BenefitsListPage implements OnInit {
  beneficioList: Beneficio[] = [];
  page: number;
  pageLikes: number;
  selectCategory: string = "";
  categoriesList: BeneficioCategoria[] = [];
  flagTabSelected = true;
  selectedCat: string = "";
  benefitsFavByCat: Beneficio [] = [];
  loading: Loading;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public beneficioProvider: BeneficioProvider) {
      this.page = 1;
      this.pageLikes = 1;
  }

  ngOnInit(){}  

  ionViewWillEnter(){
    let params:any = this.navParams.get('params');
    let localRequestOrigin:LocalRequestOrigin = this.navParams.get('localRequestOrigin');
    if (localRequestOrigin == LocalRequestOrigin.Menu && params.categoriaId) {
      this.getCategorias(params.categoriaId);
      this.getBeneficiosByCategoriaId(params.categoriaId, 1);
    } else {
      this.getCategorias();
      this.getBeneficios(1);
    }
  }

  getBeneficios(page: number){
    this.page = page;
    this.flagTabSelected = true;
    this.selectedCat = "";
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.beneficioProvider.getBeneficios(this.page).subscribe(
      data => {
        this.beneficioList = data;
        this.loading.dismiss();        
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  getBeneficiosByCategoriaId(categoriaId:number, page:number) {
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.beneficioProvider.getBeneficiosByCategoriaId(categoriaId, page).subscribe(
      data => {            
        this.beneficioList = data; 
        this.loading.dismiss();          
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);          
      }
    );
  }

  getBenefitsLikes(pageLks: number){
    this.pageLikes = pageLks;
    this.flagTabSelected = false;
    this.selectedCat = "";
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.beneficioProvider.getBenefitsLikes(this.pageLikes).subscribe(
      data => {
        this.beneficioList = data;
        this.loading.dismiss();        
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);        
      }
    );
  }

  like(beneficio: Beneficio){
    this.beneficioProvider.getLikes(beneficio.id).subscribe(
      data => {
        beneficio.liked = !beneficio.liked;                    
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToBeneficioDetail(beneficio: Beneficio){
    this.navCtrl.push(BenefitsDetailPage, {id: beneficio.id});
  }

  getCategorias(selected?:any){   
    this.beneficioProvider.getCategorias().subscribe(
      data => {
        this.categoriesList = data; 
        if(selected) {
          this.selectedCat = selected;
        }
      },
      error => { 
        this.helper.showError(error);
      }
    );
  }

  onChange(categoriaId) {  
    this.getCategorias();
    this.page = 1;
    this.beneficioList = []; 
    this.benefitsFavByCat = [];
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    if(this.flagTabSelected){
      this.beneficioProvider.getBeneficiosByCategoriaId(categoriaId, this.page).subscribe(
        data => {            
          this.beneficioList = data; 
          this.loading.dismiss();          
        },
        error => { 
          this.loading.dismiss();
          this.helper.showError(error);          
        }
      );
    }
    else{    
      this.beneficioProvider.getBeneficiosByCategoriaId(categoriaId, this.page).subscribe(
        data => { 
          this.loading.dismissAll(); 
          let beneficioListTemp = data;                               
          for (let i in beneficioListTemp) {
            if(beneficioListTemp[i].liked){
              let beneficio: Beneficio = new Beneficio();            
              beneficio.id = beneficioListTemp[i].id;
              beneficio.titulo = beneficioListTemp[i].title;
              beneficio.copete = beneficioListTemp[i].subtitle;
              beneficio.contenido = beneficioListTemp[i].content;
              beneficio.imagen = beneficioListTemp[i].image;
              beneficio.liked = beneficioListTemp[i].liked;
              this.benefitsFavByCat.push(beneficio);
            }
          }
          this.beneficioList = this.benefitsFavByCat;
        },
        error => { 
          this.loading.dismiss();
          this.helper.showError(error);         
        }
      );
    }    
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.pageLikes++;
    if(this.flagTabSelected){
      setTimeout(() => {
        this.beneficioProvider.getBeneficios(this.page).subscribe(
          data => { 
            for(let i=0; i < data.length; i++) {            
              this.beneficioList.push(data[i]);
            }          
          },
          error => {
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
        infiniteScroll.complete();
      }, 500);
    }
    if(!this.flagTabSelected){
      setTimeout(() => {
        this.beneficioProvider.getBenefitsLikes(this.pageLikes).subscribe(
          data => { 
            for(let i=0; i < data.length; i++) {            
              this.beneficioList.push(data[i]);
            }          
          },
          error => {
            this.loading.dismiss();
            this.helper.showError(error);
          }
        );
        infiniteScroll.complete();
      }, 500);
    }
  }

  doRefresh(refresher) {
    if(this.flagTabSelected){
      setTimeout(() => {
        this.page = 1;
        this.getBeneficios(this.page);
        refresher.complete();
      }, 2000);
    }
    if(!this.flagTabSelected){
      setTimeout(() => {
        this.pageLikes = 1;
        this.getBenefitsLikes(this.pageLikes);
        refresher.complete();
      }, 2000);
    }
  }

}
