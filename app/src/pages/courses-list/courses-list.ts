import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, ToastController } from 'ionic-angular';
import { CoursesProvider } from '../../providers/courses/courses.provider';
import { Capacitacion} from '../../models/courses.model';
import { CoursesDetailPage } from '../courses-detail/courses-detail';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-courses-list',
  templateUrl: 'courses-list.html',
})
export class CoursesListPage implements OnInit{
  coursesList: Capacitacion [] = [];
  loading: Loading;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public coursesProvider: CoursesProvider,
    public loadingController: LoadingController,
    public toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.getCapacitaciones();
  }

  getCapacitaciones(){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.coursesProvider.getCapacitaciones().subscribe(
      data => {       
        this.coursesList = data;
        this.loading.dismiss();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToCapacitacion(id: number) {
    this.navCtrl.push(CoursesDetailPage, {id : id});
  }
}
