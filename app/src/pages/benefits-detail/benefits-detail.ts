import { Component, OnInit, HostListener } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { BeneficioProvider } from '../../providers/benefits/benefits.provider';
import { Beneficio } from '../../models/benefits.model';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-benefits-detail',
  templateUrl: 'benefits-detail.html',
})
export class BenefitsDetailPage {
  loading: Loading;
  beneficio: Beneficio;
  isReady: boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public beneficioProvider: BeneficioProvider,
    private theInAppBrowser: InAppBrowser,
    public loadingController:LoadingController,
    public toastCtrl: ToastController) {
  }

  ngOnInit() {
    let beneficioId: number = this.navParams.data.id;
    this.getBenefitBydId(beneficioId);    
  }

  getBenefitBydId(id:number){
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();    
    this.beneficioProvider.getBeneficioById(id).subscribe(
      data => { 
        this.isReady = true;
        this.beneficio = data;
        this.loading.dismiss();
      },
      error => {
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  like(beneficio: Beneficio){
    this.beneficioProvider.getLikes(beneficio.id).subscribe(
      data => {
        beneficio.liked = !beneficio.liked;                    
      },
      error => { 
        this.helper.showError(error);
      }
    );
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: EventClick) {
     if(event.target && event.target.href){
        this.openBrowser(event.target.href);
     }
     return false; 
  }

  openBrowser(url){
    const options: InAppBrowserOptions = {
      location:'no'
    };
    this.theInAppBrowser.create(url, '_system', options);
  }

}
