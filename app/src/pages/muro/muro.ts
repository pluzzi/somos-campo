import { Component } from '@angular/core';
import { NavController, App, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { WhoiswhoListPage } from '../whoiswho-list/whoiswho-list';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { SearchPage } from '../search/search';
import { ProfilePage } from '../profile/profile';
import { AddNewsPage } from '../add-news/add-news';

@Component({
  selector: 'page-muro',
  templateUrl: 'muro.html',
})
export class MuroPage {
  indice: number; 

  tab1Root = HomePage;
  tab2Root = SearchPage;
  tab3Root = AddNewsPage;
  tab4Root = ProfilePage;

  constructor(
    public navCtrl: NavController, 
    public app: App,
    public settingsProvider: SettingsProvider,
    public navParams: NavParams) {
      this.indice = this.navParams.get("indice");
    }

  goToQuienEsQuienPage() {
    this.app.getRootNav().setRoot(WhoiswhoListPage);
  }

  ionViewWillLeave() {
    this.settingsProvider.setPendingRefresh(true);
  }
}