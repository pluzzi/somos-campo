import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { BirthdayProvider } from '../../providers/birthday/birthday.provider';
import { UserDetailPage } from '../user-detail/user-detail';
import { Birthday } from '../../models/birthday.model';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-birthday-list',
  templateUrl: 'birthday-list.html',
})
export class BirthdayListPage implements OnInit {
  birthdays: Birthday[]= [];
  page: number = 1;
  loading: Loading;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public birthdayProvider: BirthdayProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController) { }

  ngOnInit(){
    this.getBirthdays(this.page);
  }

  getBirthdays(page: number) {
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();      
    this.birthdayProvider.getBirthdays(page).subscribe(
      data => { 
        this.birthdays = data;
        this.loading.dismissAll();
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  goToUserDetail(id: string){
    this.navCtrl.push(UserDetailPage, {userId: id});
  }
}