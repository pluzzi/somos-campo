import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication.provider';
import { HomePage } from '../home/home';
import { MuroPage } from '../muro/muro';
import { HelperProvider } from '../../providers/helper/helper.provider';

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage implements OnInit {
  changePasswordForm: FormGroup;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public authProvider: AuthenticationProvider,
    public formBuilder: FormBuilder,
    public loadingController:LoadingController, 
    public toastCtrl: ToastController) {
  }

  ngOnInit(){
    this.changePasswordForm = this.formBuilder.group(
      {
        password: ['', Validators.compose([Validators.required])],
        confirmPassword: ['', Validators.compose([Validators.required])]
      },
      {
        validator: this.matchValidator
      }
    );
  }

  changePassword(){
    let form = this.changePasswordForm.value;
  	if (form.password != form.confirmPassword) {
			return;
    }

    let data = { 
      Password: form.password, 
      passwordConfirm: form.confirmPassword 
    };
    this.loading = this.loadingController.create({content : "Procesando, por favor espere..."});
    this.loading.present();
    this.authProvider.changePassword(data).subscribe(
      () => {
        this.loading.dismiss();
        this.navCtrl.setRoot(MuroPage);
      },
      error => { 
        this.loading.dismiss();
        this.helper.showError(error);
      }
    );
  }

  matchValidator(formGroup: FormGroup) {
    let password = formGroup.controls.password.value;
    let confirmPassword = formGroup.controls.confirmPassword.value;
    if (confirmPassword.length <= 0) {
      return null;
    }
    if (confirmPassword !== password) {
      return {
          doesMatchPassword: true
      };
    }
    return null;
  }
  
  backToLogin(){
    this.navCtrl.pop();
  }

}