export class Option{
    private Id: number;
    private Description: string;
    private AnswerSelected: boolean;
    private IsRightAnswer: boolean;
    private Enabled: boolean;
    
    constructor(){}
    get id(): number {
        return this.Id;
    }
    set id(id: number) {
        this.Id = id;
    }
    get description(): string{
        return this.Description;
    }
    set description(description: string){
        this.Description = description;
    }
    get answerSelected(): boolean {
        return this.AnswerSelected;
    }
    set answerSelected(answerSelected: boolean) {
        this.AnswerSelected = answerSelected;
    }
    get isRightAnswer(): boolean {
        return this.IsRightAnswer;
    }
    set isRightAnswer(isRightAnswer: boolean) {
        this.IsRightAnswer = isRightAnswer;
    }
    get enabled(): boolean {
        return this.Enabled;
    }
    set enabled(enabled: boolean) {
        this.Enabled = enabled;
    }
    
}