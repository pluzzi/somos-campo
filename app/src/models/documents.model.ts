export class Documento{
    private _id: number;
    private _title: string;
    private _description: string;
    private _filePath: string;
    private _extension: string;
    private _reaccionId: number;
    private _totalReacciones: number;    
    
    get id(): number {
        return this._id;
    }
    set id(id: number) {
        this._id = id;
    }
    get title(): string{
        return this._title;
    }
    set title(title: string){
        this._title = title;
    }
    get description(): string{
        return this._description;
    }
    set description(description: string){
        this._description = description;
    }
    get filePath(): string{
        return this._filePath;
    }
    set filePath(filePath: string){
        this._filePath = filePath;
    }
    get extension(): string{
        return this._extension;
    }
    set extension(extension: string){
        this._extension = extension;
    }

    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }
}