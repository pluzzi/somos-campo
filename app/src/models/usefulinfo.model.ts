export class Info{
    private _id: number;
    private _categoriaId: number;
    private _title : string;
    private _copete: string;
    private _html: string;
    private _icon: string;
    private _order: number;
    private _destacada: boolean;
    private _enabled: boolean;
    private _image: string;
    private _reaccionId: number;
    private _totalReacciones: number;  

    get id(): number {
        return this._id;
    }
    set id(id: number) {
        this._id = id;
    }
    get categoriaId(): number {
        return this._categoriaId;
    }
    set categoriaId(categoriaId: number) {
        this._categoriaId = categoriaId;
    }
    get title(): string{
        return this._title;
    }
    set title(title: string){
        this._title = title;
    }
    get copete(): string{
        return this._copete;
    }
    set copete(copete: string){
        this._copete = copete;
    }
    get html():string {
        return this._html;
    }
    set html(html: string){
        this._html = html;
    }
    get icon():string {
        return this._icon;
    }
    set icon(icon: string){
        this._icon = icon;
    }
    get order(): number{
        return this._order;
    }
    set order(order: number){
        this._order = order;
    }
    get enabled():boolean{
        return this._enabled;
    }
    set enabled(enabled: boolean){
        this._enabled = enabled;
    }    
    get destacada():boolean{
        return this._destacada;
    }
    set destacada(destacada: boolean){
        this._destacada = destacada;
    }
    get image(): string {
        return this._image;
    }
    set image(image: string) {
        this._image = image;
    }
    
    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }

}