interface EventClickTarget extends EventTarget {
    href: string    
}
  
interface EventClick extends EventTarget {
    target: EventClickTarget
}