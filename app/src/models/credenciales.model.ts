export class Credencial {
    private _id: string;
    private _html: string;

    constructor(id: string) {
        this._id = id;
    }
    get id(): string {
        return this._id;
    }
    set id(id: string) {
        this._id = id;
    }

    get html(): string {
        return this._html;
    }
    set html(html: string) {
        this._html = html;
    }    

}