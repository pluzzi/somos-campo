import { Question } from "./question.model";

export class FormularioGenerico{
   private FormId: number;         
   private Title: string;       
   private Description: string;
   private FormTypeId: number; 
   private Questions: Question[];
   private AlreadyAnswered: boolean;
   private UniqueReply: boolean;

   constructor(){}

   get id(): number {
        return this.FormId;
    }
    set id(id: number) {
        this.FormId = id;
    }
    get title(): string{
        return this.Title;
    }
    set title(title: string){
        this.Title = title;
    }
    get description(): string{
        return this.Description;
    }
    set description(description: string){
        this.Description = description;
    }
    get formTypeId(): number {
        return this.FormTypeId;
    }
    set formTypeId(formTypeId: number) {
        this.FormTypeId = formTypeId;
    }
       
    get questions(): Question[]{
        return this.Questions;
    }
    set questions(questions: Question[]){                
        this.Questions= questions;        
    }
    get alreadyAnswered(): boolean{
        return this.AlreadyAnswered;
    }
    set alreadyAnswered(alreadyAnswered: boolean){
        this.AlreadyAnswered = alreadyAnswered;
    }
    get uniqueReply(): boolean{
        return this.UniqueReply;
    }
    set uniqueReply(uniqueReply: boolean){
        this.UniqueReply = uniqueReply;
    }
}

