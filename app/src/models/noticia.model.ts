export class Noticia {
    private _id: string;
    private _contenido: string;
    private _fechaAlta: string;
    private _imagen: string;
    private _titulo: string;
    private _copete: string;
    private _destacada: boolean;
    private _reaccionId: number;
    private _totalReacciones: number;

    get id(): string {
        return this._id;
    }
    set id(value: string) {
        this._id = value;
    }
    get titulo(): string {
        return this._titulo;
    }
    set titulo(value: string) {
        this._titulo = value;
    }    
    get copete(): string {
        return this._copete;
    }
    set copete(value: string) {
        this._copete = value;
    }    
    get contenido(): string {
        return this._contenido;
    }
    set contenido(value: string) {
        this._contenido = value;
    }
    get fechaAlta(): string {
        return this._fechaAlta;
    }
    set fechaAlta(date: string) {
        this._fechaAlta = date;
    }
    get imagen(): string {
        return this._imagen;
    }
    set imagen(value: string) {
        this._imagen = value;
    }
    get destacada(): boolean {
        return this._destacada;
    }
    set destacada(value: boolean) {
        this._destacada = value;
    } 
    get reaccionId(): number {
        return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }
}