export class Birthday {
    private _userId: string;
    private _name: string;
    private _lastName: string;  
    private _area: string;
    private _photo: string;
    private _date: string;
    private _isBirthdayToday: boolean;
    private _reaccionId: number;
    private _totalReacciones: number;         

    get userId(): string {
        return this._userId;
    }
    set userId(value: string) {
        this._userId = value;
    }
    get name(): string {
      return this._name;
    }
    set name(value: string) {
      this._name = value;
    }
    get lastName(): string {
      return this._lastName;
    }
    set lastName(value: string) {
      this._lastName = value;
    }
    get area(): string {
      return this._area;
    }
    set area(value: string) {
      this._area = value;
    }
    get photo(): string {
      return this._photo;
    }
    set photo(value: string) {
      this._photo = value;
    }
    get date(): string {
      return this._date;
    }
    set date(value: string) {      
        this._date = value;
    }
    get isBirthdayToday(): boolean {
      return this._isBirthdayToday;
    }
    set isBirthdayToday(value: boolean) {      
        this._isBirthdayToday = value;
    }  

    get reaccionId(): number {
      return this._reaccionId;
    }
    set reaccionId(value: number) {
        this._reaccionId = value;
    }
    get totalReacciones(): number {
        return this._totalReacciones;
    }
    set totalReacciones(value: number) {
        this._totalReacciones = value;
    }

  }