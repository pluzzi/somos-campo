import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class ReaccionProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

    guardar(data: any){
        let apiUrl =  this.settingsProvider.getApiUrl() + '/reacciones/guardar';
        return this.httpClient.post(apiUrl, data).pipe(
          map(response => {
            console.log(response);
          }),
          catchError(this.globalErrorHandler.handleError)
        );
    }
}