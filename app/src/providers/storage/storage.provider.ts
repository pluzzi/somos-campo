import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';


@Injectable()
export class StorageProvider {

  constructor(public http: HttpClient) {
    console.log('qmt::StorageProvider>>constructor');
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  saveToken(token: string): void {
    console.log('qmt::StorageProvider>>saveToken');
    localStorage.setItem('token', token);
  }

  existsToken(){
    let token = localStorage.getItem('token');
    return (!isNullOrUndefined(token));
  }

}