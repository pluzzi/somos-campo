import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Usuario } from '../../models/user.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class UserProvider {
  currentUser: Usuario;

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

  setCurrentUser(user:Usuario): void{
    this.currentUser = user;
  }

  getUserId(){
    return this.currentUser && this.currentUser.id;
  }

  getFullName(){
    return this.currentUser && this.currentUser.fullname;
  }
 
  getUsername(){
    return this.currentUser && this.currentUser.username;
  }

  getPhoto() {
    return this.currentUser && this.currentUser.photo;
  }

  getUserProfile(): Observable<Usuario> {
    console.log("qmt::UserProvider>>getUserProfile");
    console.log("qmt::Recuperando info de usuario actual");

    let apiUrl =  this.settingsProvider.getApiUrl() + '/myprofile';
    return this.httpClient.get<Usuario>(apiUrl).pipe(
      map(dbProfile => {
        console.log("qmt::Datos de usuario recuperados");
        return this.mapper(dbProfile);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getUserBydId(id:number): Observable<Usuario> {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/' + id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbUser => {
        return this.mapper(dbUser);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  getUsers(page:number): Observable<Usuario[]> {
    let users: Usuario[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/page/' + page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbUsers => {
        for (let i in dbUsers) {
          users.push(this.mapper(dbUsers[i]));
        }
        return users;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  searchByName(term: string, page: number): Observable<Usuario[]>{
    let users: Usuario[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/search/' + term + '/page/' + page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbUsers => {
        for (let i in dbUsers) {
          users.push(this.mapper(dbUsers[i]));
        }
        return users;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  editPassword(data: any) {
    let apiUrl =  this.settingsProvider.getApiUrl() + '/users/edit';
    return this.httpClient.post(apiUrl, data).pipe(
      map(response => {
        console.log(response);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any): Usuario{
    let usuario: Usuario = new Usuario(dto['Id']);
    usuario.username = dto['Username'];
    usuario.email = dto['Email'];
    usuario.photo = dto['Image'];
    usuario.name = dto['Name'];
    usuario.lastName = dto['LastName'];
    usuario.fullname = dto['FullName'];
    usuario.area = dto['Area'];
    usuario.phone = dto['Phone'];
    usuario.mobile = dto['Mobile'];
    usuario.birthDate = dto['BirthFriendlyDate'];
    usuario.intern = dto['Intern'];
    return usuario;
  }

}