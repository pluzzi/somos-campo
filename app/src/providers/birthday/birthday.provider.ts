import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Birthday } from '../../models/birthday.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class BirthdayProvider {
  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

  public getBirthdays(page:number): Observable<Birthday[]>{
    let birthdays: Birthday[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/birthdays/page/' + page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbBirthdays => {
        for (let i in dbBirthdays) {
          birthdays.push(this.mapper(dbBirthdays[i]));
        }
        return birthdays;        
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any): Birthday {
    let birthday: Birthday = new Birthday();
    birthday.userId = dto['Id'];
    birthday.name = dto['Name'];
    birthday.lastName = dto['Surname'];
    birthday.photo = dto['Image'];
    birthday.area = (dto['Area']) ? dto['Area'].Name : '';
    birthday.isBirthdayToday = dto['IsBirthdayToday'];          
    birthday.date = dto['BirthFriendlyDate'];
    birthday.reaccionId = dto['ReaccionId'];
    birthday.totalReacciones = dto['TotalReacciones'];
    return birthday;
  }  
}
