import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Capacitacion } from '../../models/courses.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class CoursesProvider {

  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

    getCapacitaciones(): Observable<Capacitacion[]> {
      let capacitacionList: Capacitacion[] = [];
      let apiUrl = this.settingsProvider.getApiUrl() + '/courses/getCourses';
      return this.httpClient.get(apiUrl).pipe(
        map(dbCapacitacionList => {
          for(let i in dbCapacitacionList){
            capacitacionList.push(this.mapper(dbCapacitacionList[i]));
          }
          return capacitacionList;
        }),
        catchError(this.globalErrorHandler.handleError)
      );
    }

    getCapacitacionById(id: number) {
      let apiUrl =  this.settingsProvider.getApiUrl() + '/courses/getCourseById/'+ id;
      return this.httpClient.get(apiUrl).pipe(
        map(dbCourses => {
          return this.mapper(dbCourses);
        }),
        catchError(this.globalErrorHandler.handleError)
      );
    }

    private mapper(dto: any): Capacitacion{
      let capacitacion: Capacitacion = new Capacitacion();            
      capacitacion.id = dto['Id'];
      capacitacion.title = dto['Title'];
      capacitacion.description = dto['Description'];
      capacitacion.courseCategoryId = dto['CourseCategoryId'];
      capacitacion.courseCategory = dto['CourseCategory'];
      capacitacion.courseType = dto['CourseType'];
      capacitacion.courseTypeId = dto['CourseTypeId'];
      capacitacion.documentURL = dto['DocumentURL'];
      capacitacion.extension = dto['Extension'];
      capacitacion.videoURL = dto['VideoURL'];
      capacitacion.linkURL = dto['LinkURL'];
      capacitacion.requiresEvaluation = dto['RequiresEvaluation'];
      capacitacion.userWasAlreadyEvaluated = dto['UserWasAlreadyEvaluated'];
      capacitacion.approvedPercRequired = dto['ApprovedPercRequired'];
      capacitacion.approvedPercUser = dto['ApprovedPercUser'];
      capacitacion.formId = dto['FormId'];
      capacitacion.formUniqueReply = dto['FormUniqueReply'];
      capacitacion.evaluationDate = dto['EvaluationDate'];
      capacitacion.reaccionId = dto['ReaccionId'];
      capacitacion.totalReacciones = dto['TotalReacciones'];      
      return capacitacion;
    }
}
