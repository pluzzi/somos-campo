import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { GlobalErrorHandler } from '../../app/core/errors/errors-handler';
import { Noticia } from '../../models/noticia.model';
import { SettingsProvider } from '../settings/settings.provider';

@Injectable()
export class NoticiaProvider {
  constructor(
    public httpClient: HttpClient,
    public globalErrorHandler: GlobalErrorHandler,
    public settingsProvider:SettingsProvider) {}

  obtenerNoticias(page:number): Observable<Noticia[]> {
    let noticiaList: Noticia[] = [];
    let apiUrl =  this.settingsProvider.getApiUrl() + '/news/page/' + page;
    return this.httpClient.get(apiUrl).pipe(
      map(dbNoticiaList => {
        for (let i in dbNoticiaList) {
          noticiaList.push(this.mapper(dbNoticiaList[i]));
        }
        return noticiaList;
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  obtenerNoticiaPorId(id:number): Observable<Noticia>{
    let apiUrl =  this.settingsProvider.getApiUrl() + '/news/' + id;
    return this.httpClient.get(apiUrl).pipe(
      map(dbNoticia => {
        return this.mapper(dbNoticia);
      }),
      catchError(this.globalErrorHandler.handleError)
    );
  }

  private mapper(dto: any): Noticia {
    let noticia: Noticia = new Noticia();
    noticia.id = dto['Id'];
    noticia.titulo = dto['Title'];
    noticia.copete = dto['Copete'];
    noticia.contenido = dto['Content'];
    noticia.imagen = dto['Image'];
    noticia.fechaAlta = dto['Date'];
    noticia.destacada = dto['Featured'];
    noticia.reaccionId = dto['ReaccionId'];
    noticia.totalReacciones = dto['TotalReacciones'];
    return noticia;
  }
}
