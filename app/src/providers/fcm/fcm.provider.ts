import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { DeviceProvider } from '../device/device.provider';

@Injectable()
export class FcmProvider {

  constructor(
    public httpClient: HttpClient,
    public device: Device,
    public deviceProvider: DeviceProvider,
    public firebaseNative: Firebase,
    private platform: Platform
  ) { }

  async getToken(userId:number){
    let token;
    if(this.platform.is('android')){
      token = await this.firebaseNative.getToken();
    }
    if(this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }
    return this.saveToken(userId, token);
  }

  private saveToken(userId:number, token:string){
    if(!token) return;
    
    let uuid:string = this.device.uuid;
    let data = {
      UserId: userId,
      Uuid: uuid,
      TokenNotification: token
    };
    this.deviceProvider.registerDevice(data);
  }
  
  listenToNotifications() {
    // Listen to incoming FCM messages
    return this.firebaseNative.onNotificationOpen();
  }
}