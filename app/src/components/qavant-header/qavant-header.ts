import { Component, Input} from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { MuroPage } from '../../pages/muro/muro';

@Component({
  selector: 'qavant-header',
  templateUrl: 'qavant-header.html'
})
export class QavantHeaderComponent {
  @Input() title : string;
  @Input() back : boolean;
  @Input() indice: number;

  constructor(
    private app: App,
    public navCtrl: NavController
  ) {}

  goBack(){
    this.app.getRootNav().setRoot(MuroPage, {indice: this.indice});
  }
}
