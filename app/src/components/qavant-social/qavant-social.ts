import { Component, Input } from '@angular/core';
import { SettingsProvider } from '../../providers/settings/settings.provider';
import { Reaccion } from '../../models/reaccion.model';
import { ReaccionProvider } from '../../providers/reaccion/reaccion.provider';
import { LoadingController, ToastController } from 'ionic-angular';

@Component({
  selector: 'qavant-social',
  templateUrl: 'qavant-social.html'
})
export class QavantSocialComponent {
  @Input() modulo : string;
  @Input() entidad : any;
  
  alive: boolean;
  reacciones: Reaccion[] = [];
  reaccionIdActual: number;
  reaccionIconoActual: string;
  reaccionEtiquetaActual: string;
  totalReacciones: number;

  constructor(
    public reaccionProvider: ReaccionProvider,
    public loadingController:LoadingController,
    public toastCtrl: ToastController,
    public settingsProvider: SettingsProvider) {
      this.reacciones = this.settingsProvider.getReacciones();
  }
  
  ngOnInit() {
    this.reaccionIconoActual = this.getIconoReaccion(this.entidad.reaccionId);
    this.reaccionEtiquetaActual = this.getEtiquetaReaccion(this.entidad.reaccionId);
    this.totalReacciones = this.entidad.totalReacciones;
    this.reaccionIdActual = this.entidad.reaccionId;
  }

  guardar(reaccionId:any) {
    this.alive = !this.alive;
    let data = {
      Modulo: this.modulo,
      EntidadId: this.entidad.id,
      ReaccionId: reaccionId
    };
    this.reaccionProvider.guardar(data).subscribe(
      () => {
        this.reaccionIconoActual = this.getIconoReaccion(reaccionId);
        this.reaccionEtiquetaActual  = this.getEtiquetaReaccion(reaccionId);
        if(this.reaccionIdActual == 0) {
          this.reaccionIdActual = reaccionId;
          this.totalReacciones++;
        }
      },
      error => {
        this.showError(error);
      }
    );
  }

  getIconoReaccion(id: number): string {
    let icono: string = "";
    let reaccion:Reaccion;
    if(id == 0) {
      reaccion = this.reacciones.find(x => x.id == 2);
    } else {
      reaccion = this.reacciones.find(x => x.id == id);
    }
    if(reaccion) {
      icono = reaccion.icono;
    } 
    return icono;
  }

  getEtiquetaReaccion(id: number): string {
    let etiqueta: string = "";
    let reaccion:Reaccion;
    if(id == 0) {
      reaccion = this.reacciones.find(x => x.id == 1);
    } else {
      reaccion = this.reacciones.find(x => x.id == id);
    }
    if(reaccion) {
      etiqueta = reaccion.nombre;
    } 
    return etiqueta;
  }


  toggle(){
    this.alive = !this.alive;
  }

  showError(error: any){
    let toast = this.toastCtrl.create({
      message: (error ? error : "Error"),
      cssClass: "toast-danger",
      duration: 3000
    });
    toast.present(); 
  }
}