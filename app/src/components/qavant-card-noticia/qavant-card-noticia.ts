import { Component, Input } from '@angular/core';
import { NewsDetailPage } from '../../pages/news-detail/news-detail';
import { NavController } from 'ionic-angular';
@Component({
  selector: 'qavant-card-noticia',
  templateUrl: 'qavant-card-noticia.html'
})
export class QavantCardNoticiaComponent {
  @Input() entidad : any;

  constructor(public navCtrl: NavController) {
  }
  
  goToDetail(item: any) {
    this.navCtrl.push(NewsDetailPage, {id: item.id});
  }
}
