import { Component } from '@angular/core';

/**
 * Generated class for the QavantCardCumpleComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'qavant-card-cumple',
  templateUrl: 'qavant-card-cumple.html'
})
export class QavantCardCumpleComponent {

  text: string;

  constructor() {
    console.log('Hello QavantCardCumpleComponent Component');
    this.text = 'Hello World';
  }

}
