import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BenefitsDetailPage } from '../../pages/benefits-detail/benefits-detail';
@Component({
  selector: 'qavant-card-beneficio',
  templateUrl: 'qavant-card-beneficio.html'
})
export class QavantCardBeneficioComponent {
  @Input() entidad : any;
  constructor(public navCtrl: NavController) {
  }

  goToDetail(item: any) {
    this.navCtrl.push(BenefitsDetailPage, {id: item.id});
  }

}
