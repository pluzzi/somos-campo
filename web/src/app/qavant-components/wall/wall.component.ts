import {Component, OnInit, ViewEncapsulation, OnDestroy} from '@angular/core';
import {IsotopeOptions} from 'ngx-isotope';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {WallService } from '@services/wall/wall.service';
import {SettingsService} from '@services/settings/settings.service';
import {CardData} from '@interface/cardData';
import {isNullOrUndefined} from 'util';
declare var stepsForm: any;
declare var pg: any;
@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class WallComponent implements OnInit, OnDestroy {
  private wallRequest: any;
  news: CardData[];
  feed = [];
  showModal = false;
  public myOptions: IsotopeOptions = {
    itemSelector: '.card',
    masonry: {
      columnWidth: 290,
      gutter: 20,
      fitWidth: true,
    }
  };
  constructor(
    public settingsService: SettingsService,
    private wallService: WallService,
  ) { }

  ngOnInit() {
    this.getWallNews();
  }

  ngOnDestroy() {
    this.wallRequest.unsubscribe();
  }

  private getWallNews(): void {
    this.wallRequest = this.wallService.getWallNews()
      .subscribe(
        response => {
          this.news = response;
          this.wallService.setWallData(response);
      }
    );
  }

  hasData(): boolean {
    return ( !isNullOrUndefined(this.news) );
  }
}
