import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsPollsComponent } from './forms-polls.component';

describe('FormsPollsComponent', () => {
  let component: FormsPollsComponent;
  let fixture: ComponentFixture<FormsPollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsPollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsPollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
