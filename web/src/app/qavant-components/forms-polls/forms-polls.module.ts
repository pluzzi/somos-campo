import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {FormsPollsComponent } from './forms-polls.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '../../qavant-shared/qavant-shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonsModule, TabsModule, TypeaheadModule} from 'ngx-bootstrap';
import {pgSelectModule} from '../../@pages/components/select/select.module';
import {pgTagModule} from '../../@pages/components/tag/tag.module';
import {TextMaskModule} from 'angular2-text-mask';
import {pgSwitchModule} from '../../@pages/components/switch/switch.module';
import {pgTimePickerModule} from '../../@pages/components/time-picker/timepicker.module';
import {pgTabsModule} from '../../@pages/components/tabs/tabs.module';
import {pgSelectfx} from '../../@pages/components/cs-select/select.module';
import {pgUploadModule} from '../../@pages/components/upload/upload.module';
import {pgDatePickerModule} from '../../@pages/components/datepicker/datepicker.module';
import {QuillModule} from 'ngx-quill';
import {FormsPollsDetailComponent } from './forms-polls-detail/forms-polls-detail.component';
const routes: Routes =
  [
    { path: '', component: FormsPollsComponent },
    { path: ':id', component: FormsPollsDetailComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    pgSelectModule,
    pgTagModule,
    TextMaskModule,
    pgSwitchModule,
    pgTimePickerModule,
    pgTabsModule,
    pgSelectfx,
    pgUploadModule,
    TypeaheadModule.forRoot(),
    pgDatePickerModule,
    QuillModule
  ],
  declarations: [FormsPollsComponent, FormsPollsDetailComponent]
})
export class FormsPollsModule { }
