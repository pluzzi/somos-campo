import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsPollsDetailComponent } from './forms-polls-detail.component';

describe('FormsPollsDetailComponent', () => {
  let component: FormsPollsDetailComponent;
  let fixture: ComponentFixture<FormsPollsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormsPollsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsPollsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
