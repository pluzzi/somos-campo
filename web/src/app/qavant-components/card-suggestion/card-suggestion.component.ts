import {Component, OnInit, Input} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {SettingsService} from '@services/settings/settings.service';

@Component({
  selector: 'app-card-suggestion',
  templateUrl: './card-suggestion.component.html',
  styleUrls: ['./card-suggestion.component.scss']
})
export class CardSuggestionComponent implements OnInit {
  @Input() data: any;
  @Input() section: string;
  constructor(public settingsService: SettingsService) { }

  ngOnInit() {
  }

  getId(): number {
    return this.data.Id;
  }

  getRouter(): string {
    if ( !isNullOrUndefined(this.section) ) {
      return `/${this.section}`;
    }
  }

  getImage(): string {
    return this.data.Image;
  }

  getTitle(): string {
    return this.data.Title;
  }

  getSubtitle(): string {
    return this.data.Copete;
  }
}
