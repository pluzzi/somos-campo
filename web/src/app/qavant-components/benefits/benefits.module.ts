import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QavantSharedModule} from '../../qavant-shared/qavant-shared.module';
import {BenefitsComponent} from './benefits.component';
import {BenefitsDetailComponent} from './benefits-detail/benefits-detail.component';
const routes: Routes =
  [
    { path: '', component: BenefitsComponent },
    { path: ':id', component: BenefitsDetailComponent }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    QavantSharedModule,
  ],
  declarations: [BenefitsComponent, BenefitsDetailComponent]
})
export class BenefitsModule { }
