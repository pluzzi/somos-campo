import {Component, OnInit, OnDestroy} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Training } from '@interface/training';
import { TrainingsService } from '@services/trainings/trainings.service';
import { ActivatedRoute, Router } from '@angular/router';
import { isNullOrUndefined } from '@swimlane/ngx-datatable/release/utils';
import { DomSanitizer } from '@angular/platform-browser';
import { SettingsService } from '@services/settings/settings.service';

@Component({
  selector: 'app-trainings-detail',
  templateUrl: './trainings-detail.component.html',
  styleUrls: ['./trainings-detail.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class TrainingsDetailComponent implements OnInit, OnDestroy {
  _error: boolean;
  training: Training;
  request: any;
  id: number;
  btn = false;
  constructor(
    private trainingsService: TrainingsService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    public settingsService: SettingsService,
    ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = +params.id;
      this.getTraining();
    });
  }

  ngOnDestroy() {
    this.request.unsubscribe();
  }

  getTraining(): void {
    this.request = this.trainingsService.getById(this.id)
      .subscribe(
        response => {
          this.training = response;
        },
        error => {
          this._error = true;
        }
      );
  }

  getTitle(): string {
    return this.training.Title;
  }

  hasTraining(): boolean {
    return ( !isNullOrUndefined(this.training) );
  }

  getVideoUrl(): any {
    const link = `https://www.youtube.com/embed/${this.training.VideoURL}?rel=0`;
    const trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(link);
    return trustedUrl;
  }

  getDocumentUrl(): string {
    return this.training.DocumentURL;
  }

  getDescription(): string {
    return this.training.Description;
  }

  getUrl(): string {
    return this.training.LinkURL;
  }

  requireEvaluation(): boolean {
    return ( this.training.RequiresEvaluation && this.btn );
  }

  showVideo(): boolean {
    if ( this.training.CourseType === 'VIDEO' ) {
      this.btn = true;
      return true;
    } else {
      return false;
    }
  }

  hasDocument(): boolean {
    return ( this.training.CourseType === 'DOCUMENTO' );
  }

  hasLink(): boolean {
    return ( this.training.CourseType === 'URL' );
  }

  uniqueReply(): boolean {
    return ( this.training.FormUniqueReply );
  }

  getStyle(): any {
    const styles = {
      'background-color': this.settingsService.getPrimaryColor(),
      'color': 'white'
    };
    return styles;
  }

  showBtn(): void {
    this.btn = true;
  }

  disabledBtn(): boolean {
    return this.training.UserWasAlreadyEvaluated && this.training.FormUniqueReply;
  }

  wasAlreadyEvaluated(): boolean {
    return this.training.UserWasAlreadyEvaluated;
  }

  goToForm(): void {
    this.router.navigate(['/formularios', this.training.FormId]);
  }

}
