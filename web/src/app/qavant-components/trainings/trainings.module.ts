import {NgModule } from '@angular/core';
import {RouterModule, Routes } from '@angular/router';
import {TrainingsComponent } from './trainings.component';
import {QavantSharedModule } from '@shared';
import {TrainingsDetailComponent } from './trainings-detail/trainings-detail.component';

const routes: Routes =
  [
    { path: '', component: TrainingsComponent },
    { path: ':id', component: TrainingsDetailComponent }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    QavantSharedModule,
  ],
  declarations: [
    TrainingsComponent,
    TrainingsDetailComponent,
  ]
})
export class TrainingsModule { }
