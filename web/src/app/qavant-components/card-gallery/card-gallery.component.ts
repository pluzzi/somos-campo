import {Component, OnInit, Input} from '@angular/core';
import {GalleryService} from '@services/gallery/gallery.service';
import {Gallery} from '@interface/gallery';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-card-gallery',
  templateUrl: './card-gallery.component.html',
  styleUrls: ['./card-gallery.component.scss']
})
export class CardGalleryComponent implements OnInit {
  @Input() data: Gallery;
  @Input() insideCard: boolean;
  constructor(private galleryService: GalleryService) { }

  ngOnInit() {
    if ( this.insideCard ) {
      const fixModel: any = this.data;
      this.data.ImagenPortada = fixModel.Imagen;
      this.data.Id = fixModel.Id;
    }
  }
  // isVideo(): boolean {
  //   return false;
  // }
  hasImage(): boolean {
    return (this.data.ImagenPortada !== '' && !isNullOrUndefined(this.data.ImagenPortada) );
  }

  toggleModal(event: any): void {
    if ( !isNullOrUndefined(this.data.Id) && event.srcElement.className !== 'iconReaction' ) {
      this.galleryService.setId(this.data.Id);
      this.galleryService.openModal(true);
      // this.galleryService.getGalleryById(this.data.Id)
      //   .subscribe(
      //     response => {
      //       this.galleryService.setGalleryById(response);
      //       this.galleryService.openModal(true);
      //     }
      //   );
    }
  }
}
