import {Component, OnInit, ViewChild, OnDestroy, QueryList, ViewChildren, AfterViewChecked} from '@angular/core';
import {GalleryService} from '@services/gallery/gallery.service';
import {isNullOrUndefined} from 'util';
import {SwiperDirective} from 'ngx-swiper-wrapper';
@Component({
  selector: 'app-modal-gallery',
  templateUrl: './modal-gallery.component.html',
  styleUrls: ['./modal-gallery.component.scss']
})
export class ModalGalleryComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChildren(SwiperDirective) swiperViewes: QueryList<SwiperDirective>;
  @ViewChild('slider') public _slider: any;
  @ViewChild('fadInModal') _modal: any;
  private observable: any;
  gallery: any = [];
  images: any = [];
  // configModal: any;
  index: number;
  // index2 = 0;
  rangeValue;
  configModal: any = {
    init: true,
    observer: true,
    direction: 'horizontal',
    preloadImages: true,
    updateOnImagesReady: true,
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  };
  constructor(private galleryService: GalleryService) { }

  ngOnInit() {
    this.index = 0;
    this.observable = this.galleryService.modalObserver()
    .subscribe(
      response => {
        if (response) {
          this.getGallery();
        }
      }
    );
  }

  ngAfterViewChecked() {
    // console.log(this.configModal);
    // this.configModal.pagination.update();
  }

  ngOnDestroy() {
    if ( !isNullOrUndefined(this.observable) ) {
      this.observable.unsubscribe();
    }
  }

  swiper(): void {
    console.log(this.configModal);
  }

  getGallery(): void {
    this.galleryService.getGalleryById()
      .subscribe(
        response => {
          this.images = [];
          this.gallery = response;
          this.gallery.Tipo = 'GALER';
          this.images = this.gallery.ElementosMultimedia;
          this._modal.show();
        }
      );
  }

  isVideo(data: any): boolean {
    return (data.Tipo === 'VID');
  }

  hasImages(): boolean {
    if ( !isNullOrUndefined(this.images) ) {
      return ( this.images.length > 0 );
    }
  }

  nextSlide() {
    this.swiperViewes.last.nextSlide();
  }

  prevSlide() {
    this.swiperViewes.last.prevSlide();
  }

  close(): void {
    this.galleryService.openModal(false);
    this.images = [];
    this._modal.hide();
  }
}
