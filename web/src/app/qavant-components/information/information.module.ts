import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {QavantSharedModule} from '@shared';
import {InformationComponent} from '@qavant/information/information.component';
import { InformationDetailComponent } from '@qavant/information/information-detail/information-detail.component';
const routes: Routes =
  [
    { path: '', component: InformationComponent },
    { path: ':id', component: InformationDetailComponent }
  ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
  ],
  declarations: [InformationComponent, InformationDetailComponent]
})
export class InformationModule { }
