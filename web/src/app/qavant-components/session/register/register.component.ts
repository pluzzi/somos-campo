import { Component, OnInit } from '@angular/core';
import { Event, Router } from '@angular/router';
import {NgForm} from '@angular/forms';
import { UserService } from '@services/users/user.service';
import { isNullOrUndefined } from 'util';
import { SettingsService } from '@services/settings/settings.service';
import { Title } from '@angular/platform-browser';
import { AppService } from '@services/app/app.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterPageComponent implements OnInit {
  // Sample Variables for the form
  username: string;
  showPassword: boolean;
  emailUser: string;
  passwordUser: any;
  confirmUser: any;
  activateUser: boolean;
  _loading: boolean;
  validate: any;
  private localSettingObserver: any;
  private apiSettingObserver: any;
  constructor(
    private userService: UserService,
    public settingsService: SettingsService,
    public qavantSettings: AppService,
    private router: Router,
    private titleService: Title,
    ) {
      this.activateUser = false;
      this._loading = false;
      this.validate = {
        'message' : '',
        'status': false,
        'type': '',
        'redirect': false,
        'form': true
      };
  }

  ngOnInit() {
    if ( this.settingsService.hasSettings() ) {
      this.settingsService.sessionObserver()
      .subscribe(
        response => {
          this.titleService.setTitle(this.settingsService.getTitle());
          this._loading = false;
          }
      );
    } else {
      this.getLocalSettings();
      this._loading = false;
    }

  }

  activate(event: Event) {
    if ( !isNullOrUndefined(this.username) ) {
      this.validate.status = false;
      this._loading = true;
      this.userService.activate(this.username).subscribe(
        response => {
          this.showPassword = true;
          this.emailUser = response.Email;
          this._loading = false;
        },
        error => {
          this.validate.message = error.error.title;
          this.validate.status = true;
          this.validate.type = 'info';
          //   if ( error.status === 403 ) {
            //   this.validate.message = `El usuario ya se encuentra activo.
            //   Si no recuerdas la contraseña, elige la opción "OLVIDE MI CONTRASEÑA".`;
          //   this.username = null;
          //   this.validate.redirect = true;
          //   this.validate.form = false;
          // }

          // if ( error.status === 404 ) {
          //   this.validate.message = 'Este usuario no existe, intenta nuevamente.';
          //   this.validate.type = 'danger';
          //   this.username = null;
          //   this.validate.status = true;
          //   this.validate.redirect = false;
          // }
          this._loading = false;
        }
      );
    }
  }

  isActivatePath(): boolean {
    return (this.router.url === '/session/activate');
  }

  hasSettings(): boolean {
    return this.settingsService.hasSettings();
  }

  private getLocalSettings(): void {
    this.localSettingObserver = this.settingsService.getLocalSettings()
      .subscribe( response => {
        if ( !isNullOrUndefined(response) ) {
          this.settingsService.setLocalSettings(response);
          this.getApiSettings();
        }
      });
  }


  private getApiSettings(): void {
    this.apiSettingObserver = this.settingsService.getApiSettings()
      .subscribe(
        response => {
          this.settingsService.setApiSettings(response);
          this.settingsService.setSetting(true);
          this.titleService.setTitle(this.settingsService.getTitle());
          // this.checkSession();
        }, error => {
          // this.serverError = error;
        }
      );
  }

  generate(event: Event) {
    this.validate.status = false;
    if ( this.passwordUser === this.confirmUser
      && this.passwordUser !== '' && this.confirmUser !== '') {
      this._loading = true;
      this.userService.confirmActivate(this.username, this.emailUser, this.passwordUser)
      .subscribe( response => {
        this.validate.message = 'Tu usuario se activo exitosamente';
        this.validate.type = 'success';
        this.validate.status = true;
        this.validate.redirect = true;
        this._loading = false;
        this.validate.form = false;
      });
    }else {
      this.validate.message = 'La contraseńa no coinciden, intenta nuevamente.';
      this.validate.type = 'danger';
      this.passwordUser = null;
      this.confirmUser = null;
      this.validate.status = true;
    }
  }

  setPassword(): boolean {
    return this.showPassword;
  }

}
