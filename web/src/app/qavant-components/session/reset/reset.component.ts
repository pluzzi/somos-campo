import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@services/settings/settings.service';
import { UserService } from '@services/users/user.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  username: string;
  validate: any;
  constructor(
    public settingsService: SettingsService,
    private userService: UserService,
    ) {
      this.username = null;
      this.validate = {
        message: '',
        status: false,
        type: '',
        form: true,
        redirect: false
      };
    }

  ngOnInit() {
  }

  resetPassword(): void {
    if ( !isNullOrUndefined(this.username) ) {
      this.userService.forgotPassword(this.username).subscribe(
        response => {
          if ( isNullOrUndefined(response) ) {
            this.validate.message = `Tu contraseña fue modificada,
            y se reemplazó por tu mismo nombre de usuario. Por ejemplo,
            si tu usuario era "CLopez" ahora tu contraseña es "CLopez" también.`;
            this.validate.type = 'success';
            this.validate.status = true;
            this.validate.form = false;
            this.validate.redirect = true;
          }
        },
        error => {
            this.validate.message = error.error.title;
            this.validate.type = 'danger';
            this.validate.status = true;
            this.username = null;
        }
      );
    }
  }
}
