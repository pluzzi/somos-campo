import {Component, OnInit, OnDestroy} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {UserService} from '@services/users/user.service';
import {AuthGuard} from '@services/security/auth-guard.service';
import {AppService} from '@services/app/app.service';
import {SettingsService} from '@services/settings/settings.service';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, OnDestroy {
  userName: string;
  password: any;
  remember: true;
  error: any;
  serverError: any;
  checkCredentials: boolean;
  sessionActive: boolean;
  private sessionObserver: any;
  private localSettingObserver: any;
  private apiSettingObserver: any;
  constructor(
    private router: Router,
    private userService: UserService,
    private authGuard: AuthGuard,
    private titleService: Title,
    public qavantSettings: AppService,
    public settingsService: SettingsService,
  ) { }

  ngOnInit() {
    this.error = {
      'status': false,
      'message': ''
    };
    this.sessionObserver = this.settingsService.sessionObserver()
      .subscribe(
        response => {
          this.sessionActive = response;
          // this.router.navigate([this.qavantSettings.getPreviousPage()]);
        }
      );
    if ( this.hasSettings() ) {
      this.router.navigate([this.qavantSettings.getPreviousPage()]);
    } else {
      this.getLocalSettings();
    }
  }

  ngOnDestroy() {
    if ( !isNullOrUndefined(this.sessionObserver) ) {
      this.sessionObserver.unsubscribe();
    }
    if ( !isNullOrUndefined(this.apiSettingObserver) ) {
      this.apiSettingObserver.unsubscribe();
    }
    if ( !isNullOrUndefined(this.localSettingObserver) ) {
      this.localSettingObserver.unsubscribe();
    }
  }

  private getLocalSettings(): void {
    this.localSettingObserver = this.settingsService.getLocalSettings()
      .subscribe( response => {
        if ( !isNullOrUndefined(response) ) {
          this.settingsService.setLocalSettings(response);
          this.getApiSettings();
        }
      });
  }

  private getApiSettings(): void {
    this.apiSettingObserver = this.settingsService.getApiSettings()
      .subscribe(
        response => {
          this.settingsService.setApiSettings(response);
          this.settingsService.setSetting(true);
          this.titleService.setTitle(this.settingsService.getTitle());
          this.checkSession();
        }, error => {
          // this.serverError = error;
        }
      );
  }

  getBackgroundColor(): any {
    if ( this.hasSettings() ) {
      const styles = {
        'background-color': this.settingsService.getPrimaryColor()
      };
      return styles;
    } else {
      return { };
    }
  }

  getLoginTitleStyle(): any {
    if ( this.hasSettings() ) {
      return this.settingsService.getLoginTitleStyle();
    } else {
      return {};
    }
  }

  getBtnActionStyle(): any {
    if ( this.hasSettings() ) {
      return this.settingsService.getBtnActionApiStyle();
    } else {
      return {};
    }
  }

  hasSettings(): boolean {
    return this.settingsService.hasSettings();
  }

  hasServerError(): boolean {
    return ( !isNullOrUndefined(this.serverError) );
  }

  hasSessionActive(): boolean {
    return ( this.userService.isLoggin() );
  }

  checkSession(): void {
    if ( this.userService.hasToken() ) {
      if ( isNullOrUndefined(this.userService.user) ) {
        this.userService.getUserProfile()
          .subscribe(
            response => {
              this.userService.setUser(response);
              this.authGuard.isLoggin = true;
              this.router.navigate([this.qavantSettings.getPreviousPage()]);
          });
      } else {
        this.router.navigate([this.qavantSettings.getPreviousPage()]);
      }
    }
  }

  hasLogo(): boolean {
    return (this.settingsService.getLogo() !== '' );
  }

  // isLogin(): boolean {
  //   return ( this.checkCredentials );
  // }

  login(loginForm: NgForm, event: Event): void {
    if (loginForm.valid) {
      this.checkCredentials = true;
      this.userService.authUser(loginForm.value.userName, loginForm.value.password)
      .subscribe(
        response => {
          // this.getLocalSettings();
          if ( response.shouldModifyPassword ) {
            this.userService.saveToken(response['token'], this.remember);
            this.router.navigate(['/session/password']);
          } else {
            this.userService.login(response, this.remember);
          }
        },
        error => {
          this.error.status = true;
          this.error.message = error.error.title;
          if (error.status === 401) {
            this.checkCredentials = false;
          }
        }
      );
    }
  }

  hasBackgroundImage(): boolean {
    return this.settingsService.hasBackgroundLoginImage();
  }
}
