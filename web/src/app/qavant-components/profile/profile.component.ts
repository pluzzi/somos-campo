import {ActivatedRoute} from '@angular/router';
import {isNullOrUndefined} from 'util';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {UserService} from '@services/users/user.service';

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { User } from '@interface/user';
import { SettingsService } from '@services/settings/settings.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class ProfileComponent implements OnInit, OnDestroy {
  private profileRequest: any;
  userProfile: User;
  password: string;
  newPassword: string;
  confirmPassword: string;
  validate: any;
  myProfile = false;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    public settingsService: SettingsService,
  ) {
    this.validate = {
      'message' : '',
      'status': false,
      'type': '',
      'redirect': false,
      'form': true
    };
    this.newPassword = null;
    this.confirmPassword = null;
  }

  ngOnInit() {
   this.redirect();
  }

  ngOnDestroy() {
    this.profileRequest.unsubscribe();
  }

  redirect(): void {
    this.route.params.subscribe( params => {
      if ( isNullOrUndefined(params.id) ) {
        this.myProfile = true;
        this.profileRequest = this.userService.getUserProfile().subscribe(
          response => {
            this.userProfile = response;
          }
        );
      } else {
        this.profileRequest = this.userService.getUserById(params.id).subscribe(
          response => {
            this.userProfile = response;
          }
        );
      }
    });
  }

  getTitleSection(): string {
    if ( this.myProfile ) {
      return 'MI PERFIL';
    } else {
      return 'PERFIL DE USUARIO';
    }
  }

  hasUser(): boolean {
    return ( !isNullOrUndefined(this.userProfile) );
  }

  getUserName(): string {
    return `${this.userProfile.Name} ${this.userProfile.Surname}`;
  }

  getUserFirstName(): string {
    return this.checkValue(this.userProfile.Name);
  }

  getUserLastName(): string {
    return this.checkValue(this.userProfile.Surname );
  }

  getUserEmail(): string {
    return this.checkValue(this.userProfile.Email);
  }

  getUserImage(): string {
    return this.checkValue(this.userProfile.Image);
  }

  getUserNickName(): string {
    return this.checkValue(this.userProfile.Username);
  }

  getUserPhone(): string {
    return this.checkValue(this.userProfile.Phone);
  }

  getUserMobile(): string {
    return this.checkValue(this.userProfile.Mobile);
  }

  getUserBirthday(): string {
    return this.checkValue(this.userProfile.BirthFriendlyDate);
  }

  getUserIntern(): string {
    return this.checkValue(this.userProfile.Intern);
  }

  getUserArea(): string {
    return this.checkValue(this.userProfile.Area.toLowerCase());
  }

  checkValue(property: string): string {
    if ( isNullOrUndefined(property) ) {
      return 'No Ingresado';
    } else {
      return property;
    }
  }

  btnDisble(): boolean {
    return ( isNullOrUndefined(this.newPassword) || isNullOrUndefined(this.confirmPassword));
  }

  saveNewPassword(): void {
    const data = {
      oldPassword: this.password,
      newPassword: this.newPassword
    };
    if ( !isNullOrUndefined(this.newPassword)
      && !isNullOrUndefined(this.confirmPassword) ) {
        if (this.newPassword !== this.confirmPassword) {
          this.validate.message = 'La nueva contraseña no coincide con la confirmación de la misma';
          this.validate.type = 'danger';
          this.validate.status = true;
        }else {
          this.userService.editPassword(data).subscribe(
            response => {
              this.newPassword = null;
              this.password = null;
              this.confirmPassword = null;
              this.validate.message = 'La contraseña se modificó correctamente';
              this.validate.type = 'info';
              this.validate.status = true;
            },
            error => {
              // this.validate.match = false;
              if ( error.status === 404 ) {
                this.validate.message = 'La contraseña actual ingresada es incorrecta';
                this.validate.type = 'danger';
                this.validate.status = true;
              }
            }
          );
        }
    }else {
      this.validate.message = 'Complete los datos';
      this.validate.type = 'warning';
      this.validate.status = true;
    }
  }
}
