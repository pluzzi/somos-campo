import { News } from '@interface/news';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {NewsService} from '@services/news/news.service';
import {Router} from '@angular/router';
import {IsotopeOptions} from 'ngx-isotope';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {SettingsService} from '@services/settings/settings.service';
import {CardData} from '@interface/cardData';
declare var stepsForm: any;
declare var pg: any;
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  animations: [
    trigger('enterAnimation', [
      state('loading', style({
        opacity: '0',
        transform: 'translateY(8%)'
      })),
      state('ready', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('loading => ready', animate('300ms cubic-bezier(0.1, 0.0, 0.2, 1)'))
    ])
  ],
})
export class NewsComponent implements OnInit, OnDestroy {
  private newsRequest: any;
  news: News[];
  cardData: CardData[] = [];
  public myOptions: IsotopeOptions = {
    itemSelector: '.card',
    masonry: {
      columnWidth: 290,
      gutter: 20,
      fitWidth: true
    }
  };
  public galleryOptions: IsotopeOptions = {
    itemSelector: '.gallery-item',
    masonry: {
      columnWidth: 290,
      gutter: 10,
      fitWidth: true
    }
  };

  constructor
  (
    private router: Router,
    private newsService: NewsService,
    public settingsService: SettingsService,
  ) {}

  ngOnInit() {
    this.newsRequest = this.newsService.getNews()
      .subscribe(
        response => {
          this.fixModelCard(response);
      }
    );
  }

  ngOnDestroy() {
    this.newsRequest.unsubscribe();
  }

  fixModelCard(news: News[]): void {
    for ( const _news of news) {
      const data: CardData = {
        Id: _news.Id,
        Contenido: _news.Content,
        FechaAlta: _news.Date,
        Imagen: _news.Image,
        Tipo: 'NOTIC',
        Titulo: _news.Title,
        Copete: _news.Copete,
        Destacada: _news.Featured,
        DescRegistro: 'NOTICIAS',
        ReaccionId: _news.ReaccionId,
        TotalReacciones: _news.TotalReacciones
      };
      this.cardData.push(data);
    }
  }

  hasNews(): boolean {
    return ( this.cardData.length > 0 );
  }

  goToNewDetail(id: number): void {
    this.router.navigate(['noticias', id]);
  }
}
