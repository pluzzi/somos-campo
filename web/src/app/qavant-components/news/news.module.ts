import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewsComponent} from './news.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../@pages/components/shared.module';
import {NewsDetailComponent} from './news-detail/news-detail.component';
import {QavantSharedModule} from '@shared';

const routes: Routes =
  [
    { path: '', component: NewsComponent },
    { path: ':id', component: NewsDetailComponent },
    { path: 'muro/:id', component: NewsDetailComponent },
  ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    QavantSharedModule,
  ],
  declarations: [NewsComponent, NewsDetailComponent]
})
export class NewsModule { }
