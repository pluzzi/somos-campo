import {NgModule } from '@angular/core';
import {BirthdaysComponent} from './birthdays.component';
import {RouterModule, Routes} from '@angular/router';
import {QavantSharedModule} from '../../qavant-shared/qavant-shared.module';
const routes: Routes =
  [
    { path: '', component: BirthdaysComponent }
  ];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    QavantSharedModule,
  ],
  declarations: [BirthdaysComponent]
})
export class BirthdaysModule { }
