import {Injectable} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {isNullOrUndefined} from 'util';
@Injectable()
export class StorageService {
  api_token: any;
  constructor (private storage: LocalStorageService) {}

  /* Verifica si usuario esta logeado */
  checkToken(): boolean {
    return ( !isNullOrUndefined(this.storage.get('token')) );
  }
  /* Verifica si existe token de usuario */
  getToken(): string {
    if ( isNullOrUndefined(this.storage.get('token')) ) {
      return this.api_token;
    } else {
      return this.storage.get('token');
    }
  }

  saveToken(token: string, localStorage: boolean): void {
    if ( !isNullOrUndefined(token) ) {
      if (localStorage) {
        this.storage.set('token', token);
      } else {
        this.api_token = token;
      }
    }
  }

  removeToken(): void {
    this.storage.remove('token');
  }
}
