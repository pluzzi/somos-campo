import {Headers} from '@angular/http';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from '@services/settings/settings.service';
import {News} from '@interface/news';
import {Observable} from 'rxjs/index';
import {isNullOrUndefined} from 'util';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class NewsService {
  news: News[];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  getBackupNews(): News[] {
    if ( isNullOrUndefined(this.news) ) {
      return [];
    } else {
      return this.news;
    }
  }

  getNews(): Observable<News[]> {
    return this.http.get<News[]>(
      `${this.settingsService.getApiUrl()}news/all`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getNewsById(id: number): Observable<News> {
    return this.http.get<News>(
      `${this.settingsService.getApiUrl()}news/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  private unifyObject(news: News): void {
    console.log(news);
  }

}

