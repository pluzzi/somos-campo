import {Subject} from 'rxjs/Subject';
import {isNullOrUndefined} from 'util';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {SettingsService} from '@services/settings/settings.service';
import {UserService} from '@services/users/user.service';
import {Observable} from 'rxjs/Rx';
import {User} from '@interface/user';
@Injectable()
export class AuthService {
  isLoggedIn = new Subject<boolean>();
  redirectUrl: string; // store the URL so we can redirect after logging in
  user: User;
  private myClientId: string;
  constructor(
      private http: HttpClient,
      private router: Router,
      // private localStorageService: LocalStorageService,
      private settingsService: SettingsService,
      private location: Location,
   ) {
      // this.user = new User;
      this.redirectUrl = 'usuario/muro';
      // this.checkSession();
   }


  // isUserLogin(): boolean{
  //   return this.;
  // }

  public startSession(): void {
    //  this.login().subscribe
    //    (
    //      response => {
    //        if ( this.isLoggedIn ) {
    //          // this.userService.setSession(true);
    //          this.loginFrom();
    //         //  this.appService.showLoadingGif(false);
    //        }
    //      }
    //    );
  }

  private loginFrom(): void {
    // if ( this.appService.actionPreferences() )
    // {
    //   this.userService.getHttpUserPreferences()
    //     .subscribe( response => {
    //       this.userService.setPreferences(response);
    //       if ( !this.userService.hasCreditdeals() )
    //       {
    //         this.router.navigate(['admin/preferencias']);
    //       }
    //       else
    //       {
    //         this.router.navigate
    //         (
    //           [this.appService.getBackPath()],
    //           { queryParams: this.appService.getParams()}
    //         );
    //       }
    //     });
    // }
    // else
    // {
    //   if ( this.appService.hasRedirect() )
    //   {
    //     this.router.navigate([this.appService.getRedirect()]);
    //   }
    //   else
    //   {
    //     this.location.back();
    //   }
    // }
  }

  checkSession(): boolean {
    //  if ( !this.isLoggedIn )
    //  {
    //    if ( !isNullOrUndefined(this.localStorageService.get('api_token')) )
    //    {
    //      this.isLoggedIn = true;
    //      this.userService.setSession(true);
    //      this.userService.setApiToken(this.localStorageService.get('api_token'));
    //      // this.user.setApiToken(this.localStorageService.get('api_token'));
    //      this.refreshSession();
    //      return true;
    //    }
    //    else
    //    {
    //      this.isLoggedIn = false;
    //      this.userService.setSession(false);
    //      return false;
    //    }
    //  }

    return true;
   }

   public refreshSession(): void {
      // this.http.get(`${this.settingsService.domain}login/refresh?api_token=${this.localStorageService.get('api_token')}`)
      //   .subscribe
      //   (
      //     response => {
      //       if(response['status'] === 'success'){
      //         this.setUserLogin(response);
      //         this.userService.setSession(true);
      //       }else{
      //         // console.log('peticions refresh redirecciona la home con status 200');
      //       }
      //   }, error => {
      //     console.log(error);
      //     this.appService.setServerError(true, error.status);
      //   });
   }

   public register(user): any {
      return this.http.post(`${this.settingsService.domain}register`, user);
   }

   public credencials(email: string, password: string): any {
     const credentials = {email: 'demo', password: '123'};
     // return this.http.post<User>(`${this.apiUrl}login`, this.credencials);
    return this.http.post(`${this.settingsService.domain}login`, credentials);
   }

   public credencialsSocial(): any {
      return this.http.post(`${this.settingsService.domain}login/external`, this.user);
   }

  //  public login(): Observable<boolean> {
  //     // let cookie = Cookie.set('cookieName', '5RmcD5Y54mLhjBq9KAtj', 10 /*days from now*/);
  //     // let cookielist = Cookie.getAll();
  //    // return Observable.of(true).delay(1000).do(val => this.isLoggedIn = true);
  //     return Observable.of(true).do(val => this.isLoggedIn = true);
  //  }

  setUserLogin(response): any {

  }

  logout(): void {
    // this.http.post(`${this.settingsService.domain}logout`, '')
    //   .subscribe(
    //     response => {
    //       this.isLoggedIn = false;
    //       this.localStorageService.remove('api_token');
    //       this.userService.setSession(false);
    //       this.userService.destroyPreferences();
    //       this.router.navigate(['/']);
    //       // this.location.back();
    //    },
    //    error =>
    //    {
    //      if (error.status === 200)
    //      {
    //        this.isLoggedIn = false;
    //        this.localStorageService.remove('api_token');
    //        this.userService.setSession(false);
    //        this.userService.destroyPreferences();
    //        this.router.navigate(['/']);
    //      }
    //      else
    //      {
    //        console.log(error);
    //        // this.appService.setServerError(true, error.status);
    //      }
    //    });
   }
}
