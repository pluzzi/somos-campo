import {AppService} from './../app/app.service';
import {UserService} from '@services/users/user.service';
import {Injectable, OnInit} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationEnd} from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate {
  isLoggin: boolean;
  constructor
  (
    private router: Router,
    private appService: AppService,
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    const url: string = state.url;
    this.appService.setPreviousPage(state.url);
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this.isLoggin) { return true; }
    // Store the attempted URL for redirecting
    // this.authService.redirectUrl = url;
    // Navigate to the login page with extras
    this.router.navigate(['/session/login']);
    return false;
  }
}
