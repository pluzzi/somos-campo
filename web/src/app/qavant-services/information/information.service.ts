import { Information } from '@interface/information';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SettingsService } from '@services/settings/settings.service';
import { catchError, map, tap} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class InformationService {
  private api_url: string;
  informations: Information[];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  setAllInformation(info: Information[]) {
    this.informations = info;
  }

  getInformationRequest(): Observable<Information[]> {
    return this.http.get<Information[]>(
      `${this.settingsService.getApiUrl()}paginasInfo`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getById(id: number): Observable<Information> {
    return this.http.get<Information>(
      `${this.settingsService.getApiUrl()}infoById/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getAll(): Information[] {
    return this.informations;
  }

  hasInformations(): boolean {
    return ( !isNullOrUndefined(this.informations) && this.informations.length > 0);
  }
}
