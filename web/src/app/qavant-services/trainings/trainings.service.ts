import { Injectable } from '@angular/core';
import { SettingsService } from '@services/settings/settings.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Training } from '@interface/training';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  getTrainings(): Observable<Training[]> {
    return this.http.get<Training[]>(
      `${this.settingsService.getApiUrl()}courses/getCourses`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }

  getById(id: number): Observable<Training> {
    return this.http.get<Training>(
      `${this.settingsService.getApiUrl()}courses/getCourseById/${id}`,
      {headers: this.settingsService.getAppHeaders()}
    );
  }



}
