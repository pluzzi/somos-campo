import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SettingsService } from '@services/settings/settings.service';
import { Reaction } from '@interface/reaction';
@Injectable({
  providedIn: 'root'
})
export class ReactionsService {

  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

  saveReaction(reaction: Reaction): Observable<Reaction> {
    return this.http.post<Reaction>(
      `${this.settingsService.getApiUrl()}reacciones/guardar`, reaction,
      {headers: this.settingsService.getAppHeaders()}
    );
  }
}
