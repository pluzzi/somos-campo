import {isNullOrUndefined} from 'util';
import {map} from 'rxjs/operators/map';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {User} from '@interface/user';
import {AuthGuard} from '@services/security/auth-guard.service';
import {AuthService} from '@services/security/auth.service';
import {StorageService} from '@services/storage/storage.service';
import {SettingsService} from '@services/settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private allUsers: User[];
  private groupUsers = [];
  api_token: string;
  user: User;
  constructor(
    private router: Router,
    private http: HttpClient,
    private settingsService: SettingsService,
    private authGuard: AuthGuard,
    private storageService: StorageService,
  ) { }

  activate(username: string): any {
    const params = {
      'UserName': username,
      'PasswordHash': this.settingsService.getHasActivateUser(),
    };
    return this.http.post(
      `${this.settingsService.getApiUrl()}users/toactivate`, params
    );
  }

  confirmActivate(username: string, email: string, password: string): any {
    const params = {
      'UserName': username,
      'PasswordHash': this.settingsService.getHasActivateUser(),
      'Email': email,
      'Password': password,
    };
    return this.http.post(
      `${this.settingsService.getApiUrl()}users/activate`, params
    );
  }

  forgotPassword(username: string): any {
    const data = {
      'UserName': username,
    };
    return this.http.post(
      `${this.settingsService.getApiUrl()}users/forgotpassword `, data
    );
  }

  resetPassword(password: any, confirm: any): any {
    const data = {
      Password: password, passwordConfirm: confirm
    };
    return this.http.post(
      `${this.settingsService.getApiUrl()}users/modifypassword `, data,
        { headers: this.settingsService.getAppHeaders()}
    );
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(
      `${this.settingsService.getApiUrl()}users/all`,
      { headers: this.settingsService.getAppHeaders()}
    );
    // .pipe( map (response => this.setGroupUsers(response)));
  }

  setAllUser(users: User[]): void {
    this.allUsers = users;
  }

  getAllUsers(): User[] {
    return this.allUsers;
  }

  private setGroupUsers(data: User[]): User[] {
    for ( const user of data ) {
      if ( this.groupUsers.length < 1) {
        const firtsGroup = {group: user.Name.charAt(0).toLowerCase(), users: [user]};
        this.groupUsers.push(firtsGroup);
      } else {
        const index =  this.getIndexOnGroupUsers(user);
        if ( isNullOrUndefined(index) ) {
          const addNewGroup = {group: user.Name.charAt(0).toLowerCase(), users: [user]};
          this.groupUsers.push(addNewGroup);
        } else {
          this.groupUsers[index].users.push(user);
        }
      }
    }
    return this.groupUsers;
  }

  private getIndexOnGroupUsers(user: User): any {
    for (let index = 0; index < this.groupUsers.length; index++) {
      const element = this.groupUsers[index];
      if ( user.Name.charAt(0).toLowerCase() === this.groupUsers[index].group ) {
        return index;
      }
    }
    return null;
  }

  isLoggin(): boolean {
    return this.storageService.checkToken();
  }

  hasToken(): boolean {
    return this.storageService.checkToken();
  }
  // funcion provisional -- recibo objetos diferentes
  setUser(user: User): void {
    this.user = user;
  }

  getUser(): User {
    return this.user;
  }

  getUserProfile(): Observable<User> {
    return this.http.get<User>(
      `${this.settingsService.getApiUrl()}myprofile`,
      { headers: this.settingsService.getAppHeaders()}
    );
  }

  getUserById(id: number) {
    return this.http.get<User>(
      `${this.settingsService.getApiUrl()}users/${id}`,
      { headers: this.settingsService.getAppHeaders()}
    );
  }

   editPassword(data: any) {
    return this.http.post(
      `${this.settingsService.getApiUrl()}users/edit`,
        data,
        { headers: this.settingsService.getAppHeaders()}
      );
    // let apiUrl = this.config.apiEndpoint + '/users/edit';
    // return this.httpClient.post(apiUrl, data).pipe(
    //   map(response => {
    //     console.log(response);
    //   }),
    //   catchError(error => {
    //     console.log(error);
    //     error.message = 'La contraseña actual es inválida.';
    //     return this.globalErrorHandler.handleError(error);
    //   })
    // );
  }

  authUser(user: string, password: string): any {
    const data = {UserName: user, Password: password};
    return this.http.post<User>(`${this.settingsService.getApiUrl()}login`, data);
  }

  login(data: any, remember: boolean): void {
    this.authGuard.isLoggin = true;
    this.user = data['user'];
    this.storageService.saveToken(data['token'] , remember);
    if (data.shouldModifyPassword) {
      this.router.navigate(['/session/password']);
    } else {
      this.router.navigate(['/muro']);
    }
  }

  saveToken(token: any, remember: boolean): void {
    this.storageService.saveToken(token , remember);
  }

  logout(): void {
    this.authGuard.isLoggin = false;
    this.storageService.removeToken();
    this.router.navigate(['/session/login']);
  }

  getName(): string {
    return this.user.Name;
  }

  getImage(): string {
    return this.user.Image;
  }

}
