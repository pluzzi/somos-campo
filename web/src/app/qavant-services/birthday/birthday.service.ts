import {map } from 'rxjs/operators/map';
import {Observable } from 'rxjs/index';
import {Birthday } from './../../qavant-interfaces/birthday';
import {SettingsService } from '@services/settings/settings.service';
import {HttpClient } from '@angular/common/http';
import {Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BirthdayService {
  private api_url: string;
  usersByDay = [];
  constructor(
    private http: HttpClient,
    private settingsService: SettingsService,
  ) { }

   getBirthdays(): Observable<Birthday[]> {
    return this.http.get<Birthday[]>(
      `${this.settingsService.getApiUrl()}birthdays/all`,
      {headers: this.settingsService.getAppHeaders()}
      ).pipe( map(response => this.setFormat(response) ));
   }

   private setFormat(data: Birthday[]): any {
     this.usersByDay = [];
    for ( const user of data ) {
      this.getDayAndMonth(user.BirthDate, user);
    }
    return this.usersByDay;
   }

  private getDayAndMonth(date: any, userData: Birthday): void {
    const user = new Date(date);
    const userFormat = user.getDate() + '-' + user.getMonth();
    if ( this.usersByDay.length < 1 ) {
      const day = { day: userFormat, users: [userData] };
      this.usersByDay.push(day);
    } else {
      let insert = false;
      for ( let i = 0; i < this.usersByDay.length; i++ ) {
        if ( this.usersByDay[i].day === userFormat ) {
          this.usersByDay[i].users.push(userData);
          insert = false;
          break;
        } else {
          insert = true;
        }
      }

      if ( insert ) {
        const day = { day: userFormat, users: [userData] };
        this.usersByDay.push(day);
      }
    }

    const today = new Date();
    const todayformat = today.getDate() + '-' + today.getMonth();
  }
}
