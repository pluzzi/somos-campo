import { QavantSharedModule } from './qavant-shared.module';

describe('QavantSharedModule', () => {
  let qavantSharedModule: QavantSharedModule;

  beforeEach(() => {
    qavantSharedModule = new QavantSharedModule();
  });

  it('should create an instance', () => {
    expect(qavantSharedModule).toBeTruthy();
  });
});
