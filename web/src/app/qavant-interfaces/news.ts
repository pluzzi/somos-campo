export interface News {
  Id: number;
  Content: string;
  Date: string;
  Image: string;
  Title: string;
  Featured: boolean;
  Copete: string;
  ReaccionId: number;
  TotalReacciones: number;
  Type?: string;
}
