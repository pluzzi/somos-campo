export interface Option {
  Id: number;
  Description: string;
  Enabled: boolean;
  IsRightAnswer: boolean;
  Order: number;
  AnswerSelected: boolean;
}
