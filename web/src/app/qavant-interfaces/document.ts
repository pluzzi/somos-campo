export interface Document {
  Id: number;
  Title: string;
  Description: string;
  FilePath: string;
  FileTypeLabel: string;
  Extension: string;
  DocIcon: string;
  DocIconColor: string;
  Link?: string;
}
