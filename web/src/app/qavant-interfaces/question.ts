import {Option} from './option';
export interface Question {
  Id: number;
  Answer: any;
  AnswerOptionId: number;
  Order: number;
  Question: string;
  QuestionTypeId: string;
  Required: boolean;
  Options: Option[];
  validate?: any;
  client?: any;
}
