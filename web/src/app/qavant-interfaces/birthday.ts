export interface Birthday {
  Id: number;
  Active: boolean;
  Address: string;
  Area: string;
  BirthDate: string;
  City: string;
  Email: string;
  Enable: true;
  Gender: string;
  Hobby: string;
  Image: string;
  Intern: string;
  Mobile: string;
  Name: string;
  Password: string;
  Phone: string;
  RegistrationDate: string;
  Surname: string;
  Team: string;
  UserName: string;
  UsersGroup: string;
}
