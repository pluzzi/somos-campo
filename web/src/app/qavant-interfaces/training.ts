export interface Training {
  ApprovedPercRequired: any;
  ApprovedPercUser: number;
  CourseCategory: string;
  CourseCategoryId: number;
  CourseType: string;
  CourseTypeId: number;
  Description: string;
  DocumentURL: string;
  EvaluationDate: any;
  Extension: string;
  FormId: number;
  FormUniqueReply: boolean;
  Id: number;
  LinkURL: string;
  RequiresEvaluation: boolean;
  Title: string;
  UserWasAlreadyEvaluated: boolean;
  VideoURL: string;
}
