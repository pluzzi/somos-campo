export interface User {
  Id: number;
  Active: boolean;
  Address: string;
  Area: any;
  BirthDate: string;
  City: string;
  Email: string;
  Enable: boolean;
  Gender: string;
  Hobby: string;
  Image: string;
  Intern: string;
  Mobile: string;
  Name: string;
  LastName: string;
  Password: string;
  Phone: string;
  RegistrationDate: string;
  Surname: string;
  Team: string;
  Username: string;
  UsersGroup: string;
  BirthdayType: string;
  BirthFriendlyDate?: string;
}
