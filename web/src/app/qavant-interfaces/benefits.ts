export interface Benefits {
  Id: number;
  Content: string;
  Title: string;
  Image: string;
  Subtitle: string;
  To: string;
  Date: string;
  Liked: boolean;
  TotalReacciones: number;
  ReaccionId: number;
}
