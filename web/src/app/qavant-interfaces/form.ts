import {Question} from './question';
export interface Form {
  Id: number;
  AlreadyAnswered: boolean;
  Title: string;
  Code: any;
  Description: string;
  FormTypeId: number;
  MailIntro: string;
  MailSubject: string;
  SendMails: boolean;
  UniqueReply: boolean;
  Questions: Array<Question>;
}
