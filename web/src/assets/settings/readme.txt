==================================================================================================
Ayuda para completar el archivo de configuracion config.json
==================================================================================================
LoginSubtitle: "Subtitulo Qavant"
LoginTitle: "Bienvenidos al sitio corporativo de la Empresa Qavant"
RecordsPerPage: "10"
SiteTitle: "Qavant"
WallBgImage: "assets/settings/images/cover.jpg"
WallSubtitle: "intranet Corporativa"
WallTitle: "Bienvenido a Qavant"
---------------------------------------------------------------------------
api
---------------------------------------------------------------------------
end_point: Direccion de la api. EJ: "https://qavant.goiar.com/excle/api/"
title" : Titulo que aparecera en la solapa del navegador. EJ: "Ex-ClÈ"


---------------------------------------------------------------------------
design
---------------------------------------------------------------------------
colors
      primary : Color primario utilizado en titulos.EJ: "#034a84"
      secundary" : Color secuandario utilizado en textos. EJ: #8d8e87"

login_description
  estilos para el texto de bienvenida o descripcion de la empresa en la seccion de
  iniciar sesion.

---------------------------------------------------------------------------
titleSection
---------------------------------------------------------------------------
color: Color de los textos de los titulos de cada pagina EJ "#034a84"
border-bottom : linea separadora de los titulos de las paginas. EJ: "2px solid #034a84"
font-size: TamaÒo de los textos de los titulos de cada pagina. EJ:"24px"
setPrimaryColorApi: Este campo prioriza el parametro color primario de la api. 
  Si el valor es true, 'color' y 'border-bottom' no sera tomado.

---------------------------------------
wall
----------------------------------------
title: Titulo que esta ubicado en la seccion muro Ej: "Bienvenido a Qavant"
subtitle : Copete o descripcion Ej: "intranet Corporativa"


---------------------------------------------------------------------------
Login
---------------------------------------------------------------------------
description: Descripcion de la empresa o texto de bienvenida
EJ: "Bienvenidos al sitio corporativo de la Empresa"

images
---------------------------------------------------------------------------
logo :ruta del logo de la empresa.
	Medida: 470x140px (ancho x alto)
	Carpeta:"assets/settings/images/logo.png"
login_background_image" : imagen del login.
	Medida: 1920x1080px
	Carpeta:"assets/settings/images/login_background.jpg"
