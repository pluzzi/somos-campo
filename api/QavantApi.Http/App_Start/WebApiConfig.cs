﻿using QavantApi.Http.Filters;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;

namespace QavantApi.Http
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IExceptionLogger), new QavantExceptionLogger());
            config.Filters.Add(new CustomExceptionFilter());
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute("LikeRoute", "benefits/{id}/like", new { controller = "Benefits", action = "GetLike" });
            config.Routes.IgnoreRoute("settoken", "SetToken");
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
