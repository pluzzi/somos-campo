﻿using Newtonsoft.Json;
using Qavant.Infrastructure.Authentication;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Filters;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace QavantApi.Http.Filters
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var ex = context.Exception;
            if (ex is ApplicationException || ex is AuthenticationProviderException)
            {
                var details = new
                {
                    title = ex.Message,
                    status = HttpStatusCode.BadRequest
                };
                string json = JsonConvert.SerializeObject(details);
                var httpResponseMsg = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    RequestMessage = context.Request,
                    Content = new StringContent(json, Encoding.UTF8, "application/json")
                };
                context.Response = httpResponseMsg;
            }
            else
            {
                var details = new
                {
                    title = "Error interno en el servidor, por favor intente más tarde.",
                    status = HttpStatusCode.InternalServerError
                };
                string json = JsonConvert.SerializeObject(details);
                var httpResponseMsg = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    RequestMessage = context.Request,
                    Content = new StringContent(json, Encoding.UTF8, "application/json")
                };
                context.Response = httpResponseMsg;
            }
        }
    }
}