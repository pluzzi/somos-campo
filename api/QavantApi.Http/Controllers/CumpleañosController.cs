﻿using Qavant.Application.Services.Cumpleaños;
using Qavant.Application.Services.Shared;
using QavantApi.Http.Infrastructure.Authorization;
using System.Collections.Generic;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization] 
    public class CumpleañosController : QavantApiBaseController
    {
        private readonly ICumpleañosService _cumpleañosService;

        public CumpleañosController(ICumpleañosService cumpleañosService)
        {
            _cumpleañosService = cumpleañosService;
        }

        [Route("birthdays/all")]
        public List<CumpleañosOutput> GetAllBirthdays()
        {
            var output = _cumpleañosService.GetCumpleaños();
            return output.CumpleañosList;
        }

        [Route("birthdays/page/{page}")]
        public List<CumpleañosOutput> GetBirthdays(int page)
        {
            var output = _cumpleañosService.GetCumpleaños(page);
            return output.CumpleañosList;
        }
    }
}