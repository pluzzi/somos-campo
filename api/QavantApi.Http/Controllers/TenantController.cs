﻿using Qavant.Application;
using Qavant.Application.Services.Tenants;
using QavantApi.Http.Helpers;
using QavantApi.Http.Infrastructure.Authorization;
using System;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    
    public class TenantController : QavantApiBaseController
    {
        private readonly ITenantService _tenantService;

        public TenantController(ITenantService tenantService)
        {
            _tenantService = tenantService;
        }

        [Route("tenant/settings")]
        [QavantApiAuthorization]
        public IHttpActionResult GetMobileSettings()
        {
            var encryptedToken = Request.Headers.GetValues("x-token").FirstOrDefault();
            if (string.IsNullOrEmpty(encryptedToken))
                throw new ArgumentException("Token inválido.");

            var decodedToken = TokenHelper.DecodeToken(encryptedToken);
            var tokenInfo = TokenHelper.GetTokenInfo(decodedToken);
            var output = _tenantService.GetClientSettings(tokenInfo.Canal, tokenInfo.TenantId);
            return Ok(output);
        }

        [HttpPost]
        [Route("tenant/websettings")]
        public IHttpActionResult GetWebSettings()
        {
            var tenantService = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
            var tenantHolder = RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;

            // validamos existencia de tenant
            string tenantKey = RequestHeadersHelper.GetTenantKey(HttpContext.Current);
            var tenant = tenantService.ValidateAndReturn(Canal.Web, tenantKey);
            tenantHolder.Tenant = tenant;
            var output = _tenantService.GetClientSettings(Canal.Web, tenant.Id);
            return Ok(output);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}