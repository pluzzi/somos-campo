﻿using Qavant.Application.Services.InfoPaginas;
using QavantApi.Http.Infrastructure.Authorization;
using System.Collections.Generic;
using System.Web.Http;
namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class InfoUtilController : QavantApiBaseController
    {
        private readonly IInfoUtilService _infoUtilService;

        public InfoUtilController(IInfoUtilService infoUtilService)
        {
            _infoUtilService = infoUtilService;
        }

        [Route("paginasInfo")]
        public List<InfoUtilOutput> GetInfoPaginas()
        {
            var output = _infoUtilService.GetListInfoUtil();
            return output.InfoPaginas;
        }

        //GET: api/infoPaginasByCategoriaId/5    infoPaginas según Categoría
        [Route("infoPaginas/categoriaId/{id}")]
        public List<InfoUtilOutput> GetInfoPaginasByCatId(int id)
        {
            var output = _infoUtilService.GetListInfoUtilByCategoryId(id);
            return output.InfoPaginas;
        }

        [Route("infoById/{id}")]
        public InfoUtilOutput GetInfoUtilById(int id)
        {
            var output = _infoUtilService.GetInfoUtilById(id);
            return output;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}