﻿
using Microsoft.ApplicationInsights;
using Qavant.Application.Services.Capacitaciones;
using Qavant.Infrastructure.ApplicationLogger;
using Qavant.Infrastructure.Persistence.Repositories;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using QavantApi.Http.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class CapacitacionesController : QavantApiBaseController
    {
        private readonly ICapacitacionService _capacitacionService;

        public CapacitacionesController(ICapacitacionService capacitacionService)
        {
            _capacitacionService = capacitacionService;
        }

        [Route("courses/getCourses")]
        public List<CapacitacionOutput> GetCourses()
        {
            var output = _capacitacionService.GetCapacitaciones();
            return output.Capacitaciones;
        }

        [Route("courses/getCourseById/{Id}")]
        public CapacitacionOutput GetCourseById(int Id)
        {
            var output = _capacitacionService.GetCapacitacionById(Id);
            return output;
        }
    }
}