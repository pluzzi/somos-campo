﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using QavantApi.Http.Models;
using QavantApi.Http.Infrastructure.Authorization;
using System.Web;
using System.IO;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class BillsController : QavantApiBaseController
    {
        [Route("bills")]
        [HttpPost]
        public IHttpActionResult CreateBill()
        {
            try
            {
                //string userId = HttpContext.Current.Request.Params.Get("userId");
                //string token = HttpContext.Current.Request.Params.Get("token");
                //string denunciationId = HttpContext.Current.Request.Params.Get("denunciationId");

                QavantUser currentUser = CurrentUser();

                HttpPostedFile httpPostedImageFile = HttpContext.Current.Request.Files["UploadedImage"];

                string path = HttpContext.Current.Server.MapPath("~/Uploads/Bills");

                if (string.IsNullOrEmpty(httpPostedImageFile.FileName))
                {
                    // "This request is not properly formatted";
                    throw new Exception();
                }
                //string fileName = httpPostedImageFile.FileName;
                string fileName = DateTime.Now.ToString("yyyyMMddhhmmss") + ".jpg";
                Bill bill = new Bill();

                bill.CreationUserId = currentUser.Id;
                bill.BillDate = HttpContext.Current.Request.Params.Get("FechaFactura");
                bill.BillNumber = HttpContext.Current.Request.Params.Get("NroFactura");
                bill.Comments = HttpContext.Current.Request.Params.Get("Comentarios");
                bill.CreationDate = DateTime.Now;
                bill.Path = path + fileName;
                

                //Grabar la Imagen
                httpPostedImageFile.SaveAs(Path.Combine(path, fileName));

                //Crea el Video
                //CreateAuddiar(postCreated);
                db.Bills.Add(bill);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return Ok();
        }
    }
}