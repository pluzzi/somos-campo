﻿using Qavant.Application.Services.Categorias;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace QavantApi.Http.Controllers
{
    [QavantApiAuthorization]
    public class CategoriasController : QavantApiBaseController
    {
        private readonly ICategoriaService _categoriaService;

        public CategoriasController(ICategoriaService categoriaService)
        {
            _categoriaService = categoriaService;
        }

        [Route("categorias/moduloId/{moduloId}")]
        [HttpGet]
        [ObsoleteAttribute("This method has been deprecated.", true)]
        public List<CategoriaOutput> ObtenerCategoriasPorModuloId(int moduloId)
        {
            var output = _categoriaService.ObtenerCategoriasPorModuloId(moduloId);
            return output.Categorias;
        }

        [Route("categorias/modulo/{nemonico}")]
        [HttpGet]
        public List<CategoriaOutput> ObtenerCategoriasPorModulo(string nemonico)
        {
            var output = _categoriaService.ObtenerCategoriasPorModulo(nemonico);
            return output.Categorias;
        }
    }
}