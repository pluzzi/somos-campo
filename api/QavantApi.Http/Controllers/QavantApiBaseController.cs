﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using QavantApi.Http.Infrastructure.Authorization;
using QavantApi.Http.Models;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using Qavant.Application.Services;
using Qavant.Application;

namespace QavantApi.Http.Controllers
{
    public class QavantApiBaseController : ApiController
    {
        private TokenInfo _tokenInfo;
        protected QavantDbContext db = new QavantDbContext();


        protected int UsuarioId
        {
            get
            {
                return _tokenInfo.UserId; ;
            }
        }

        protected QavantUser CurrentUser()
        {
            return db.Users.Include(u => u.Area).First(x => x.UserName == _tokenInfo.UserName && x.EmpresaId == _tokenInfo.TenantId);
        }

        public void SetToken(TokenInfo tokenInfo)
        {
            _tokenInfo = tokenInfo;
        }
    }
}