﻿using JWT;
using Newtonsoft.Json;
using Qavant.Application;
using Qavant.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace QavantApi.Http.Infrastructure.Authorization
{
    public class TokenHelper
    {
        public static TokenInfo GetTokenInfo(string decodedToken)
        {
            dynamic tokenData = JsonConvert.DeserializeObject<dynamic>(decodedToken);
            var info = new TokenInfo
            {
                TenantId = tokenData.EmpresaId,
                UserId = tokenData.UserId,
                UserName = tokenData.UserName,
                Canal = tokenData.Canal
            };
            return info;
        }

        public static string CreateToken(int empresaId, int userId, string userName, Canal canal)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtKey");
            var payload = new Dictionary<string, object>
            {
                {"EmpresaId", empresaId },
                {"UserId", userId },
                {"UserName", userName },
                {"Canal", canal }
            };
            var token = JsonWebToken.Encode(payload, secret, JwtHashAlgorithm.HS256);
            var cryptography = new QavantTripleDES();
            return cryptography.Encrypt(token, true);
        }

        public static string DecodeToken(string encryptedToken)
        {
            var secret = ConfigurationManager.AppSettings.Get("JwtKey");
            var cryptography = new QavantTripleDES();
            string jwt = cryptography.Decrypt(encryptedToken, true);
            var decodedToken = JsonWebToken.Decode(jwt, secret);
            if (string.IsNullOrEmpty(decodedToken))
                throw new Exception("Problemas en la decodificación del token.");

            return decodedToken;
        }
    }
}