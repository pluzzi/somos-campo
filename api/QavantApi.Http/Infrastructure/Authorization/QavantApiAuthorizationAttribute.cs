﻿using Qavant.Application;
using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure.LoggerActivity;
using Qavant.Application.Services.Tenants;
using Qavant.Infrastructure.Logging;
using QavantApi.Http.Controllers;
using QavantApi.Http.Models;
using System;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;


namespace QavantApi.Http.Infrastructure.Authorization
{
    public class QavantApiAuthorizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var logger = new Log4NetAdapter();
            var context = new QavantDbContext();

            logger.Debug("Recuperamos informacion del token");
            var encryptedToken = actionContext.Request.Headers.GetValues("x-token").FirstOrDefault();
            if (string.IsNullOrEmpty(encryptedToken))
                throw new ArgumentException("Token inválido.");
            var decodedToken = TokenHelper.DecodeToken(encryptedToken);
            var helper = new QavantAuthorizationHelper(context);
            var tokenInfo = helper.GetInfoFrom(decodedToken);

            logger.Debug("Recuperar configuracion del Tenant (disponible en toda la aplicación)");
            var tenantService = actionContext.RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantService)) as ITenantService;
            var tenantHolder = actionContext.RequestContext.Configuration.DependencyResolver.GetService(typeof(ITenantHolder)) as ITenantHolder;
            tenantHolder.Tenant = tenantService.GetCurrentTenant(tokenInfo.Canal, tokenInfo.TenantId);
            tenantHolder.Canal = tokenInfo.Canal;

            
            var usuario = context.Users.FirstOrDefault(x => x.UserName == tokenInfo.UserName && x.EmpresaId == tokenInfo.TenantId && x.Enable == true);

            logger.Debug("Validamos existencia de usuario");
            if (usuario == null)
                throw new UsuarioNoRegistradoException("No se encontró el usuario. Comunicarse con el área correspondiente.");
            tenantHolder.UsuarioId = usuario.Id;

            logger.Debug("validamos usuario activo");
            if (!usuario.Active)
                throw new UsuarioInactivoException("Su cuenta de usuario ha sido desactivada.");

            logger.Debug("Recuperar parámetros básicos para el log de actividad");
            var logHolder = actionContext.RequestContext.Configuration.DependencyResolver.GetService(typeof(ILogHolder)) as ILogHolder;
            logHolder.TenantId = tokenInfo.TenantId;
            logHolder.Canal = tokenInfo.Canal;
            logHolder.UsuarioId = tokenInfo.UserId;

            (actionContext.ControllerContext.Controller as QavantApiBaseController).SetToken(tokenInfo);
        }
    }
}