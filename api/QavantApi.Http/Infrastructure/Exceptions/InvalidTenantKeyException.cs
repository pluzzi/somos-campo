﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.Exceptions
{
    public sealed class InvalidTenantKeyException : ApplicationException
    {
        public InvalidTenantKeyException(string message) : base(message)
        { }
    }
}