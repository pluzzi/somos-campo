﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class FormPost
    {
        public int FormId { get; set; }
        public ICollection<FormAnswerPost> Answers { get; set; }
    }

    public class FormAnswerPost
    {
        public int QuestionId { get; set; }
        public int? QuestionOptionId { get; set; }
        public string Answer { get; set; }
    }
}