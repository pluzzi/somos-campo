﻿namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class DevicePost
    {
        public int UserId { get; set; }
        public string Uuid { get; set; }
        public string TokenNotification { get; set; }
    }
}