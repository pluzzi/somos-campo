﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class CompliancePost
    {
        public string ToReport { get; set; }
        public string RecipientType { get; set; }
        public string RecipientName { get; set; }
        public string Event { get; set; }
        public string Date { get; set; }
        public string Present { get; set; }
        public string Comment { get; set; }
    }
}