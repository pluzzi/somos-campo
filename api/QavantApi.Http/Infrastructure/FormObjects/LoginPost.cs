﻿using Qavant.Application;
using QavantApi.Http.Helpers;
using System.Web;

namespace QavantApi.Http.Infrastructure.FormObjects
{
    public class LoginRequest
    {
        private Canal _canal;
        private string _tenantKey;

        public LoginRequest()
        {
            Canal = Canal.None;
            TenantKey = string.Empty;
        }
        public Canal Canal
        {
            get
            {
                return _canal;
            }
            set
            {
                _canal = RequestHeadersHelper.GetCanal(HttpContext.Current);
            }
        }
        public string TenantKey
        {
            get
            {
                return _tenantKey;
            }
            set
            {
                _tenantKey = RequestHeadersHelper.GetTenantKey(HttpContext.Current);
            }
        }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}