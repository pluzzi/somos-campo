﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QavantApi.Http.Models;

namespace QavantApi.Http.Infrastructure.Responses
{
    public class ImageGalleryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CantImages { get; set; }
    }


}