﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QavantApi.Http.Models;

namespace QavantApi.Http.Infrastructure.Responses
{
    public class CourseResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CourseCategoryId { get; set; }
        public string CourseCategory { get; set; }
        public int CourseTypeId { get; set; }
        public string CourseType { get; set; }
        public string DocumentURL { get; set; }
        public string Extension { get; set; }
        public string VideoURL { get; set; }
        public string LinkURL { get; set; }
        public bool RequiresEvaluation { get; set; }
        public bool? UserWasAlreadyEvaluated { get; set; }
        public int? ApprovedPercRequired { get; set; }
        public int? ApprovedPercUser { get; set; }
        public int? FormId { get; set; }
        public bool? FormUniqueReply { get; set; }
        public DateTime? EvaluationDate { get; set; }
    }

    

}