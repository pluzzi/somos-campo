﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QavantApi.Http.Models
{
    public class Categoria
    {
        public int Id { get; set; }

        public int EmpresaId { get; set; }

        public int ModuloId { get; set; }

        public string Nombre { get; set; }

        public int? PadreCategoriaId { get; set; }

        public string Imagen { get; set; }

        public bool Habilitada { get; set; }

        //public ICollection<Course> Courses { get; set; }

    }
}