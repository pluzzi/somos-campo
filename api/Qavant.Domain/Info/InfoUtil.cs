﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Reacciones;
using System;

namespace Qavant.Domain.Info
{
    public class InfoUtil : IReaccionable
    {
        public int Id { get; private set; }

        public string Titulo { get; private set; }

        public string Copete { get; private set; }

        public string Html { get; private set; }        

        public bool Destacada { get; private set; }

        public string Imagen { get; private set; }

        public Categoria Categoria { get; private set; }

        public InfoReaccion InfoReaccion { get; private set; }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
    }
}