﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Multimedia;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Galerias
{
    public class Galeria : IReaccionable
    {
        public Galeria()
        {
            ElementosMultimedia = new List<ElementoMultimedia>();
        }

        public int Id { get; private set; }
        public string Titulo { get; private set; }
        public DateTime FechaAlta { get; private set; }
        public DateTime FechaDesde { get; private set; }
        public string Copete { get; private set; }
        public string ImagenPortada { get; private set; }
        public bool Destacada { get; private set; }
        public Categoria Categoria { get; private set; }
        public ICollection<ElementoMultimedia> ElementosMultimedia { get; private set; }
        public InfoReaccion InfoReaccion { get; private set; }

        public Galeria SetElementosMultimedia(ICollection<ElementoMultimedia> elementos)
        {
            foreach (var elemento in elementos)
            {
                ElementosMultimedia.Add(elemento);
            }
            return this;
        }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
    }
}
