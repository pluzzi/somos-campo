﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Beneficios
{
    public class BeneficioLike
    {
        public int Id { get; set; }

        public int BeneficioId { get; set; }

        public int UsuarioId { get; set; }
    }
}