using System;

namespace Qavant.Domain.Formularios
{
    public class Opcion
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }
        public Int16 Orden { get; private set; }
        public bool Habilitado { get; private set; }
    }
}