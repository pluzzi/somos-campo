﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Formularios
{
    public class Respuesta
    {
        public Respuesta()
        {
            RespuestaSeleccion = new List<Opcion>();
        }

        public Pregunta Pregunta { get; private set; }
        public List<Opcion> RespuestaSeleccion { get; set; }
        public string RespuestaSimple { get; set; }

        public Respuesta SetValorRespuesta(string valor)
        {
            RespuestaSimple = valor;
            return this;
        }
        public Respuesta SetPregunta(Pregunta pregunta)
        {
            Pregunta = pregunta;
            return this;
        }
        public void AgregarOpcionSeleccionada(Opcion opcion)
        {
            RespuestaSeleccion.Add(opcion);
        }
    }
}