﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Formularios
{
    public enum TipoFormulario : byte
    {
        Encuesta = 1,
        Generico = 2,
        Evaluacion = 3,
        Tramite = 4
    }
}