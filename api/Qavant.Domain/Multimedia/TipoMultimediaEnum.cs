﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Multimedia
{
    public enum TipoMultimediaEnum
    {
        IMG, //Imagen
        VID, //Video
        AUD, //Audio 
        DOC //Documento
    }
}