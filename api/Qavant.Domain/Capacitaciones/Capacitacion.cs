﻿using Qavant.Domain.Categorias;
using Qavant.Domain.Formularios;
using Qavant.Domain.Reacciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Capacitaciones
{
    public class Capacitacion : IReaccionable
    {
        public int Id { get; private set; }
        public string Titulo { get; private set; }
        public string Descripcion { get; private set; }        
        public string DocumentoURL { get; private set; }
        public string VideoURL { get; private set; }
        public string LinkURL { get; private set; }
        public DateTime FechaDesde { get; private set; }
        public DateTime FechaHasta { get; private set; }
        public bool RequiereEvaluacion { get; private set; }
        public bool Habilitada { get; private set; }
        public DateTime FechaAlta { get; private set; }
        public int PorcentajeAprobado { get; private set; }
        public Categoria Categoria { get; private set; }
        public Formulario Formulario { get; private set; }
        public CapacitacionTipo CapacitacionTipo { get; private set; }
        public int? FormularioId { get; private set; }
        public InfoReaccion InfoReaccion { get; private set; }

        public void SetInfoReaccion(InfoReaccion infoReaccion)
        {
            InfoReaccion = infoReaccion;
        }
        
    }
}