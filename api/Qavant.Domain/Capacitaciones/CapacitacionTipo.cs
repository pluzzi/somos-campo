﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Capacitaciones
{
    public class CapacitacionTipo
    {
        public int Id { get; private set; }
        public string Nombre { get; private set; }
    }
}
