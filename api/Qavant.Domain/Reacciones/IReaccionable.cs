﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Reacciones
{
    public interface IReaccionable
    {
        void SetInfoReaccion(InfoReaccion infoReaccion);

    }
}
