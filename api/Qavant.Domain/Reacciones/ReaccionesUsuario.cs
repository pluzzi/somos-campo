﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Reacciones
{
    public class ReaccionesUsuario
    {
        public int Id { get; private set; }
        public int TenantId { get; private set; }
        public int UsuarioId { get; private set; }
        public int ModuloId { get; private set; }
        public int EntidadId { get; private set; }
        public int ReaccionId { get; private set; }
        public DateTime FechaAlta { get; private set; }
    }
}