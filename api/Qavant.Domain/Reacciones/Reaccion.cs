﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Reacciones
{
    public class Reaccion
    {
        public int Id { get; private set; }
        public string Nombre { get; private set; }
        public string Icono { get; private set; }
        public int Valor { get; private set; }
        public int  Orden { get; private set; }
    }
}