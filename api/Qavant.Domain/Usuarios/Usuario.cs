﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Usuarios
{
    public class Usuario
    {
        public int Id { get; private set; }
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public string Imagen { get; private set; }
        public string Telefono { get; private set; }
        public string Celular { get; private set; }
        public string Interno { get; private set; }
        public string Direccion { get; private set; }
        public DateTime FechaNacimiento { get; private set; }
        public bool Activo { get; private set; }
        public bool Habilititado { get; private set; }
        public string Genero { get; private set; }
        public Area Area { get; private set; }
        public string Legajo { get; private set; }
        public string Dni { get; private set; }
        public string Iniciales { get; set; }

        public Usuario SetPassword(string password)
        {
            Password = password;
            return this;
        }

        public Usuario SetActivo(bool valor)
        {
            Activo = valor;
            return this;
        }

    }
}