﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Usuarios
{
    public class Grupo
    {
        public int Id { get; private set; }
        public string Nombre { get; set; }
    }
}
