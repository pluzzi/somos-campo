﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Domain.Tenants
{
    public class NullTenant : Tenant
    {
        public static NullTenant Instance = new NullTenant();
    }
}
