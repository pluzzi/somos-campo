﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Authentication
{
    public class AuthenticationProviderException : Exception
    {
        internal AuthenticationProviderException(string message) : base(message)
        {
        }
    }
}
