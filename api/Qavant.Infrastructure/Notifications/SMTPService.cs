﻿using Qavant.Application.Infrastructure;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Notifications
{
    public class SMTPService : IEmailService
    {
        private SmtpClient _smtp;
        private string _SMTPUsername;
        private string _SMTPPassword;
        private string _SMTPServer;
        private int _SMTPPort;
        private bool _SMTPEnableSsl;
        private string _displayName;
        private string _emailFrom;

        public void Configure(string SMTPUsername, string SMTPPassword, string SMTPServer, int SMTPPort, bool SMTPEnableSsl, string emailFrom, string displayName)
        {
            _SMTPUsername = SMTPUsername;
            _SMTPPassword = SMTPPassword;
            _SMTPServer = SMTPServer;
            _SMTPPort = SMTPPort;
            _SMTPEnableSsl = SMTPEnableSsl;
            _emailFrom = emailFrom;
            _displayName = displayName;

            _smtp = new SmtpClient
            {
                Host = _SMTPServer,
                Port = _SMTPPort,
                EnableSsl = _SMTPEnableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_SMTPUsername, _SMTPPassword)
            };
        }

        public void SendMail(string email, string subject, string body)
        {
            MailMessage message = new MailMessage();
            if (IsValidEmail(email))
            {
                message.To.Add(email);
            }
            Send(message, subject, body);
        }

        public void SendMail(List<string> emails, string subject, string body)
        {
            MailMessage message = new MailMessage();
            foreach (string email in emails)
            {
                if (IsValidEmail(email))
                {
                    message.To.Add(email);
                }
            }
            Send(message, subject, body);
        }

        private void Send(MailMessage mailMessage, string subject, string body)
        {
            try
            {
                var from = new MailAddress(_emailFrom, _displayName);
                mailMessage.From = from;
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                Task.Factory.StartNew(() => _smtp.Send(mailMessage));
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}