﻿using Qavant.Infrastructure.Authentication.PluginInterface;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Authentication.ActiveDirectory
{
    public class ActiveDirectoryProvider : IAuthenticationProvider
    {
        private Dictionary<string, string> _parameters;

        public void SetParameters(Dictionary<string, string> parameters)
        {
            _parameters = parameters;
        }

        public bool Validate(int tenantId, string username, string password)
        {
            string domain = _parameters["ActiveDirectoryDomain"];
            string path = _parameters["ActiveDirectoryPath"];

            string domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(path, domainAndUsername, password);
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                SearchResult result = search.FindOne();
                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}