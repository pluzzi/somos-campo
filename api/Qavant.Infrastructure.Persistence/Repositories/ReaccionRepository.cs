﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Reacciones;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class ReaccionRepository : IReaccionRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public ReaccionRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Reaccion GetReaccionById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var sentimiento = db.Reacciones.Where(s => s.Id == id).Single();
                    return sentimiento;
                }
                catch (InvalidOperationException)
                {
                    return new NullReaccion();
                }
            }
        }

        public void Guardar(int sentimientoId, int moduloId, int entidadId)
        {
            string _connectionString = ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString;
            string query = "INSERT INTO dbo.ReaccionesUsers (EmpresaId, EntidadId, UserId, ModuloId, ReaccionId, FechaAlta) " +
                            "VALUES (@EmpresaId, @EntidadId, @UserId, @ModuloId, @ReaccionId, @FechaAlta)";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    try
                    {
                        cmd.Parameters.Add("@EmpresaId", SqlDbType.Int).Value = _tenantHolder.Tenant.Id;
                        cmd.Parameters.Add("@EntidadId", SqlDbType.Int).Value = entidadId;
                        cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = _tenantHolder.UsuarioId;
                        cmd.Parameters.Add("@ModuloId", SqlDbType.Int).Value = moduloId;
                        cmd.Parameters.Add("@ReaccionId", SqlDbType.Int).Value = sentimientoId;
                        cmd.Parameters.Add("@FechaAlta", SqlDbType.DateTime).Value = DateTime.Now;

                        // open connection, execute INSERT, close connection
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}