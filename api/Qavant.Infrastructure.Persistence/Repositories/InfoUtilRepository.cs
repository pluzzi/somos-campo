﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Info;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class InfoUtilRepository: IInfoUtilRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public InfoUtilRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<InfoUtil> GetListInfoUtil()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.ListaInfoUtil
                             .Include(c => c.Categoria)
                             .Where(x => x.Habilitada == true)
                             .OrderByDescending(x => new { x.Orden })
                             .Where(CommonFilters.CustomFilters<Entities.InfoUtilEntity>(_tenantHolder.UsuarioId, ModuloEnum.InfoUtil, db))
                             .ToList()
                            .Select(e => new Entities.InfoUtilEntity
                            {
                                Id = e.Id,
                                Titulo = e.Titulo,
                                Copete = e.Copete,
                                Html = e.Html,
                                Orden = e.Orden,
                                Destacada = e.Destacada,
                                Imagen = e.Imagen,
                                Categoria = e.Categoria,
                                Habilitada = e.Habilitada,
                                ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.InfoUtil, e.Id, db),
                                TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.InfoUtil, e.Id, db)
                            })
                            .ToList();

                    List<InfoUtil> infoResults = new List<InfoUtil>();
                    foreach (var entity in entities)
                    {
                        InfoUtil info = Mapper.Map<InfoUtil>(entity);
                        info.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        infoResults.Add(info);
                    }
                    return infoResults;
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }

        public ICollection<InfoUtil> GetListInfoUtilByCategoriaId(int categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.ListaInfoUtil
                                    .Include(c => c.Categoria)
                                    .Where(x => x.CategoriaId == categoriaId)
                                    .Where(x => x.Habilitada == true)
                                    .OrderByDescending(x => new { x.Orden })
                                    .Where(CommonFilters.CustomFilters<Entities.InfoUtilEntity>(_tenantHolder.UsuarioId, ModuloEnum.InfoUtil, db))
                                    .ToList()
                                    .Select(e => new Entities.InfoUtilEntity
                                    {
                                        Id = e.Id,
                                        Titulo = e.Titulo,
                                        Copete = e.Copete,
                                        Html = e.Html,
                                        Orden = e.Orden,
                                        Destacada = e.Destacada,
                                        Imagen = e.Imagen,
                                        Habilitada = e.Habilitada,
                                        Categoria = e.Categoria,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.InfoUtil, e.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.InfoUtil, e.Id, db)
                                    })
                                    .ToList();

                    List<InfoUtil> infoResults = new List<InfoUtil>();
                    foreach (var entity in entities)
                    {
                        InfoUtil info = Mapper.Map<InfoUtil>(entity);
                        info.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        infoResults.Add(info);
                    }
                    return infoResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public InfoUtil GetInfoUtilById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.ListaInfoUtil
                                 .Include(c => c.Categoria)
                                 .Where(i => i.Id == id)
                                 .Where(CommonFilters.CustomFilters<Entities.InfoUtilEntity>(_tenantHolder.UsuarioId, ModuloEnum.InfoUtil, db))
                                 .Single();

                    InfoUtil info = Mapper.Map<InfoUtil>(entity);
                    info.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.InfoUtil, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.InfoUtil, entity.Id, db)
                    });
                    return info;
                }
                catch (InvalidOperationException)
                {
                    return new NullInfoUtil();
                }
            }
        }


    }
}