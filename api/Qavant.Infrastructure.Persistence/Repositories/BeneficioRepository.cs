﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Beneficios;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Entities;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class BeneficioRepository : IBeneficioRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public BeneficioRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Beneficio GetBeneficioById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Beneficios
                                    .Where(b => b.Id == id)
                                    .Where(b => b.Habilitada)
                                    .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                                    .Single();

                    Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                    beneficio.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, entity.Id, db)
                    });
                    return beneficio;
                }
                catch (InvalidOperationException)
                {
                    return new NullBeneficio();
                }
            }
        }

        public ICollection<Beneficio> GetBeneficios()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Beneficios
                        .Include(b => b.Categorias)
                        .Where(x => x.Habilitada == true)
                        .OrderByDescending(x => x.FechaAlta)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficios(int page)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Include(b => b.Categorias)
                        .Where(x => x.Habilitada == true)
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => categoriaId == null || x.Categorias.Any(a => a.Id == categoriaId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .Where(CommonFilters.CustomFilters<Entities.BeneficioEntity>(_tenantHolder.UsuarioId, ModuloEnum.Beneficios, db))
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();


                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        #region Favoritos
        public void SetLike(int beneficioId, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                var LikesActuales = db.BeneficiosLikes.Where(w => w.BeneficioId == beneficioId && w.UsuarioId == usuarioId).ToList();
                if (LikesActuales.Count() == 0)
                {
                    BeneficioLikeEntity oBenefLike = new BeneficioLikeEntity();
                    oBenefLike.BeneficioId = beneficioId;
                    oBenefLike.UsuarioId = usuarioId;
                    db.BeneficiosLikes.Add(oBenefLike);
                    db.SaveChanges();
                }
                else
                {
                    db.BeneficiosLikes.RemoveRange(LikesActuales);
                    db.SaveChanges();
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .Where(x => categoriaId == null || x.Categorias.Any(a => a.Id == categoriaId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritos(int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .OrderByDescending(x => x.FechaAlta)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Beneficio> GetBeneficiosFavoritos(int page, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
                    var entities = db.Beneficios
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Likes.Any(a => a.UsuarioId == usuarioId))
                        .OrderByDescending(x => x.FechaAlta)
                        .Skip((page - 1) * recordsPerPage).Take(recordsPerPage)
                        .ToList()
                        .Select(b => new BeneficioEntity
                        {
                            Id = b.Id,
                            Titulo = b.Titulo,
                            Copete = b.Copete,
                            Contenido = b.Contenido,
                            Imagen = b.Imagen,
                            FechaDesde = b.FechaDesde,
                            FechaHasta = b.FechaHasta,
                            ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Beneficios, b.Id, db),
                            TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Beneficios, b.Id, db)
                        })
                        .ToList();

                    List<Beneficio> beneficiosResults = new List<Beneficio>();
                    foreach (var entity in entities)
                    {
                        Beneficio beneficio = Mapper.Map<Beneficio>(entity);
                        beneficio.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        beneficiosResults.Add(beneficio);
                    }
                    return beneficiosResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public bool GetLikedByBeneficioId(int id, int usuarioId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var beneficio = (from b in db.Beneficios
                                     where b.Id == id && b.Habilitada && b.Likes.Any(bl => bl.UsuarioId == usuarioId)
                                     select b).Count() > 0;
                    return beneficio;
                }
                catch (InvalidOperationException)
                {
                    return false;
                }
            }
        }
        #endregion

    }
}