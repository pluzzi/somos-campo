﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Credencial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Qavant.Domain.Usuarios;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class CredencialRepository : ICredencialRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public CredencialRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Credencial GetCredencialById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var credencial = db.Credenciales                                  
                                    .Single();

                    var user = db.Usuarios
                        .Include(a => a.Area)            
                        .Where(u => u.Id == id)
                        .Single();
                    
                    Credencial credReemplazo = new Credencial();

                    credReemplazo.Id = credencial.Id;
                    if(user.Imagen == "")
                    {
                        var listSettings = _tenantHolder.Tenant.ParametrosPublicos;
                        var colorPrincipal = "";
                        var colorSecundario = "";

                        foreach (var item in listSettings)
                        {
                            if (item.Key == "theme")
                            {
                                foreach (var i in item.Value)
                                {
                                    if (i.Key == "PrimaryColor")
                                        colorPrincipal = i.Value;
                                    if (i.Key == "SecondaryColor")
                                        colorSecundario = i.Value;
                                }
                            }
                        }
                        var primerLetraNombre = user.Nombre.Substring(0, 1).ToUpper();
                        var primerLetraApellido = user.Apellido.Substring(0, 1).ToUpper();
                        var texto = primerLetraApellido + primerLetraNombre;
                        if (user.Imagen == "")
                        {
                            AvatarGenerator avatar = new AvatarGenerator();
                            user.Imagen = avatar.Generate(primerLetraNombre, primerLetraApellido, colorPrincipal, colorSecundario);
                        }
                    }
                    credReemplazo.Html = credencial.Html.Replace("#fotoCredencial", user.Imagen);
                    credReemplazo.Html = credReemplazo.Html.Replace("#nombreUsuario", user.Nombre);        
                    credReemplazo.Html = credReemplazo.Html.Replace("#apellidoUsuario", user.Apellido);
                    credReemplazo.Html = credReemplazo.Html.Replace("#areaUsuario", user.Area.Nombre);
                    credReemplazo.Html = credReemplazo.Html.Replace("#legajoUsuario", user.Legajo);
                    credReemplazo.Html = credReemplazo.Html.Replace("#dniUsuario", user.Dni);
                    return credReemplazo;
                }
                catch (InvalidOperationException)
                {
                    return new NullCredencial();
                }
            }
        }
    }
}