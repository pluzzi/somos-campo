﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Galerias;
using Qavant.Domain.Modulos;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Qavant.Domain.Reacciones;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class GaleriaRepository : IGaleriaRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public GaleriaRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Galeria ObtenerGaleriaPorId(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Galerias
                                    .Include(g => g.Categoria)
                                    .Where(g => g.Id == id)
                                    .Where(g => g.FechaDesde <= DateTime.Today)
                                    .Where(g => g.FechaHasta >= DateTime.Today)
                                    .Where(g => g.Habilitada)
                                    .Where(CommonFilters.CustomFilters<Entities.GaleriaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Galerias, db))
                                    .Single();

                    Galeria galeria = Mapper.Map<Galeria>(entity);
                    galeria.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Galerias, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Galerias, entity.Id, db)
                    });
                    return galeria;
                }
                catch (InvalidOperationException)
                {
                    return new NullGaleria();
                }
            }
        }

        public ICollection<Galeria> ObtenerGalerias()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Galerias
                                    .Include(g => g.Categoria)
                                    .Where(g => g.Habilitada == true)
                                    .Where(g => g.FechaDesde <= DateTime.Today)
                                    .Where(g => g.FechaHasta >= DateTime.Today)
                                    .OrderByDescending(g => new { g.Destacada })
                                    .ThenByDescending(g => new { g.FechaAlta } )
                                    .Where(CommonFilters.CustomFilters<Entities.GaleriaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Galerias, db))
                                    .ToList()
                                    .Select(g => new Entities.GaleriaEntity
                                    {
                                        Id = g.Id,
                                        Titulo = g.Titulo,
                                        Categoria = g.Categoria,
                                        Copete = g.Copete,
                                        FechaAlta = g.FechaAlta,
                                        FechaDesde = g.FechaDesde,
                                        ImagenPortada = g.ImagenPortada,
                                        Destacada = g.Destacada,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Galerias, g.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Galerias, g.Id, db)
                                    })
                                    .ToList();

                    List<Galeria> galeriasResults = new List<Galeria>();
                    foreach (var entity in entities)
                    {
                        Galeria galeria = Mapper.Map<Galeria>(entity);
                        galeria.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        galeriasResults.Add(galeria);
                    }
                    return galeriasResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public ICollection<Galeria> ObtenerGalerias(int pagina)
        {
            int recordsPerPage = Convert.ToInt32(_tenantHolder.Tenant.ParametrosPublicos["global"]["RecordsPerPage"]);
            var galerias = ObtenerGalerias();
            return galerias.Skip((pagina - 1) * recordsPerPage).Take(recordsPerPage).ToList();
        }

        public ICollection<Galeria> ObtenerGaleriasPorCategoria(int categoriaId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Galerias
                                    .Include(g => g.Categoria)
                                    .Where(g => g.CategoriaId == categoriaId)
                                    .Where(g => g.Habilitada == true)
                                    .Where(g => g.FechaDesde <= DateTime.Today)
                                    .Where(g => g.FechaHasta >= DateTime.Today)
                                    .OrderByDescending(g => new { g.Destacada })
                                    .ThenByDescending(g => new { g.FechaAlta })
                                    .Where(CommonFilters.CustomFilters<Entities.GaleriaEntity>(_tenantHolder.UsuarioId, ModuloEnum.Galerias, db))
                                    .ToList()
                                    .Select(g => new Entities.GaleriaEntity
                                    {
                                        Id = g.Id,
                                        Titulo = g.Titulo,
                                        Categoria = g.Categoria,
                                        Copete = g.Copete,
                                        FechaAlta = g.FechaAlta,
                                        FechaDesde = g.FechaDesde,
                                        ImagenPortada = g.ImagenPortada,
                                        Destacada = g.Destacada,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Galerias, g.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Galerias, g.Id, db)
                                    })
                                    .ToList();

                    List<Galeria> galeriasResults = new List<Galeria>();
                    foreach (var entity in entities)
                    {
                        Galeria galeria = Mapper.Map<Galeria>(entity);
                        galeria.SetInfoReaccion(new InfoReaccion()
                        {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        galeriasResults.Add(galeria);
                    }
                    return galeriasResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }

            }
        }
    }
}
