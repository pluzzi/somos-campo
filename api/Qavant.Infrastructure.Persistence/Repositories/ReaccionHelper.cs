﻿using Qavant.Domain.Modulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class ReaccionHelper
    {
        public static int GetTotalReacciones(int tenantId, ModuloEnum modulo, int entidadId, QavantDbContext db)
        {
            return db.ReaccionesUsuarios
                .Where(r => r.TenantId == tenantId)
                .Where(r => r.ModuloId == (int)modulo)
                .Where(r => r.EntidadId == entidadId)
                .ToList()
                .Select(n => new
                {
                    uid = n.UsuarioId
                })
                .Distinct()
                .Count();
        }

        public static int GetReaccionId(int tenantId, int usuarioId, ModuloEnum modulo, int entidadId, QavantDbContext db)
        {
            var reaccion = db.ReaccionesUsuarios
                             .Where(n => n.TenantId == tenantId)
                             .Where(n => n.UsuarioId == usuarioId)
                             .Where(n => n.ModuloId == (int)modulo)
                             .Where(n => n.EntidadId == entidadId)
                             .OrderByDescending(n => n.FechaAlta)
                             .FirstOrDefault();
            if (reaccion == null)
                return 0;
            else
                return reaccion.ReaccionId;
        }

    }
}
