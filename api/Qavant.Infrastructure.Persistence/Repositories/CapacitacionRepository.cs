﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Capacitaciones;
using Qavant.Domain.Categorias;
using Qavant.Domain.Modulos;
using Qavant.Domain.Reacciones;
using Qavant.Infrastructure.Persistence.Entities;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class CapacitacionRepository: ICapacitacionRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public CapacitacionRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;

        }

        public ICollection<Capacitacion> GetCapacitaciones()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Capacitaciones
                                    .Include(c => c.Categoria)
                                    .Include(c => c.CapacitacionTipo)
                                    .Where(x => x.Habilitada)
                                    .Where(x => x.FechaDesde <= DateTime.Today)
                                    .Where(x => x.FechaHasta >= DateTime.Today)
                                    .Where(x => x.TenantId == _tenantHolder.Tenant.Id)
                                    .Where(CommonFilters.CustomFilters<Entities.CapacitacionEntity>(_tenantHolder.UsuarioId, ModuloEnum.Capacitaciones, db))
                                    .ToList()
                                    .Select(c => new CapacitacionEntity
                                    {
                                        Id = c.Id, 
                                        Titulo = c.Titulo,
                                        Descripcion = c.Descripcion,
                                        DocumentoURL = c.DocumentoURL,
                                        VideoURL = c.VideoURL,
                                        LinkURL = c.LinkURL,
                                        Categoria = c.Categoria,
                                        FechaDesde = c.FechaDesde,
                                        FechaHasta = c.FechaHasta,
                                        RequiereEvaluacion = c.RequiereEvaluacion,
                                        CapacitacionTipo = c.CapacitacionTipo,
                                        Habilitada = c.Habilitada,
                                        FechaAlta = c.FechaAlta,
                                        FormularioId = c.FormularioId,
                                        PorcentajeAprobado = c.PorcentajeAprobado,
                                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Capacitaciones, c.Id, db),
                                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Capacitaciones, c.Id, db)
                                    })
                                    .ToList();

                    List<Capacitacion> capacitacionesResults = new List<Capacitacion>();
                    foreach (var entity in entities)
                    {
                        Capacitacion capacitacion = Mapper.Map<Capacitacion>(entity);
                        capacitacion.SetInfoReaccion(new InfoReaccion() {
                            ReaccionId = entity.ReaccionId,
                            TotalReacciones = entity.TotalReacciones
                        });
                        capacitacionesResults.Add(capacitacion);
                    }
                    return capacitacionesResults;
                }
                catch (InvalidOperationException)
                {
                    throw;
                }
            }
        }

        public Capacitacion GetCapacitacionById(int id)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entity = db.Capacitaciones
                                         .Include(c => c.Categoria)
                                         .Include(c => c.CapacitacionTipo)
                                         .Where(c => c.Id == id)
                                         .Where(x => x.Habilitada)
                                         .Where(x => x.FechaDesde <= DateTime.Today)
                                         .Where(x => x.FechaHasta >= DateTime.Today)
                                         .Where(x => x.TenantId == _tenantHolder.Tenant.Id)
                                         .Where(CommonFilters.CustomFilters<Entities.CapacitacionEntity>(_tenantHolder.UsuarioId, ModuloEnum.Capacitaciones, db))
                                         .FirstOrDefault();
                    
                    Capacitacion capacitacion = Mapper.Map<Capacitacion>(entity);
                    capacitacion.SetInfoReaccion(new InfoReaccion()
                    {
                        ReaccionId = ReaccionHelper.GetReaccionId(_tenantHolder.Tenant.Id, _tenantHolder.UsuarioId, ModuloEnum.Capacitaciones, entity.Id, db),
                        TotalReacciones = ReaccionHelper.GetTotalReacciones(_tenantHolder.Tenant.Id, ModuloEnum.Capacitaciones, entity.Id, db)
                    });
                    return capacitacion;
                }
                catch (InvalidOperationException)
                {
                    return new NullCapacitacion();
                }
            }
        }

    }
}