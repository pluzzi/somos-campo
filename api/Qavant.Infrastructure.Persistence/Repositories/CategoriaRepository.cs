﻿using AutoMapper;
using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class CategoriaRepository: ICategoriaRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public CategoriaRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<Categoria> ObtenerCategoriasPorModuloId(int moduloId)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Categorias
                        .Where(x => x.Habilitada == true)
                        .Where(x=> x.ModuloId == moduloId)
                        .ToList();

                    List<Categoria> categoriasResults = new List<Categoria>();
                    foreach (var entity in entities)
                    {
                        Categoria categoria = Mapper.Map<Categoria>(entity);
                        categoriasResults.Add(categoria);
                    }
                    return categoriasResults;
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }

        public ICollection<Categoria> ObtenerCategoriasPorModulo(string nemonico)
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                try
                {
                    var entities = db.Categorias
                        //.Include(x => x.Modulo)
                        .Where(x => x.Habilitada == true)
                        .Where(x => x.Modulo.Nemonico == nemonico)
                        .ToList();

                    List<Categoria> categoriasResults = new List<Categoria>();
                    foreach (var entity in entities)
                    {
                        Categoria categoria = Mapper.Map<Categoria>(entity);
                        categoriasResults.Add(categoria);
                    }
                    return categoriasResults;
                }
                catch (InvalidOperationException ex)
                {
                    throw;
                }
            }
        }
    }
}