﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class ModuloRepository : IModuloRepository
    {
        private readonly ITenantHolder _tenantHolder;

        public ModuloRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public Modulo GetModuloPorNemonico(string nemonico)
        {
            using (var db = new QavantTenantDbContext())
            {
                try
                {
                    var modulo = db.Modulos.Where(m => m.Nemonico == nemonico).Single();
                    return modulo;
                }
                catch (InvalidOperationException)
                {
                    return new NullModulo();
                }
            }
        }
    }
}
