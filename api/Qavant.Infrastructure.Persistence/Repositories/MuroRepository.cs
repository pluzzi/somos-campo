﻿using Qavant.Application;
using Qavant.Application.Repositories;
using Qavant.Domain.Muro;
using Qavant.Infrastructure.Persistence.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Repositories
{
    public class MuroRepository: IMuroRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString;

        private readonly ITenantHolder _tenantHolder;

        public MuroRepository(ITenantHolder tenantHolder)
        {
            _tenantHolder = tenantHolder;
        }

        public ICollection<Publicacion> GetPublicaciones()
        {
            using (var db = new QavantDbContext(_tenantHolder.Tenant.Id))
            {
                var empresaIdParameter = new SqlParameter("@EmpresaId", _tenantHolder.Tenant.Id);
                var paginacionParameter = new SqlParameter("@Paginacion", true);
                var siguienteIdParameter = new SqlParameter("@SiguienteId", 1);
                var userIdParameter = new SqlParameter("@UserId", _tenantHolder.UsuarioId);
                var fechaActualParameter = new SqlParameter("@FechaActual", DateTime.Now);

                var tempResult = db.Database
                    .SqlQuery<Publicacion>("mur_GetDatosMuro @EmpresaId, @Paginacion, @SiguienteId, @UserId, @FechaActual", empresaIdParameter, paginacionParameter, siguienteIdParameter, userIdParameter, fechaActualParameter)
                    .ToList();

                var publicaciones = tempResult
                    .AsQueryable()
                    .Where(CommonFilters.CustomFiltersMuro(_tenantHolder.UsuarioId, db)).OrderByDescending(o => o.FechaAlta)
                    .ToList();

                return publicaciones;
            }
        }

        
    }
}


