﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class FormsRespDetalleEntity
    {
        public int Id { get; set; }
        public int CabeceraRespuestasFormId { get; set; }
        public int PreguntaId { get; set; }
        public int? OpcionIdSeleccionada { get; set; } // Pueder contener un valor que proviene de una pregunta de opcion única o múltiple
        public List<Entities.FormsOpcionEntity> RespuestaSeleccion { get; set; }
        public string RespuestaSimple { get; set; }
    }
}
