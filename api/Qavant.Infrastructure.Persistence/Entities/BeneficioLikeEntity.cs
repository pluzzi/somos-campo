﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class BeneficioLikeEntity
    {
        public int Id { get; set; }

        public int BeneficioId { get; set; }

        public int UsuarioId { get; set; }

        public virtual Entities.BeneficioEntity Beneficio { get; set; }
    }
}
