﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class UsuarioEntity
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int RegionId { get; set; }
        public int SucursalId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Imagen { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Interno { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public bool Activo { get; set; }
        public bool Habilititado { get; set; }
        public string Genero { get; set; }
        public int GeneroCatalogoId { get; set; }
        public string Legajo { get; set; }
        public string Dni { get; set; }
        public Area Area { get; set; }
        public ICollection<Entities.GrupoEntity> Grupos { get; set; }
    }
}
