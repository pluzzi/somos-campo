﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class CapacitacionEntity : IEntity
    {
        public int Id { get; set; }
        public int CapacitacionTipoId { get; set; }
        public string UsuarioCreadorId { get; set; }
        public int CategoriaId { get; set; }
        public int? FormularioId { get; set; }
        public int TenantId { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string DocumentoURL { get; set; }
        public string VideoURL { get; set; }
        public string LinkURL { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public bool RequiereEvaluacion { get; set; }
        public bool Habilitada { get; set; }
        public DateTime FechaAlta { get; set; }
        public int PorcentajeAprobado { get; set; }

        public Entities.CategoriaEntity Categoria { get; set; }
        public Entities.CapacitacionTipoEntity CapacitacionTipo { get; set; }

        [NotMapped]
        public int ReaccionId { get; set; }
        [NotMapped]
        public int TotalReacciones { get; set; }
    }
}
