﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class FormsRespCabeceraEntity
    {
        public int Id { get; set; }
        public int FormularioId { get; set; }
        public int UsuarioId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int EstadoId { get; set; }
    }
}
