﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class NoticiaEntity : IEntity
    {
        public int Id { get; set; }

        public int TenantId { get; set; }

        public string Titulo { get; set; }

        public string Copete { get; set; }

        public string Contenido { get; set; }

        public string Imagen { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool Destacada { get; set; }

        public bool Habilitada { get; set; }

        [NotMapped]
        public int ReaccionId { get; set; }
        [NotMapped]
        public int TotalReacciones { get; set; }
    }
}
