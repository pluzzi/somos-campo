﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class CapacitacionTipoEntity
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
