﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class FormsPreguntaEntity
    {
        public int Id { get; set; }
        public Int16 Orden { get; set; }
        public string Enunciado { get; set; }
        public string HtmlInputType { get; set; }
        public bool Habilitado { get; set; }
        public bool Requerido { get; set; }
        public int FormularioId { get; set; }
        public Entities.FormsEntity Formulario { get; set; }
        public ICollection<Entities.FormsOpcionEntity> Opciones { get; set; }
    }
}
