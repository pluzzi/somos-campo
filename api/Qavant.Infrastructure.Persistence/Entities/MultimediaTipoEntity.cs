﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class MultimediaTipoEntity
    {
        public Int16 Id { get; set; }
        public string Descripcion { get; set; }
        public string Nemonico { get; set; }


    }
}
