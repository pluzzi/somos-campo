﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Entities
{
    public class FormsOpcionEntity
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Int16 Orden { get; set; }
        public bool Habilitado { get; set; }
        public int PreguntaId { get; set; }
        public Entities.FormsPreguntaEntity Pregunta { get; set; }
    }
}
