﻿using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class NoticiaEntityMap : EntityTypeConfiguration<Entities.NoticiaEntity>
    {
        public NoticiaEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("Titulo");

            Property(t => t.Copete)
                .IsOptional()
                .HasColumnName("Copete");

            Property(t => t.Contenido)
                .IsOptional()
                .HasColumnName("Contenido");

            Property(t => t.Imagen)
                .IsOptional()
                .HasColumnName("Image");

            Property(t => t.FechaAlta)
                .IsOptional()
                .HasColumnName("FechaAlta");

            Property(t => t.Destacada)
                .IsOptional()
                .HasColumnName("Destacada");

            Property(t => t.Habilitada)
                .IsOptional()
                .HasColumnName("Habilitada");

            // Table and relationships 
            ToTable("Noticias");
        }
    }
}