﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class GaleriaEntityMap : EntityTypeConfiguration<Entities.GaleriaEntity>
    {
        public GaleriaEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("Titulo");

            Property(t => t.Copete)
                .IsRequired()
                .HasColumnName("Copete");

            Property(t => t.ImagenPortada)
                .IsRequired()
                .HasColumnName("ImagenPortada");

            Property(t => t.FechaAlta)
                .IsRequired()
                .HasColumnName("FechaAlta");

            Property(t => t.FechaAlta)
                .IsRequired()
                .HasColumnName("FechaAlta");

            Property(t => t.Habilitada)
                .IsRequired()
                .HasColumnName("Habilitada");

            Property(t => t.Destacada)
                .IsRequired()
                .HasColumnName("Destacada");

            Property(t => t.CategoriaId)
                .IsRequired()
                .HasColumnName("CategoriaId");

            // Table and relationships 
            ToTable("Galerias");
        }
    }
}
