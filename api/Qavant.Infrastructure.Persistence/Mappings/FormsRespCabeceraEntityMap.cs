﻿using Qavant.Domain.Formularios;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class FormsRespCabeceraEntityMap : EntityTypeConfiguration<Entities.FormsRespCabeceraEntity>
    {
        public FormsRespCabeceraEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.FormularioId)
                .IsRequired()
                .HasColumnName("FormsId");

            Property(t => t.UsuarioId)
                .IsRequired()
                .HasColumnName("UserId");

            Property(t => t.EstadoId)
                .IsRequired()
                .HasColumnName("FormsEstadoId");

            Property(t => t.FechaCreacion)
                .IsOptional()
                .HasColumnName("FechaCreacion");




            // Table and relationships 
            ToTable("FormsRespCabecera");
        }
    }
}