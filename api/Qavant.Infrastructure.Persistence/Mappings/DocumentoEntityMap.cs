﻿using Qavant.Domain.Documentos;
using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class DocumentoEntityMap : EntityTypeConfiguration<Entities.DocumentoEntity>
    {
        public DocumentoEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Title)
                .IsRequired()
                .HasColumnName("Title");

            Property(t => t.Description)
                .IsOptional()
                .HasColumnName("Description");

            Property(t => t.FilePath)
                .IsOptional()
                .HasColumnName("Filepath");

            Property(t => t.Enabled)
                .IsOptional()
                .HasColumnName("Enabled");

            Property(t => t.CreatedDate)
                .IsOptional()
                .HasColumnName("CreatedDate");

            Property(t => t.DocumentLibraryId)
                .IsOptional()
                .HasColumnName("DocumentLibraryId");

            Property(t => t.CategoriaId)
                .IsOptional()
                .HasColumnName("CategoriaId");

            // Table and relationships 
            ToTable("Documents");
        }
    }
}