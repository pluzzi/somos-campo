﻿using Qavant.Domain.Reacciones;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class ReaccionesUsuarioMap : EntityTypeConfiguration<ReaccionesUsuario>
    {
        public ReaccionesUsuarioMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.ModuloId)
                .IsRequired()
                .HasColumnName("ModuloId");

            Property(t => t.EntidadId)
                .IsRequired()
                .HasColumnName("EntidadId");

            Property(t => t.UsuarioId)
                .IsRequired()
                .HasColumnName("UserId");

            Property(t => t.ReaccionId)
                .IsRequired()
                .HasColumnName("ReaccionId");

            Property(t => t.FechaAlta)
                .IsRequired()
                .HasColumnName("FechaAlta");

            // Table and relationships 
            ToTable("ReaccionesUsers");
        }
    }
}