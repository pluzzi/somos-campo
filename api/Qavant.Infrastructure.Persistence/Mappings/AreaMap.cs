﻿using Qavant.Domain.Noticias;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class AreaMap : EntityTypeConfiguration<Area>
    {
        public AreaMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Nombre");

            Property(t => t.Habilitada)
                .IsOptional()
                .HasColumnName("Habilitada");


            // Table and relationships 
            ToTable("Areas");
        }
    }
}