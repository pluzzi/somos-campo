﻿using System.Data.Entity.ModelConfiguration;


namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class CategoriaEntityMap : EntityTypeConfiguration<Entities.CategoriaEntity>
    {
        public CategoriaEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties			
            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Nombre");

            Property(t => t.Imagen)
                .IsOptional()
                .HasColumnName("Imagen");

            Property(t => t.Habilitada)
                .IsRequired()
                .HasColumnName("Habilitada");

            Property(t => t.ModuloId)
                .IsRequired()
                .HasColumnName("ModuloId");

            Property(t => t.TenantId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.PadreId)
                .IsOptional()
                .HasColumnName("PadreCategoriaId");

            // Table and relationships 
            ToTable("Categorias");
        }
    }

}