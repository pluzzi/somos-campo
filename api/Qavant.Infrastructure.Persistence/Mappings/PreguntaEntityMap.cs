﻿using Qavant.Domain.Formularios;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class PreguntaEntityMap : EntityTypeConfiguration<Entities.FormsPreguntaEntity>
    {
        public PreguntaEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.FormularioId)
                .IsRequired()
                .HasColumnName("Form_Id");

            Property(t => t.HtmlInputType)
                .IsRequired()
                .HasColumnName("FormsQuestionsType_Id");

            Property(t => t.Enunciado)
                .IsOptional()
                .HasColumnName("Question");

            Property(t => t.Orden)
                .IsOptional()
                .HasColumnName("Order");

            Property(t => t.Habilitado)
                .IsOptional()
                .HasColumnName("Enabled");

            Property(t => t.Requerido)
                .IsOptional()
                .HasColumnName("Required");

            // Table and relationships 
            ToTable("FormsQuestions");
        }
    }
}