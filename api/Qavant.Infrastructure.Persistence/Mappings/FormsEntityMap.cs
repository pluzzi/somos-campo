﻿using Qavant.Domain.Formularios;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class FormsEntityMap : EntityTypeConfiguration<Entities.FormsEntity>
    {
        public FormsEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
               .IsRequired()
               .HasColumnName("EmpresaId");

            Property(t => t.Tipo)
                .IsOptional()
                .HasColumnName("FormTypeId");

            Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("Title");

            Property(t => t.Descripcion)
                .IsOptional()
                .HasColumnName("Description");

            Property(t => t.FechaCreacion)
                .IsOptional()
                .HasColumnName("CreationDate");

            Property(t => t.FechaDesde)
                .IsOptional()
                .HasColumnName("DateFrom");

            Property(t => t.FechaHasta)
                .IsOptional()
                .HasColumnName("DateTo");

            Property(t => t.Habilitado)
                .IsOptional()
                .HasColumnName("Enabled");

            Property(t => t.RespuestaUnica)
                .IsOptional()
                .HasColumnName("UniqueReply");

            Property(t => t.EnviaEmail)
                .IsOptional()
                .HasColumnName("SendMails");

            Property(t => t.EmailPara)
                .IsOptional()
                .HasColumnName("MailsTo");

            Property(t => t.EmailAsunto)
                .IsOptional()
                .HasColumnName("MailSubject");

            Property(t => t.EmailTitulo)
                .IsOptional()
                .HasColumnName("MailTitle");

            Property(t => t.EmailIntroduccion)
                .IsOptional()
                .HasColumnName("MailIntro");

            Property(t => t.Eliminado)
                .IsOptional()
                .HasColumnName("Deleted");

            Ignore(t => t.Respondido);

            // Table and relationships 
            ToTable("Forms");
        }
    }
}

