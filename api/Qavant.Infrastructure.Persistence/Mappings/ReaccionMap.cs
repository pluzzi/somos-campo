﻿using Qavant.Domain.Reacciones;
using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class ReaccionMap : EntityTypeConfiguration<Reaccion>
    {
        public ReaccionMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties			
            Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("Nombre");

            Property(t => t.Icono)
                .IsRequired()
                .HasColumnName("Icono");

            Property(t => t.Orden)
                .IsRequired()
                .HasColumnName("Orden");

            // Table and relationships 
            ToTable("Reacciones");
        }
    }
}
