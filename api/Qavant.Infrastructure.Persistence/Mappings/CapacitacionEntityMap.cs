﻿using System.Data.Entity.ModelConfiguration;

namespace Qavant.Infrastructure.Persistence.Mappings
{
    public class CapacitacionEntityMap : EntityTypeConfiguration<Entities.CapacitacionEntity>
    {
        public CapacitacionEntityMap()
        {
            // Primary Key
            HasKey(t => t.Id);
            Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id");

            // Properties	
            Property(t => t.TenantId)
                .IsRequired()
                .HasColumnName("EmpresaId");

            Property(t => t.Titulo)
               .IsRequired()
               .HasColumnName("Title");

            Property(t => t.Descripcion)
                .IsOptional()
                .HasColumnName("Description");

            Property(t => t.CapacitacionTipoId)
                .IsRequired()
                .HasColumnName("CourseType_Id");

            Property(t => t.DocumentoURL)
                .IsOptional()
                .HasColumnName("DocumentURL");

            Property(t => t.VideoURL)
                .IsOptional()
                .HasColumnName("VideoURL");

            Property(t => t.LinkURL)
                .IsOptional()
                .HasColumnName("LinkURL");

            Property(t => t.FechaDesde)
                .IsRequired()
                .HasColumnName("From");

            Property(t => t.FechaHasta)
                .IsRequired()
                .HasColumnName("To");

            Property(t => t.RequiereEvaluacion)
                .IsRequired()
                .HasColumnName("RequiresEvaluation");

            Property(t => t.FormularioId)
                .IsOptional()
                .HasColumnName("Form_Id");

            Property(t => t.Habilitada)
                .IsRequired()
                .HasColumnName("Enabled");

            Property(t => t.UsuarioCreadorId)
                .IsOptional()
                .HasColumnName("Author_Id");

            Property(t => t.FechaAlta)
                .IsRequired()
                .HasColumnName("CreationDate");

            Property(t => t.PorcentajeAprobado)
                .IsRequired()
                .HasColumnName("ApprovedPerc");

            Property(t => t.CategoriaId)
                .IsRequired()
                .HasColumnName("CategoriaId");

            // Table and relationships 
            ToTable("Courses");
        }
    }
}