﻿using Qavant.Domain.Modulos;
using Qavant.Domain.Tenants;
using Qavant.Infrastructure.Persistence.Mappings;
using System.Configuration;
using System.Data.Entity;

namespace Qavant.Infrastructure.Persistence
{
    public class QavantTenantDbContext : DbContext
    {
        public DbSet<Tenant> Tenants { get; private set; }
        public DbSet<Modulo> Modulos { get; private set; }

        public QavantTenantDbContext() : base(ConfigurationManager.ConnectionStrings["QavantDbConnection"].ConnectionString)   // specify here conn-string entry if using SQL Server
        {
            Tenants = base.Set<Tenant>();
            Modulos = base.Set<Modulo>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<NullTenant>();
            modelBuilder.Ignore<NullModulo>();
            modelBuilder.Configurations.Add(new TenantMap());
            modelBuilder.Configurations.Add(new ModuloMap());

        }
    }
}