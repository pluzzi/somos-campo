﻿using Qavant.Application.Services.Shared;
using Qavant.Domain.Galerias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Galerias
{
    public class GaleriaOutput
    {
        public int Id { get; }
        public string Titulo { get; }
        public string Copete { get; }
        public int? CategoriaId { get; }
        public string Categoria { get; }
        public string FechaAlta { get; }
        public string FechaDesde { get; }
        public string ImagenPortada { get; }
        public bool Destacada { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }
        public List<ElementoMultimediaOutput> ElementosMultimedia { get; }


        public GaleriaOutput(Galeria galeria)
        {
            Id = galeria.Id;
            Titulo = galeria.Titulo;
            Copete = galeria.Copete;
            FechaAlta = galeria.FechaAlta.ToString("dd/MM/yyyy");
            FechaDesde = galeria.FechaDesde.ToString("dd/MM/yyyy");
            ImagenPortada = galeria.ImagenPortada;
            Destacada = galeria.Destacada;
            CategoriaId = galeria.Categoria?.Id;
            Categoria = galeria.Categoria?.Nombre;
            TotalReacciones = galeria.InfoReaccion.TotalReacciones;
            ReaccionId = galeria.InfoReaccion.ReaccionId;

            List<ElementoMultimediaOutput> elementosResults = new List<ElementoMultimediaOutput>();
            foreach (var elemento in galeria.ElementosMultimedia)
            {
                ElementoMultimediaOutput output = new ElementoMultimediaOutput(elemento);
                elementosResults.Add(output);
            }
            ElementosMultimedia = elementosResults;
        }
    }
}