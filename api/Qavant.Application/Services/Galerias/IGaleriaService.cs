﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Galerias
{
    public interface IGaleriaService
    {
        GaleriaListOutput ObtenerGalerias();
        GaleriaListOutput ObtenerGalerias(int pagina);
        GaleriaListOutput ObtenerGaleriasPorCategoria(int categoriaId);
        GaleriaOutput ObtenerGaleriaPorId(int galeriaId);
        ElementoMultimediaListOuput ObtenerElementosMultimedia(int galeriaId, int cantidad);
    }
}