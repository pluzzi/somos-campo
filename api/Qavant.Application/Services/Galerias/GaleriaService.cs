﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Galerias;
using Qavant.Domain.Modulos;
using System;
using System.Linq;

namespace Qavant.Application.Services.Galerias
{
    public class GaleriaService : IGaleriaService
    {
        private IGaleriaRepository _galeriaRepository;
        private IMultimediaRepository _multimediaRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public GaleriaService(IGaleriaRepository galeriaRepository, IMultimediaRepository multimediaRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _galeriaRepository = galeriaRepository;
            _multimediaRepository = multimediaRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public GaleriaOutput ObtenerGaleriaPorId(int galeriaId)
        {
            var galeria = _galeriaRepository.ObtenerGaleriaPorId(galeriaId);
            if (galeria is NullGaleria)
                throw new ApplicationException("Galeria no encontrada.");

            var elementosMultimedia = _multimediaRepository.ObtenerElementosMultimedia(ModuloEnum.Galerias, galeriaId);
            galeria.SetElementosMultimedia(elementosMultimedia);

            var log = LogActividad.CreateNew(ModuloEnum.Galerias, LogTipo.VisitaDetalle, galeria.Id);
            _applicationLoggerActivity.Save(log);

            GaleriaOutput output = new GaleriaOutput(galeria);
            return output;
        }

        public GaleriaListOutput ObtenerGalerias()
        {
            var galerias = _galeriaRepository.ObtenerGalerias();
            GaleriaListOutput output = new GaleriaListOutput(galerias);
            var log = LogActividad.CreateNew(ModuloEnum.Galerias, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;

        }

        public GaleriaListOutput ObtenerGalerias(int pagina)
        {
            var galerias = _galeriaRepository.ObtenerGalerias(pagina);
            GaleriaListOutput output = new GaleriaListOutput(galerias);
            var log = LogActividad.CreateNew(ModuloEnum.Galerias, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public GaleriaListOutput ObtenerGaleriasPorCategoria(int categoriaId)
        {
            var galerias = _galeriaRepository.ObtenerGaleriasPorCategoria(categoriaId);
            GaleriaListOutput output = new GaleriaListOutput(galerias);
            var log = LogActividad.CreateNew(ModuloEnum.Galerias, LogTipo.VisitaPorCategorias);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public ElementoMultimediaListOuput ObtenerElementosMultimedia(int galeriaId, int cantidad)
        {
            var elementos = _multimediaRepository.ObtenerElementosMultimedia(ModuloEnum.Galerias, galeriaId);
            Random rnd = new Random();
            var elementosRandom = elementos.OrderBy(x => rnd.Next()).Take(cantidad).ToArray();
            ElementoMultimediaListOuput output = new ElementoMultimediaListOuput(elementosRandom);
            return output;
        }
    }
}