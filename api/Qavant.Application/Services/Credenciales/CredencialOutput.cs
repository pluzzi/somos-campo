﻿using Qavant.Domain.Credencial;
using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Credenciales
{
    public sealed class CredencialOutput
    {
        public int Id { get; }
        public string Html { get; }


        public CredencialOutput(Domain.Credencial.Credencial credencial)
        {
            Id = credencial.Id;
            Html = credencial.Html;          
        }
    }
}