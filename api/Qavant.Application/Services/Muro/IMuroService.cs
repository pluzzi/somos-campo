﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Muro
{
    public interface IMuroService
    {
        PublicacionListOutput GetPublicaciones();
    }
}
