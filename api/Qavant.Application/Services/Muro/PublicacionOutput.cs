﻿using Qavant.Application.Services.Shared;
using Qavant.Domain.Muro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Muro
{
    public class PublicacionOutput
    {
        public int Id { get; }
        public string Tipo { get; set; }
        public string DescRegistro { get; }
        public string Titulo { get; }
        public string Copete { get; }
        public string Contenido { get; }
        public string FechaAlta { get; }
        public string Imagen { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public PublicacionOutput(Publicacion publicacion)
        {
            Id = publicacion.Id;
            Tipo = publicacion.TipoRegistro;
            DescRegistro = publicacion.DescRegistro;
            Titulo = publicacion.Titulo;
            Copete = publicacion.Copete;
            Contenido = publicacion.Contenido;
            FechaAlta = publicacion.FechaAlta.ToString("dd/MM/yyyy");
            Imagen = publicacion.Imagen;
            ReaccionId = publicacion.ReaccionId;
            TotalReacciones = publicacion.TotalReacciones;
        }
    }
}