﻿using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Usuarios;

namespace Qavant.Application.Services.Login
{
    public class LoginService : ILoginService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IAuthentication _authentication;

        public LoginService(
            ITenantHolder tenantHolder,
            IUsuarioRepository usuarioRepository,
            IAuthentication authentication)
        {
            _tenantHolder = tenantHolder;
            _usuarioRepository = usuarioRepository;
            _authentication = authentication;
        }

        public LoginOutput Login(string username, string password)
        {
            _authentication.Configure(_tenantHolder.Tenant.ParametrosInternos["login"]);
            var isValid = _authentication.Validate(_tenantHolder.Tenant.Id, username, password);
            if (!isValid)
                throw new CredencialesInvalidasException($"Credenciales inválidas.");

            var usuario = _usuarioRepository.GetUsuarioByUsername(username);
            if (usuario is NullUsuario)
                throw new UsuarioNoRegistradoException($"El usuario '{username}' no está registrado en la empresa '{_tenantHolder.Tenant.Nombre}'.");

            if (!usuario.Activo)
                throw new UsuarioInactivoException("Su cuenta de usuario ha sido desactivada.");

            return new LoginOutput(usuario);
        }

        public LoginOutput Logged(string username)
        {
            var usuario = _usuarioRepository.GetUsuarioByUsername(username);
            if (usuario is NullUsuario)
            { 
                throw new UsuarioNoRegistradoException($"El usuario '{username}' no está registrado en la empresa '{_tenantHolder.Tenant.Nombre}'.");
            }
            LoginOutput output = new LoginOutput(usuario);
            return output;
        }
    }
}