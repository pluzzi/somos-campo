﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Authentication
{
    public interface ILoginService
    {
        UsuarioOutput Login(string username, string password, string tenantKey, Canal canal);
    }
}
