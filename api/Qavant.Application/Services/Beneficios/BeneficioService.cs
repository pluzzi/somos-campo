﻿using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Beneficios;
using Qavant.Domain.Modulos;
using System;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Beneficios
{
    public class BeneficioService : IBeneficioService
    {
        private IBeneficioRepository _beneficioRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public BeneficioService(
            IBeneficioRepository BeneficioRepository, 
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _beneficioRepository = BeneficioRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public BeneficioOutput SetLike( int beneficioId, int usuarioId)
        {
            if (beneficioId <= 0)
                throw new ArgumentNullException(nameof(beneficioId));

            _beneficioRepository.SetLike(beneficioId, usuarioId);

            var Beneficio = _beneficioRepository.GetBeneficioById(beneficioId);
            if (Beneficio is NullBeneficio)
                throw new ApplicationException("Beneficio no encontrado.");

            BeneficioOutput output = new BeneficioOutput(Beneficio);
            output.Liked = _beneficioRepository.GetLikedByBeneficioId(usuarioId, usuarioId);

            var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.AccionExitosa, Beneficio.Id);
            _applicationLoggerActivity.Save(log);

            return output;
        }

        public BeneficioOutput GetBeneficioById(int id, int usuarioId)
        {
            if (id <= 0)
                throw new ArgumentNullException(nameof(id));

            var Beneficio = _beneficioRepository.GetBeneficioById(id);
            if (Beneficio is NullBeneficio)
                throw new ApplicationException("Beneficio no encontrado.");

            BeneficioOutput output = new BeneficioOutput(Beneficio);
            output.Liked = _beneficioRepository.GetLikedByBeneficioId(id, usuarioId);

            var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaDetalle, Beneficio.Id);
            _applicationLoggerActivity.Save(log);

            return output;
        }

        public BeneficioListOutput GetBeneficios(int usuarioId)
        {
            var beneficios = _beneficioRepository.GetBeneficios();
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach (BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public BeneficioListOutput GetBeneficios(int page, int usuarioId)
        {
            var beneficios = _beneficioRepository.GetBeneficios(page);
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach(BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public BeneficioListOutput GetBeneficiosFavoritos(int usuarioId)
        {
            var beneficios = _beneficioRepository.GetBeneficiosFavoritos(usuarioId);
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach (BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public BeneficioListOutput GetBeneficiosFavoritos(int page, int usuarioId)
        {
            var beneficios = _beneficioRepository.GetBeneficiosFavoritos(page, usuarioId);
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach (BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public BeneficioListOutput GetBeneficiosFavoritosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            var beneficios = _beneficioRepository.GetBeneficiosFavoritosPorCategoria(page, usuarioId, categoriaId);
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach (BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaPorCategorias);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public BeneficioListOutput GetBeneficiosPorCategoria(int page, int usuarioId, int? categoriaId)
        {
            var beneficios = _beneficioRepository.GetBeneficiosPorCategoria(page, usuarioId, categoriaId);
            BeneficioListOutput output = new BeneficioListOutput(beneficios);
            foreach (BeneficioOutput beneficioOutput in output.Beneficios)
            {
                beneficioOutput.Liked = _beneficioRepository.GetLikedByBeneficioId(beneficioOutput.Id, usuarioId);
            }
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Beneficios, LogTipo.VisitaPorCategorias);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }
    }
}
