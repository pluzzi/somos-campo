﻿using Qavant.Domain.Beneficios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Beneficios
{
    public sealed class BeneficioListOutput
    {
        public List<BeneficioOutput> Beneficios { get; }

        public BeneficioListOutput(ICollection<Beneficio> beneficios)
        {
            List<BeneficioOutput> BeneficiosResults = new List<BeneficioOutput>();
            foreach (var beneficio in beneficios)
            {
                BeneficioOutput output = new BeneficioOutput(beneficio);
                BeneficiosResults.Add(output);
            }
            Beneficios = BeneficiosResults;
        }
    }
}