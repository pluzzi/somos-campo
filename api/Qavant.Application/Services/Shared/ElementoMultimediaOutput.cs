﻿using Qavant.Domain.Multimedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Shared
{
    public class ElementoMultimediaOutput
    {
        public string RutaArchivo { get; }
        public string Tipo { get; }

        public ElementoMultimediaOutput(ElementoMultimedia elemento)
        {
            RutaArchivo = elemento.RutaArchivo;
            Tipo = elemento.Tipo;
        }
    }
}
