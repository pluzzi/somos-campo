﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Documentos
{
    public interface IDocumentoService
    {
        DocumentoListOutput GetDocumentos();
        DocumentoListOutput GetDocumentos(int page);
        DocumentoListOutput GetDocumentoByCategoryId(int id, int page);
    }
}
