﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;

namespace Qavant.Application.Services.Documentos
{
    public class DocumentoService : IDocumentoService
    {
        private IDocumentoRepository _documentoRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public DocumentoService(IDocumentoRepository documentoRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _documentoRepository = documentoRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public DocumentoListOutput GetDocumentoByCategoryId(int categoriaId, int page)
        {
            var documentos = _documentoRepository.GetDocumentoPorCategoriaId(categoriaId, page);
            DocumentoListOutput output = new DocumentoListOutput(documentos);
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Documentos,LogTipo.VisitaPorCategorias);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public DocumentoListOutput GetDocumentos(int page)
        {
            var documentos = _documentoRepository.GetDocumentos(page);
            DocumentoListOutput output = new DocumentoListOutput(documentos);
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Documentos, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public DocumentoListOutput GetDocumentos()
        {
            var documentos = _documentoRepository.GetDocumentos();
            DocumentoListOutput output = new DocumentoListOutput(documentos);
            var log = LogActividad.CreateNew(ModuloEnum.Documentos, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }
    }
}
