﻿using Qavant.Application.Exceptions;
using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Tenants;
using System;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Tenants
{
    public class TenantService : ITenantService
    {
        private readonly ITenantRepository _tenantRepository;

        public TenantService(ITenantRepository tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        public TenantOutput ValidateAndReturn(Canal canal, string tenantKey)
        {
            var tenantAux = GetTenant(canal, tenantKey);
            if (tenantAux is NullTenant)
                throw new TenantNotFoundException($"La empresa '{tenantKey}' no existe.");

            var tenant = _tenantRepository.GetCurrentTenant(canal ,tenantAux.Id);
            TenantOutput output = new TenantOutput(tenant);
            return output;
        }

        public TenantOutput GetCurrentTenant(Canal canal, int tenantId)
        {
            if (tenantId <= 0)
                throw new ApplicationException(nameof(tenantId));

            var tenant = _tenantRepository.GetCurrentTenant(canal, tenantId);
            if (tenant is NullTenant)
                throw new TenantNotFoundException("No existe la empresa.");

            TenantOutput output = new TenantOutput(tenant);
            return output;
        }

        public dynamic GetClientSettings(Canal canal, int tenantId)
        {
            if (tenantId <= 0)
                throw new ApplicationException(nameof(tenantId));

            var tenant = _tenantRepository.GetCurrentTenant(canal, tenantId);
            if (tenant is NullTenant)
                throw new TenantNotFoundException("No existe la empresa.");

            TenantOutput output = new TenantOutput(tenant);
            return output.ParametrosPublicos;
        }

        private Tenant GetTenant(Canal canal, string tenantKey)
        {
            Tenant tenant;
            switch (canal)
            {
                case Canal.App:
                    tenant = _tenantRepository.GetTenantByKey(e => e.TenantKeyApp.ToLower().Contains(tenantKey.ToLower()));
                    break;
                case Canal.Web:
                    tenant = _tenantRepository.GetTenantByKey(e => e.TenantKeyWeb == tenantKey);
                    break;
                default:
                    tenant = new NullTenant();
                    break;
            }
            return tenant;
        }
    }
}