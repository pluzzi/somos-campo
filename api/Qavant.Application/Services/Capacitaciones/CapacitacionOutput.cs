﻿using Qavant.Domain.Capacitaciones;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Capacitaciones
{
    public sealed class CapacitacionOutput
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CourseCategoryId { get; set; }
        public string CourseCategory { get; set; }
        public int CourseTypeId { get; set; }
        public string CourseType { get; set; }
        public string DocumentURL { get; set; }
        public string Extension { get; set; }
        public string VideoURL { get; set; }
        public string LinkURL { get; set; }
        public bool RequiresEvaluation { get; set; }
        public bool? UserWasAlreadyEvaluated { get; set; } //
        public int? ApprovedPercRequired { get; set; }
        public int? ApprovedPercUser { get; set; } //
        public int? FormId { get; set; }
        public bool? FormUniqueReply { get; set; }
        public DateTime? EvaluationDate { get; set; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }


        public CapacitacionOutput(Capacitacion capacitacion)
        {
            Id = capacitacion.Id;
            Title = capacitacion.Titulo;
            Description = capacitacion.Descripcion;
            CourseCategoryId = capacitacion.Categoria.Id;
            CourseCategory = capacitacion.Categoria.Nombre; //
            CourseTypeId = capacitacion.CapacitacionTipo.Id;
            CourseType = capacitacion.CapacitacionTipo.Nombre;
            DocumentURL = capacitacion.DocumentoURL;
            Extension = (capacitacion.DocumentoURL != null) ? Path.GetExtension(capacitacion.DocumentoURL).Replace(".", "") : "";
            VideoURL = capacitacion.VideoURL;
            LinkURL = capacitacion.LinkURL;
            RequiresEvaluation = capacitacion.RequiereEvaluacion;
            ApprovedPercRequired = capacitacion.PorcentajeAprobado;
            FormId = capacitacion.RequiereEvaluacion? capacitacion.FormularioId :(int?)null;
            ReaccionId = capacitacion.InfoReaccion.ReaccionId;
            TotalReacciones = capacitacion.InfoReaccion.TotalReacciones;
        }
    }
}