﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.MiPerfil
{
    public interface IMiPerfilService 
    {
        UsuarioOutput GetPerfil(int usuarioId);
        void SetNewPassword(int usuarioId, string password, string confirmPassword);
        void ChangeOldPassword(int usuarioId, string oldPassword, string newPassword, string confirmPassword);
    }
}
