﻿using Qavant.Domain.Info;

namespace Qavant.Application.Services.InfoPaginas
{
    public sealed class InfoUtilOutput
    {
        public int Id { get;  }
        public string Titulo { get; }
        public string Copete { get; }
        public string Html { get; }                
        public bool Destacada { get; }
        public string Imagen { get; }
        public string Categoria { get; }
        public int? CategoriaId { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public InfoUtilOutput(InfoUtil info)
        {
            Id = info.Id;
            Titulo = info.Titulo;
            Copete = info.Copete;
            Html = info.Html;
            Destacada = info.Destacada;
            Imagen = info.Imagen;
            Categoria = info.Categoria?.Nombre;
            CategoriaId = info.Categoria?.Id;
            ReaccionId = info.InfoReaccion.ReaccionId;
            TotalReacciones = info.InfoReaccion.TotalReacciones;
        }
    }
}