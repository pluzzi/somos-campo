﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Info;
using Qavant.Domain.Modulos;
using System;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.InfoPaginas
{
    public class InfoUtilService : IInfoUtilService
    {
        private IInfoUtilRepository _infoPaginaRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public InfoUtilService(IInfoUtilRepository infoPaginaRepository,IApplicationLoggerActivity applicationLoggerActivity)
        {
            _infoPaginaRepository = infoPaginaRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public InfoUtilListOutput GetListInfoUtil()
        {
            var info = _infoPaginaRepository.GetListInfoUtil();
            if(info is NullInfoUtil)
                throw new ApplicationException("Información no encontrada.");
            InfoUtilListOutput output = new InfoUtilListOutput(info);

            var log = LogActividad.CreateNew(ModuloEnum.InfoUtil, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);

            return output;
        }

        public InfoUtilListOutput GetListInfoUtilByCategoryId(int categoriaId)
        {
            if (categoriaId <= 0)
                throw new ArgumentNullException(nameof(categoriaId));
            var infoPaginas = _infoPaginaRepository.GetListInfoUtilByCategoriaId(categoriaId);
            InfoUtilListOutput output = new InfoUtilListOutput(infoPaginas);

            var log = LogActividad.CreateNew(ModuloEnum.InfoUtil, LogTipo.VisitaPorCategorias);
            _applicationLoggerActivity.Save(log);

            return output;
        }

        public InfoUtilOutput GetInfoUtilById(int id)
        {
            if (id <= 0)
                throw new ArgumentNullException(nameof(id));

            var info = _infoPaginaRepository.GetInfoUtilById(id);

            if (info is NullInfoUtil)
                throw new ApplicationException("Información no encontrada.");

            InfoUtilOutput output = new InfoUtilOutput(info);

            var log = LogActividad.CreateNew(ModuloEnum.InfoUtil, LogTipo.VisitaDetalle, info.Id);
            _applicationLoggerActivity.Save(log);

            return output;
        }

    }
}
