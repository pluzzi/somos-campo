﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Cumpleaños
{
    public class CumpleañosListOutput
    {
        public List<CumpleañosOutput> CumpleañosList { get; }

        public CumpleañosListOutput(ICollection<Domain.Usuarios.Cumpleaños> cumpleañosList)
        {
            List<CumpleañosOutput> cumpleañosResults = new List<CumpleañosOutput>();
            foreach (var c in cumpleañosList)
            {
                CumpleañosOutput output = new CumpleañosOutput(c);
                cumpleañosResults.Add(output);
            }
            CumpleañosList = cumpleañosResults;
        }
    }
}