﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Cumpleaños
{
    public interface ICumpleañosService 
    {
        CumpleañosListOutput GetCumpleaños();
        CumpleañosListOutput GetCumpleaños(int page);
    }
}