﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Modulos;

namespace Qavant.Application.Services.Cumpleaños
{
    public class CumpleañosService : ICumpleañosService
    {
        private ICumpleañosRepository _cumpleañosRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public CumpleañosService(ICumpleañosRepository cumpleañosRepository, IApplicationLoggerActivity applicationLoggerActivity)
        {
            _cumpleañosRepository = cumpleañosRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public CumpleañosListOutput GetCumpleaños()
        {
            var cumpleaños = _cumpleañosRepository.GetCumpleaños();
            CumpleañosListOutput output = new CumpleañosListOutput(cumpleaños);
            var log = LogActividad.CreateNew(ModuloEnum.Cumpleaños, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }

        public CumpleañosListOutput GetCumpleaños(int page)
        {
            var cumpleaños = _cumpleañosRepository.GetCumpleaños(page);
            CumpleañosListOutput output = new CumpleañosListOutput(cumpleaños);
            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Cumpleaños, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

    }
}