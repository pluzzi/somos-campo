﻿using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Categorias
{
    public sealed class CategoriaOutput
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public Nullable<int> PadreCategoriaId { get; set; }
        public string Imagen { get; set; }

        public CategoriaOutput(Categoria Categoria)
        {
            Id = Categoria.Id;
            Nombre = Categoria.Nombre;
            PadreCategoriaId = Categoria.PadreId;
            Imagen = Categoria.Imagen;
        }
    }
}