﻿using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Categorias
{
    public sealed class CategoriaListOutput
    {
        public List<CategoriaOutput> Categorias { get; }

        public CategoriaListOutput(ICollection<Categoria> categorias)
        {
            List<CategoriaOutput> CategoriasResults = new List<CategoriaOutput>();
            foreach (var Categoria in categorias)
            {
                CategoriaOutput output = new CategoriaOutput(Categoria);
                CategoriasResults.Add(output);
            }

            List<CategoriaOutput> lsCatTree = new List<CategoriaOutput>();

            CategoriasTreeFormat(CategoriasResults, ref lsCatTree, null, "", true);

            Categorias = lsCatTree;
        }

        
        private List<CategoriaOutput> CategoriasTreeFormat(List<CategoriaOutput> list, ref List<CategoriaOutput> outList, int? parentId = null, string parentPrefix = "", bool soloHabilitados = true)
        {
            string sMsj = "";
            try
            {
                sMsj = "Paso 1";
                List<CategoriaOutput> lsToReturn = list
                           .Where(w => w.PadreCategoriaId == parentId)
                           .Select(s => new CategoriaOutput (new Categoria())
                           {

                               Id = s.Id,
                               Nombre = parentPrefix + s.Nombre,
                               Imagen = s.Imagen,
                               PadreCategoriaId = s.PadreCategoriaId
                           })
                           .ToList()
                           .OrderBy(o => o.Nombre).ToList();

                sMsj = "Paso 2";
                if (lsToReturn.Count > 0)
                {
                    int index = 0;
                    if (parentId != null)
                        index = outList.FindIndex(w => w.Id == parentId) + 1;
                    outList.InsertRange(index, lsToReturn);
                    foreach (CategoriaOutput cat in lsToReturn)
                        CategoriasTreeFormat(list, ref outList, cat.Id, "--" + parentPrefix, soloHabilitados);
                }

                return lsToReturn;
            }
            catch (Exception ex)
            {
                string sMsjError = "CategoriasTreeFormat --> " + sMsj + " - " + ex.Message;
                if (ex.InnerException != null) { sMsjError = sMsjError + " - " + ex.InnerException.Message; }
                throw new Exception(sMsjError);

            }

        }
    }
}