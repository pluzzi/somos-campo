﻿namespace Qavant.Application.Services.Reacciones
{
    public interface IReaccionService
    {
        void Guardar(ReaccionInput input);
    }
}