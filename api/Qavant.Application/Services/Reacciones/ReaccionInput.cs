﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Reacciones
{
    public class ReaccionInput
    {
        public int ReaccionId { get; set; }
        public int EntidadId { get; set; }
        public string Modulo { get; set; }
    }
}