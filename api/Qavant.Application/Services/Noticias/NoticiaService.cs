﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Modulos;
using Qavant.Domain.Noticias;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Noticias
{
    public class NoticiaService : INoticiaService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly INoticiaRepository _noticiaRepository;
        private IApplicationLoggerActivity _applicationLoggerActivity;

        public NoticiaService(
            ITenantHolder tenantHolder,
            INoticiaRepository noticiaRepository, 
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _tenantHolder = tenantHolder;
            _noticiaRepository = noticiaRepository;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public NoticiaOutput ObtenerNoticiaPorId(int noticiaId)
        {
            var noticia = _noticiaRepository.ObtenerNoticiaPorId(noticiaId);
            if (noticia is NullNoticia)
                throw new ApplicationException("Noticia no encontrada.");

            var log = LogActividad.CreateNew(ModuloEnum.Noticias, LogTipo.VisitaDetalle, noticia.Id);
            _applicationLoggerActivity.Save(log);

            NoticiaOutput output = new NoticiaOutput(noticia);
            return output;
        }

        public NoticiaListOutput ObtenerNoticias(int page)
        {
            var noticias = _noticiaRepository.ObtenerNoticias(page);
            NoticiaListOutput output = new NoticiaListOutput(noticias);

            if (page == 1)
            {
                var log = LogActividad.CreateNew(ModuloEnum.Noticias, LogTipo.VisitaLista);
                _applicationLoggerActivity.Save(log);
            }
            return output;
        }

        public NoticiaListOutput ObtenerNoticias()
        {
            var noticias = _noticiaRepository.ObtenerNoticias();
            NoticiaListOutput output = new NoticiaListOutput(noticias);
            var log = LogActividad.CreateNew(ModuloEnum.Noticias, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);
            return output;
        }
    }
}