﻿using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Noticias
{
    public sealed class NoticiaListOutput
    {
        public List<NoticiaOutput> Noticias { get; }

        public NoticiaListOutput(ICollection<Noticia> noticias)
        {
            List<NoticiaOutput> noticiasResults = new List<NoticiaOutput>();
            foreach (var noticia in noticias)
            {
                NoticiaOutput output = new NoticiaOutput(noticia);
                noticiasResults.Add(output);
            }
            Noticias = noticiasResults;
        }
    }
}