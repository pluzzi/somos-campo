﻿using Qavant.Domain.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioOutput
    {
        public int FormId { get; }
        public string Title { get; }
        public string Description { get; }
        public int FormTypeId { get; }
        public List<PreguntaOutput> Questions { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public FormularioOutput(Formulario formulario)
        {
            FormId = formulario.Id;
            Title = formulario.Titulo;
            Description = formulario.Descripcion;
            FormTypeId = (byte)formulario.Tipo;
            ReaccionId = formulario.InfoReaccion.ReaccionId;
            TotalReacciones = formulario.InfoReaccion.TotalReacciones;

            List<PreguntaOutput> preguntasResults = new List<PreguntaOutput>();
            foreach (var pregunta in formulario.Preguntas)
            {
                PreguntaOutput output = new PreguntaOutput(pregunta);
                preguntasResults.Add(output);
            }
            Questions = preguntasResults;
        }
    }

    public class PreguntaOutput
    {
        public int Id { get; }
        public string Question { get; }
        public string QuestionTypeId { get; }
        public int Order { get; }
        public bool Required { get; }
        public string Answer { get; }
        public int AnswerOptionId { get; }
        public List<OpcionOutput> Options { get; }


        public PreguntaOutput(Pregunta pregunta)
        {
            Id = pregunta.Id;
            Question = pregunta.Enunciado;
            QuestionTypeId = pregunta.HtmlInputType.ToString();
            Order = pregunta.Orden;
            Required = pregunta.Requerido;

            List<OpcionOutput> opcionesResults = new List<OpcionOutput>();
            foreach (var opcion in pregunta.Opciones)
            {
                OpcionOutput output = new OpcionOutput(opcion);
                opcionesResults.Add(output);
            }
            Options = opcionesResults;
        }
    }

    public class OpcionOutput
    {
        public int Id { get; }
        public string Description { get; }
        public int Order { get; }
        public bool AnswerSelected { get; }
        public bool IsRightAnswer { get; }
        public bool Enabled { get; }

        public OpcionOutput(Opcion opcion)
        {
            Id = opcion.Id;
            Description = opcion.Descripcion;
            Order = opcion.Orden;
            Enabled = opcion.Habilitado;
        }
    }
}