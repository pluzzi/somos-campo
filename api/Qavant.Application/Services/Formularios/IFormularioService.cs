﻿using Qavant.Domain.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Formularios
{
    public interface IFormularioService 
    {
        FormularioListOutput GetFormsPorTipo(byte tipo, int usuarioId);
        void GuardarRespuestas(FormularioInput formInput, int usuarioId);
        FormularioOutput GetForm(int formId, int usuarioId);
    }
}