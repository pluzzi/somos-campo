﻿using Qavant.Application.Infrastructure;
using Qavant.Application.Repositories;
using Qavant.Domain.Formularios;
using Qavant.Domain.Modulos;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioService : IFormularioService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IFormularioRepository _formularioRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IHtmlTemplateGenerator _htmlTemplateGenerator;
        private readonly IEmailService _emailService;
        private readonly IApplicationLoggerActivity _applicationLoggerActivity;

        public FormularioService(
            ITenantHolder tenantHolder,
            IFormularioRepository formularioRepository,
            IUsuarioRepository usuarioRepository,
            IHtmlTemplateGenerator htmlTemplateGenerator,
            IEmailService emailService,
            IApplicationLoggerActivity applicationLoggerActivity)
        {
            _tenantHolder = tenantHolder;
            _formularioRepository = formularioRepository;
            _usuarioRepository = usuarioRepository;
            _emailService = emailService;
            _htmlTemplateGenerator = htmlTemplateGenerator;
            _applicationLoggerActivity = applicationLoggerActivity;
        }

        public FormularioListOutput GetFormsPorTipo(byte tipo, int usuarioId)
        {
            TipoFormulario tipoForm = (TipoFormulario)Enum.ToObject(typeof(TipoFormulario), tipo);
            if (!Enum.IsDefined(typeof(TipoFormulario), tipoForm))
                throw new ArgumentException($"Tipo de formulario no existente.");

            var formularios = _formularioRepository.GetFormsPorTipo(tipoForm, usuarioId);

            var log = LogActividad.CreateNew(ModuloEnum.Formularios, LogTipo.VisitaLista);
            _applicationLoggerActivity.Save(log);

            FormularioListOutput output = new FormularioListOutput(formularios);
            return output;
        }

        public FormularioOutput GetForm(int formId, int usuarioId)
        {
            Formulario form = _formularioRepository.GetForm(formId, usuarioId);
            if (form is NullFormulario)
                throw new ApplicationException("No existe el formulario solicitado.");

            var log = LogActividad.CreateNew(ModuloEnum.Formularios, LogTipo.VisitaDetalle, form.Id);
            _applicationLoggerActivity.Save(log);

            FormularioOutput output = new FormularioOutput(form);
            return output;
        }

        public void GuardarRespuestas(FormularioInput formInput, int usuarioId)
        {
            var usuario = ValidarUsuario(usuarioId);
            var form = ValidarFormulario(formInput.FormId, usuarioId);
            ValidarRespuestaUnica(form);

            var respuestasForm = GenerarEstructuraRespuestas(usuario, form, formInput);
            ValidarCantidadMinimaRespuestas(respuestasForm);
            ValidarPreguntasObligatorias(respuestasForm);

            GuardarRespuestas(respuestasForm);
            EnviarNotificacion(usuario, form, respuestasForm);
        }

        #region Internal members
        private Usuario ValidarUsuario(int usuarioId)
        {
            var usuario = _usuarioRepository.GetUsuarioById(usuarioId);
            if (usuario is NullUsuario)
                throw new ApplicationException("Usuario inválido.");

            if (!usuario.Habilititado)
                throw new ApplicationException("Usuario deshabilitado.");

            return usuario;
        }

        private Formulario ValidarFormulario(int formularioId, int usuarioId)
        {
            var form = _formularioRepository.GetForm(formularioId, usuarioId);
            if (form is NullFormulario)
                throw new ApplicationException("Formulario inválido.");

            if (!form.Habilitado)
                throw new ApplicationException("Formulario deshabilitado.");

            return form;
        }

        private void ValidarRespuestaUnica(Formulario formulario)
        {
            bool esFormRptaUnica = formulario.RespuestaUnica;
            bool respondido = formulario.Respondido;

            if (esFormRptaUnica && respondido)
                throw new ApplicationException("Formulario ya respondido: Este formulario es de respuesta única.");
        }

        private RespuestasEnFormulario GenerarEstructuraRespuestas(Usuario usuario, Formulario formulario, FormularioInput formInput)
        {
            List<Respuesta> respuestasResults = new List<Respuesta>();
            foreach (var preguntaInput in formInput.Questions)
            {
                var pregunta = formulario.Preguntas.Where(p => p.Id == preguntaInput.Id).SingleOrDefault();
                if (pregunta != null)
                {
                    Respuesta respuesta = new Respuesta();
                    respuesta.SetPregunta(pregunta);
                    if (pregunta.Tipo == TipoPregunta.Seleccion)
                    {
                        if (pregunta.HtmlInputType == "MULTIPLE")
                        {
                            foreach (var opcionInput in preguntaInput.Options)
                            {
                                if (opcionInput.AnswerSelected)
                                {
                                    var opcion = pregunta.Opciones.Where(o => o.Id == opcionInput.Id).SingleOrDefault();
                                    if (opcion != null)
                                    {
                                        respuesta.AgregarOpcionSeleccionada(opcion);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // DROPDOWN, OPTION
                            var opcion = pregunta.Opciones.Where(o => o.Id == preguntaInput.AnswerOptionId).SingleOrDefault();
                            if (opcion != null)
                            {
                                respuesta.AgregarOpcionSeleccionada(opcion);
                            }
                        }
                    }
                    else
                    {
                        // Simple
                        string valor = (preguntaInput.Answer != null) ? preguntaInput.Answer : string.Empty;
                        respuesta.SetValorRespuesta(valor);
                    }
                    respuestasResults.Add(respuesta);
                }
            }

            RespuestasEnFormulario respuestasFormulario = new RespuestasEnFormulario(usuario, formulario, respuestasResults);
            return respuestasFormulario;
        }

        private void ValidarCantidadMinimaRespuestas(RespuestasEnFormulario respuestasForm)
        {
            int cantidad = respuestasForm.GetCantidadPreguntasRespondidas();
            if (cantidad == 0)
                throw new ApplicationException("Debe responder al menos una pregunta.");
        }

        private void ValidarPreguntasObligatorias(RespuestasEnFormulario respuestasForm)
        {
            int cantidad = respuestasForm.GetCantidadPreguntasObligatoriasSinRespuesta();
            if (cantidad > 0)
                throw new ApplicationException("Hay preguntar obligatorias que faltan responder.");
        }

        private void GuardarRespuestas(RespuestasEnFormulario respuestasForm)
        {
            respuestasForm.RemoverRespuestasVacias();
            _formularioRepository.GuardarRespuestas(respuestasForm);
        }

        private void EnviarNotificacion(Usuario usuario, Formulario form, RespuestasEnFormulario respuestasForm)
        {
            if (!form.EnviaEmail)
                return;

            try
            {
                var emails = form.GetEmailsNotificacion();
                string mensaje = GenerarCuerpoMensaje(usuario, form, respuestasForm);

                var SMTPUsername = _tenantHolder.Tenant.ParametrosInternos["email"]["SMTPUsername"];
                var SMTPPassword = _tenantHolder.Tenant.ParametrosInternos["email"]["SMTPPassword"];
                var SMTPServer = _tenantHolder.Tenant.ParametrosInternos["email"]["SMTPServer"];
                var SMTPPort = Convert.ToInt32(_tenantHolder.Tenant.ParametrosInternos["email"]["SMTPPort"]);
                var SMTPEnableSsl = Convert.ToBoolean(_tenantHolder.Tenant.ParametrosInternos["email"]["SMTPEnableSsl"]);
                var emailFrom = _tenantHolder.Tenant.ParametrosInternos["email"]["EmailFrom"];
                var emailDisplayName = _tenantHolder.Tenant.ParametrosInternos["email"]["EmailDisplayName"];

                _emailService.Configure(SMTPUsername, SMTPPassword, SMTPServer, SMTPPort, SMTPEnableSsl, emailFrom, emailDisplayName);
                _emailService.SendMail(emails, form.EmailAsunto, mensaje);
            }
            catch (Exception)
            {
                // log
            }
        }

        private string GenerarCuerpoMensaje(Usuario usuario, Formulario form, RespuestasEnFormulario respuestasForm)
        {
            string mensaje = string.Empty;
            FormularioEmailViewModel vm = new FormularioEmailViewModel(usuario, form, respuestasForm);
            //TO-DO: el template HTML puede considerarse como parte de la parametría del tenant. Moverlo a BD.
            string template = @"
            <div style='border:1px solid #ccc; padding:10px;'>
            <h2>@Model.Titulo</h2>
            <h4>@Model.Introduccion</h4>
            <p><strong>Usuario: </strong> @Model.UsuarioApellido, @Model.UsuarioNombre</p>
            <p><strong>Email: </strong> @Model.UsuarioEmail</p>
            </div>

            <hr />

            <div style='border:1px solid #ccc; padding:10px;'>
            @foreach (var pregunta in @Model.Preguntas)
            {
            <p><strong>@pregunta.Enunciado: </strong></p>
                if (pregunta.RespuestasSeleccion.Count > 0)
                {
                    <ul>
                    @foreach (var seleccion in pregunta.RespuestasSeleccion)
                    {
                        <li>@seleccion.Descripcion</li>
                    }
                    </ul>
                }
                else
                {
                    @pregunta.RespuestaTexto
                } 
            }
            </div>
            ";
            mensaje = _htmlTemplateGenerator.GetTemplateEmail(template, vm);
            return mensaje;
        }
        #endregion
    }
}