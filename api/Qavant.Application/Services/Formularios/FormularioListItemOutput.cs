﻿using Qavant.Domain.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Formularios
{
    public class FormularioListItemOutput
    {
        public int Id { get; }
        public string Title { get; }
        public string Description { get; }
        public bool AlreadyAnswered { get; }
        public bool UniqueReply { get; }
        public int ReaccionId { get; }
        public int TotalReacciones { get; }

        public FormularioListItemOutput(Formulario formulario)
        {
            Id = formulario.Id;
            Title = formulario.Titulo;
            Description = formulario.Descripcion;
            UniqueReply = formulario.RespuestaUnica;
            AlreadyAnswered = formulario.Respondido;
            ReaccionId = formulario.InfoReaccion.ReaccionId;
            TotalReacciones = formulario.InfoReaccion.TotalReacciones;
        }
    }
}