﻿using Qavant.Application.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Services.Activacion
{
    public interface IActivacionService
    {
        UsuarioOutput ValidarUsuario(string username);
        void ActivarUsuario(string username, string password);
    }
}
