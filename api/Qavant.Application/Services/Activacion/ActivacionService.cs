﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qavant.Application.Exceptions;
using Qavant.Application.Repositories;
using Qavant.Application.Services.Shared;
using Qavant.Domain.Usuarios;
using ApplicationException = Qavant.Application.Exceptions.ApplicationException;

namespace Qavant.Application.Services.Activacion
{
    public class ActivacionService : IActivacionService
    {
        private readonly ITenantHolder _tenantHolder;
        private readonly IUsuarioRepository _usuarioRepository;

        public ActivacionService(ITenantHolder tenantHolder, IUsuarioRepository usuarioRepository)
        {
            _tenantHolder = tenantHolder;
            _usuarioRepository = usuarioRepository;
        }

        public UsuarioOutput ValidarUsuario(string username)
        {
            var usuario = _usuarioRepository.GetUsuarioByUsername(username);
            if (usuario is NullUsuario)
                throw new UsuarioNoRegistradoException($"No encontramos el usuario '{username}' ingresado en el sistema.");

            if (usuario.Activo)
                throw new ApplicationException($"El usuario '{username}' ya se encuentra activo en el sistema. Si no recuerdas tu contraseña elige la opción OLVIDÉ MI CONTRASEÑA para recuperarla.");

            UsuarioOutput output = new UsuarioOutput(usuario);
            return output;
        }

        public void ActivarUsuario(string username, string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ApplicationException("Contraseña en blanco.");

            var usuario = _usuarioRepository.GetUsuarioByUsername(username);
            if (usuario is NullUsuario)
                throw new UsuarioNoRegistradoException($"No encontramos el usuario '{username}' ingresado en el sistema.");

            usuario.SetActivo(true);
            usuario.SetPassword(password);
            _usuarioRepository.Actualizar(usuario);
        }
    }
}
