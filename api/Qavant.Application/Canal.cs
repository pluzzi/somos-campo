﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application
{
    public enum Canal
    {
        None = 0,
        App = 1,
        Web = 2,
        Admin = 3
    }
}
