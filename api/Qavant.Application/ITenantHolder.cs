﻿using Qavant.Application.Services.Shared;
using Qavant.Application.Services.Tenants;
using Qavant.Domain.Tenants;
using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application
{
    public interface ITenantHolder
    {
        TenantOutput Tenant { get; set; }
        int UsuarioId { get; set; }
        Canal Canal { get; set; }
    }
}