﻿using Qavant.Application.Services.Tenants;
using Qavant.Domain.Tenants;

namespace Qavant.Application
{
    public class TenantHolder : ITenantHolder
    {
        public TenantOutput Tenant { get; set; }
        public int UsuarioId { get; set; }
        public Canal Canal { get; set; }
    }
}