﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure.LoggerActivity
{
    public class LogHolder : ILogHolder
    {
        public int TenantId { get; set; }
        public int UsuarioId { get; set; }
        public Canal Canal { get; set; }
    }
}
