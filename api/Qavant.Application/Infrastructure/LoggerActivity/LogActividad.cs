﻿using Qavant.Domain.Modulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Infrastructure
{
    public class LogActividad
    {
        public static LogActividad CreateNew(ModuloEnum modulo, LogTipo logTipo)
        {
            return CreateNew(modulo, logTipo, null, string.Empty);
        }

        public static LogActividad CreateNew(ModuloEnum modulo, LogTipo logTipo, int? entidadId)
        {
            return CreateNew(modulo, logTipo, entidadId, string.Empty);
        }

        public static LogActividad CreateNew(ModuloEnum modulo, LogTipo logTipo, int? entidadId, string detalle)
        {
            var log = new LogActividad
            {
                Modulo = modulo,
                EntidadId = entidadId,
                LogTipo = logTipo,
                Detalle = detalle,
                FechaActividad = DateTime.Now
            };
            return log;
        }

        public ModuloEnum Modulo { get; private set; }
        public int? EntidadId { get; private set; }
        public LogTipo LogTipo { get; private set; }
        public string Detalle { get; private set; }
        public DateTime FechaActividad { get; private set; }
    }
} 