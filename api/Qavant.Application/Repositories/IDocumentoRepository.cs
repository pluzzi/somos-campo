﻿using Qavant.Domain.Documentos;
using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IDocumentoRepository
    {
        ICollection<Documento> GetDocumentos();
        ICollection<Documento> GetDocumentos(int page);
        ICollection<Documento> GetDocumentoPorCategoriaId(int categoriaId, int page);
    }
}
