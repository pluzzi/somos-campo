﻿using Qavant.Domain.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IUsuarioRepository
    {
        Usuario Login(string username, string password);

        void Actualizar(Usuario usuario);

        bool Exists(string username);

        Usuario GetUsuarioById(int usuarioId);

        Usuario GetUsuarioByUsername(string username);

        ICollection<Usuario> GetUsuarios();

        ICollection<Usuario> GetUsuarios(int page);

        ICollection<Usuario> Search(int page, string searchTerm);

        void ResetPassword(Usuario usuario);
    }
}