﻿using Qavant.Domain.Capacitaciones;
using System.Collections.Generic;

namespace Qavant.Application.Repositories
{
    public interface ICapacitacionRepository
    {
        ICollection<Capacitacion> GetCapacitaciones();
        Capacitacion GetCapacitacionById(int Id);
    }
}
