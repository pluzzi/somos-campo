﻿using Qavant.Domain.Info;
using System.Collections.Generic;

namespace Qavant.Application.Repositories
{
    public interface IInfoUtilRepository
    {
        ICollection<InfoUtil> GetListInfoUtil();
        ICollection<InfoUtil> GetListInfoUtilByCategoriaId(int id);
        InfoUtil GetInfoUtilById(int id);
    }
}
