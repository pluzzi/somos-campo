﻿using Qavant.Domain.Formularios;
using System.Collections.Generic;

namespace Qavant.Application.Repositories
{
    public interface IFormularioRepository
    {
        ICollection<Formulario> GetFormsPorTipo(TipoFormulario tipo, int usuarioId);

        Formulario GetForm(int formId, int usuarioId);

        void GuardarRespuestas(RespuestasEnFormulario solucion);
    }
}
