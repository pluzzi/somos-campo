﻿using Qavant.Domain.Galerias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IGaleriaRepository
    {
        Galeria ObtenerGaleriaPorId(int id);
        ICollection<Galeria> ObtenerGalerias();
        ICollection<Galeria> ObtenerGalerias(int pagina);
        ICollection<Galeria> ObtenerGaleriasPorCategoria(int categoriaId);
    }
}
