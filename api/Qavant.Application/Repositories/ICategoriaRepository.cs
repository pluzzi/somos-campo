﻿using Qavant.Domain.Categorias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface ICategoriaRepository
    {
        ICollection<Categoria> ObtenerCategoriasPorModuloId(int moduloId);
        ICollection<Categoria> ObtenerCategoriasPorModulo(string nemonico);
    }
}
