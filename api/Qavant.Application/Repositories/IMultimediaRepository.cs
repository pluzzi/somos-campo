﻿using Qavant.Domain.Modulos;
using Qavant.Domain.Multimedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface IMultimediaRepository
    {
        ICollection<ElementoMultimedia> ObtenerElementosMultimedia(ModuloEnum modulo, int entidadId);
        ICollection<ElementoMultimedia> ObtenerElementosMultimedia(ModuloEnum modulo, int entidadId, int? cantidad = 0);
    }
}