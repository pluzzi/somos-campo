﻿using Qavant.Domain.Noticias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qavant.Application.Repositories
{
    public interface INoticiaRepository
    {
        Noticia ObtenerNoticiaPorId(int id);
        ICollection<Noticia> ObtenerNoticias();
        ICollection<Noticia> ObtenerNoticias(int page);        
    }
}